import { GetStaticProps } from "next";
import React from "react";

import { Link } from "@/components/Link";
import { Table } from "@/components/Table";
import { Title } from "@/components/Title";
import { getMembers, Member } from "@/lib/members";
import { Term, capitalize, getCurrentTermYear } from "@/utils";

import styles from "./members.module.css";

interface Props {
  members: Member[];
  year: number;
  term: Term;
}

export default function Members(props: Props) {
  return (
    <>
      <Title>Members</Title>
      <h1 className={styles.title}>Members</h1>
      <p>
        {`The members for ${capitalize(props.term)} ${
          props.year
        } are listed here. We currently
        have ${props.members.length} members. Use of this list for solicitation
        of any form is prohibited, if you wish to get in touch with the
        membership as a whole please contact the Executive.`}
      </p>
      <Table className={styles.table}>
        <thead>
          <tr>
            <th>Name / Webpage</th>
            <th>Program</th>
            <th>Userid</th>
          </tr>
        </thead>
        <tbody>
          {props.members.map((member) => (
            <tr key={member.id}>
              <td>
                <Link href={`https://csclub.uwaterloo.ca/~${member.id}/`}>
                  {member.name}
                </Link>
              </td>
              <td>{member.program}</td>
              <td>{member.id}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export const getStaticProps: GetStaticProps<Props> = async () => {
  const curTerm = getCurrentTermYear();
  return {
    props: {
      year: curTerm.year,
      term: curTerm.term,
      members: await getMembers(curTerm.year, curTerm.term),
    },
  };
};
