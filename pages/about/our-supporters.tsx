import React from "react";

import { Image } from "@/components/Image";
import { Title } from "@/components/Title";

import Content from "../../content/about/our-supporters.mdx";

import styles from "./our-supporters.module.css";

export default function OurSupporters() {
  return (
    <div className={styles.page}>
      <Title>Our Supporters</Title>
      <div className={styles.headerContainer}>
        <h1 className={styles.header}>Our Supporters</h1>
        <Image src="images/our-supporters/codey.svg" className={styles.codey} />
      </div>
      <div className={styles.content}>
        <Content />
      </div>
    </div>
  );
}
