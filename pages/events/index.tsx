import { GetStaticProps } from "next";

import { getEventsPageProps } from "@/lib/events";
import { getCurrentTermYear } from "@/utils";

import TermPage, { Props } from "./[year]/[term]";

export default TermPage;

export const getStaticProps: GetStaticProps<Props> = async () => {
  return { props: await getEventsPageProps(getCurrentTermYear()) };
};
