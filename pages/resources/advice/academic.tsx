import React from "react";

import { Title } from "@/components/Title";

import Content from "../../../content/resources/advice/academic-advice.md";

import { Advice } from "./co-op";

export default function AcademicAdvice() {
  return (
    <>
      <Title>Academic Advice</Title>
      <Advice>
        <Content />
      </Advice>
    </>
  );
}
