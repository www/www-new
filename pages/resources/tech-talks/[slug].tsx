import { ParsedUrlQuery } from "querystring";

import { GetStaticPaths, GetStaticProps } from "next";
import { MDXRemote, MDXRemoteSerializeResult } from "next-mdx-remote";
import React from "react";

import { TechTalkCard } from "@/components/TechTalkCard";
import { Title } from "@/components/Title";
import { getTechTalk, getTechTalkNames, Metadata } from "@/lib/tech-talks";

import { Header } from "./";

import styles from "./index.module.css";

interface Props {
  content: MDXRemoteSerializeResult;
  metadata: Metadata;
}

export default function TechTalk({ content, metadata }: Props) {
  return (
    <div className={styles.page}>
      <Title>{[metadata.title, "Tech Talks"]}</Title>
      <Header />
      <TechTalkCard
        {...metadata}
        poster={metadata.thumbnails.large ?? metadata.thumbnails.small}
      >
        <MDXRemote {...content} />
      </TechTalkCard>
    </div>
  );
}

export const getStaticProps: GetStaticProps<Props, Params> = async (
  context
) => {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const talk = await getTechTalk(context.params!.slug);

  return { props: talk };
};

interface Params extends ParsedUrlQuery {
  slug: string;
}

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  return {
    fallback: false,
    paths: (await getTechTalkNames()).map((slug) => ({ params: { slug } })),
  };
};
