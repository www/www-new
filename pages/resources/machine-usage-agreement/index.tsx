import path from "path";

import {
  createReadAllPage,
  Options,
} from "@/components/OrganizedContent/ReadAll";
import { createReadAllGetStaticProps } from "@/components/OrganizedContent/static";

export const options: Options = {
  title: "Machine Usage Agreement",
  // TODO: Different image for this?
  image: "images/services.svg",
  pagePath: path.join("resources", "machine-usage-agreement"),
};

export default createReadAllPage(options);

export const getStaticProps = createReadAllGetStaticProps(options.pagePath);
