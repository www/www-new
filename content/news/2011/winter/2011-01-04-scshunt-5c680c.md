---
author: 'scshunt'
date: 'January 04 2011 00:00'
---

Nominations for Winter 2011 executive are now open on the CSC whiteboard. They close at 3:00 PM on Tuesday, January 11. Elections will be held at 4:00 PM on Wednesday, January 12 on the brand-new furniture in the Comfy Lounge. You can also nominate someone or ask any other questions or just complain to [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>).