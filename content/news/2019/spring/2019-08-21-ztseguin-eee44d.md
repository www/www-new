---
author: 'ztseguin'
date: 'August 21 2019 01:00'
---

On August 21st, we were informed that sometime before 2:30PM EDT taurine caught fire. As a result, it is currently unavailable and is expected to remain so permanently.

See the [Machine List](<https://wiki.csclub.uwaterloo.ca/Machine_List#General-Use_Servers>) for alternate general use servers.