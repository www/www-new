---
author: 'mtrberzi'
date: 'September 13 2019 01:00'
---

Fall 2019 elections have concluded. Here are your executives for the term.

- President: Dhruv Jauhar (djauhar)
- Vice President: Aditya Thakral (a3thakra)
- Treasurer: Rishabh Minocha (rkminoch)
- Secretary/Assistant Vice President: Tammy Khalaf (tekhalaf)
- Sysadmin: Murphy Berzish (mtrberzi)

<!-- -->

 The remaining appointed positions are - Office Manager: Zihan Zhang (z577zhan)
- Librarian: Raghav Sethi (r5sethi)

<!-- -->