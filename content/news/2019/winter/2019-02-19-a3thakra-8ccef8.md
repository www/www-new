---
author: 'a3thakra'
date: 'February 19 2019 00:00'
---

There will be scheduled downtime for maintenance on many Computer Science Club servers on Thursday, February 21 beginning at 9:45 AM. Machines are expected to be up by 11:30 AM, but issues may arise that require more time to correct.

The list of affected public servers is:

- taurine
- sucrose
- corn-syrup
- high-fructose-corn-syrup (hfcs)
- [caffeine](<https://csclub.uwaterloo.ca/>)

<!-- -->

During this time, CSC email will be inaccessible but mail will be queued up and delivered after the maintenance period is over. All websites hosted by CSC will be inaccessible for a short period of time as caffeine is rebooted. CSC Cloud will also be unavailable, and any virtual machines on CSC cloud will have to be manually restarted after the maintenance window.

Apologies for the inconvenience.