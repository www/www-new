---
author: 'jbroman'
date: 'September 17 2013 01:00'
---

Congratulations to this term's new executive!

- President: ehashman
- Vice-president: m4burns
- Treasurer: dchlobow
- Secretary: e45lee

<!-- -->

Additionally, the following people were appointed.

- Sysadmin: jbroman
- Librarian: jladan
- Office Manager: scshunt

<!-- -->