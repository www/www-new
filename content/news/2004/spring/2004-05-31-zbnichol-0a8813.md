---
author: 'zbnichol'
date: 'May 31 2004 01:00'
---

Audio has been added for the Larry Smith talk: Computing's Next Great Empires. It is available [in Ogg format](<http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk.ogg>) or [in MP3 format](<http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk.mp3>). Thanks to all who came out. The talk was a great success.