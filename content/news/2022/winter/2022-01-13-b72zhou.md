---
author: 'b72zhou'
date: 'January 13 2022 00:00'
---

Winter 2022 elections have concluded. Here are your executives for the term:

- President: Juthika Hoque (j3hoque)
- Vice President (Head of Events): Eric Huang (e48huang)
- Assistant Vice President (Lead of Marketing): Dina Orucevic (dmorucev)
- Treasurer: Eden Chan (e223chan)
- Sysadmin: Raymond Li (r389li)

<!-- -->

Additionally, the following postions were appointed:

- Head of Discord: Andy Wang (a284wang)
- Heads of Design: Vivian Guo (v6guo) and Jenny Zhang (j2447zha)
- Head of Reps: Amy Luo (a27luo)

<!-- -->