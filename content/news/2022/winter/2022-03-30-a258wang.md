---
author: 'a258wang'
date: 'March 30 2022 00:00'
---
Are you graduating this year and looking back on your time at UW? 💭  Do you want to share your experiences, learn about others’ experiences and create something you’ll be able to look back on years from now? Well CSC is launching its first ever Class Profile for the CS Class of 2022! 🎉

🤔 What is a Class Profile?
👉 It’s a voluntary and confidential survey that asks questions about YOUR experience as a CS/CFM/CS-BBA student at the University of Waterloo. After the data collection, we will release a summary of the responses in the form of a website so you can learn more about your graduating class.

📌 Why should I fill it out?
👉 To share your experience as a CS Student at UW.
👉 To create a web page that will not only be entertaining for you to view with your friends but also serve as a guide for current and future CS students to get a glimpse into being a CS student.
👉 To be entered in a draw to win 1 of 3 gift cards valued at $25, $25, $50!

👀 How do I fill it out?
👉 Fill the form out at https://bit.ly/uw-cs22-class-profile!

📅 The form must be submitted by April 29, 2022 at 11:59pm EST for your response to be recorded and to be eligible for the giveaway. Make sure to get your fellow CS ‘22 friends to fill it out as well!

Please note that while this survey is confidential, in order to fulfill the giveaway we need to collect your email; however this will be stored separately from the form. If you have any questions or concerns, feel free to contact us through our Discord or email us at exec@csclub.uwaterloo.ca! 💙
