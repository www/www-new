---
author: 'shahanneda'
date: 'November 18 2022 00:00'
---
CS Club is hiring!

📣 Are you passionate about making a difference in the UW Computer Science and surrounding communities? Are you interested in organizing far-reaching events, working on community initiatives, reaching out to industry professionals, or being a member of an impactful team?

If so, apply for a role on CS Club's Organizing Committee for Winter 2023! CS Club (CSC) is looking for people like you! 🙌

👀 Role descriptions can be found at: https://csclub.ca/role-descriptions

⏲️ The form will close on Friday, November 25 at 11:59 PM, so apply ASAP! We'll reach out through email after this date for interviews.

👉 Apply at https://csclub.ca/hiring! Alternatively, you can email us at exec@csclub.uwaterloo.ca from your UW email with an introduction of yourself, which positions you're interested in, and any questions you might have.