---
author: 'n3parikh'
date: 'June 06 2021 01:00'
---

MathSoc has decided to waive club fees for the Spring 2021 term.

- If you are a current CSC member who has already paid for Spring 2021 membership, your membership has been extended by one term.
- If you are a current CSC member whose membership is expired for the Spring 2021 term, you can email [exec@csclub.uwaterloo.ca](<mailto:syscom@csclub.uwaterloo.ca>) to have your membership renewed for the term at no cost.

<!-- -->

If you are not a CSC member, but would like to become one, we continue to use the modified process we put in place in Spring 2020 making remote registrations possible:

Email the CSC Systems Committee at [syscom@csclub.uwaterloo.ca](<mailto:syscom@csclub.uwaterloo.ca>) from your UWaterloo email address with the following:

1. a scan or photograph copy of your WatCard,
2. your WatIAM userid, and
3. your acknowledgement of having read, understood, and agreeing with our [Machine Usage Agreement](<https://csclub.uwaterloo.ca/services/machine_usage>).

<!-- -->

The club office will remained closed until further notice. Have a safe Spring term!