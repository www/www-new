---
author: 'merenber'
date: 'March 19 2021 01:00'
---

Computer Science Club systems and services will be unavailable on Saturday, Mar. 20 due to a planned power outage in the Mathematics and Computer Building (MC) from 7am to 5pm.

The CSC will begin shutting down machines at 6am in preparation of the outage.

Please prepare for the outage by:

-  Ensuring all running processes have their state saved (configuration, data, etc.) 
-  Any important files are backed up off-site from the CSC 

<!-- -->

If you have any questions/concerns, please email the Systems Committee.