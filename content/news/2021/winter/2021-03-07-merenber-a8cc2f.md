---
author: 'merenber'
date: 'March 07 2021 00:00'
---

CSC now hosts its own instance of BigBlueButton at [https://bbb.csclub.uwaterloo.ca](<https://bbb.csclub.uwaterloo.ca>), freely available for all members.

[BigBlueButton](<https://bigbluebutton.org>) is a free and open source video conferencing platform. It runs directly in the browser, and includes many useful features such as multi-user whiteboards, embedded videos, and interactive polls.

To get the most out of BBB, there are some tutorial videos you can watch [here](<https://bigbluebutton.org/html5>).

If you have any questions or concerns, please email syscom.

Happy video calling!