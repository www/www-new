---
author: 'merenber'
date: 'March 29 2021 01:00'
---

The CSC has updated its SpamAssassin configuration to use a site-wide Bayesian learner instead of per-user. This will hopefully reduce duplicate efforts to keep spam out of our inboxes. Unfortunately this may lead to increased false positives. See the instructions on our [wiki](<https://wiki.csclub.uwaterloo.ca/Mail#Spamfiltering>) on how to revert to the old behaviour if you wish to do so.