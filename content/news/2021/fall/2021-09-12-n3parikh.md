---
author: 'n3parikh'
date: 'September 12 2021 00:00'
---
📢 UW’s Tech Clubs are proud to present to you Bootcamp! This event is a new initiative brought to you to help sharpen your resumes happening on September 19th from 6 PM to 10 PM ET. We're looking for both mentors who are willing to critique resumes, and mentees who would like their resume critiqued.

📝 In order to qualify to be a mentor, you must at least have started your second coop. This is a great opportunity to help others with the knowledge you have, and make a difference in the UW Tech community. If that interests you, please sign up! 💻 We’ll be emailing you more information closer to the event date.

👉 Sign up to be a mentor at https://bit.ly/bootcamp-resumes-interviews

As a mentee, you’ll have the opportunity to meet with a chosen mentor for a certain amount of time, and receive feedback on your resume. 💬 We’ll be using an SE22 FYDP project ReviewKit at https://reviewkit.me/main/ to help us. You will be paired with a mentor who is knowledgeable in the same or a similar career path to yours to ensure relevant feedback! 👌

If you’re interested, please sign up! We would love to help you feel ready and confident for the upcoming job hunt. After signing up, you’ll soon receive a link to the Discord server in which this event takes place.

👉 Sign up to be a mentee at https://bit.ly/f21-bootcamp-resume 


📅 Deadline to Sign Up: Friday 17th September 11:59 PM ET
