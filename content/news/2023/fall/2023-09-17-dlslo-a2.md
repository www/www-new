---
author: 'dlslo'
date: 'September 17 2023 00:01'
---

The executive team would like to appoint the following:

- Head Community Representative: Gordon Lin (g3lin)
- Head of Design: Anny Wei (a25wei)
- Head of Discord (CodeyBot) Developers: Prabhav Khera (p2khera), Fan Yang (f25yang)
- Head Discord Moderator: Laura Nguyen (l69nguye)
- Heads of Events: Anthony Wang (aj3wang), Stella Tian (jy7tian)
- Head of External Affairs: Devin Li (dyli)
- Head of Marketing: Arijit Chowdhury (a49chowd), Sabina Gorbachev (sgorbach)
- Head of Photography: Angela Xu (as2xu)
- Webmaster: Richard Shuai (r2shuai), Darren Lo (dlslo)
- Office Manager: Ivy Lei (ihlei), Kevin Cui (k8cui)
