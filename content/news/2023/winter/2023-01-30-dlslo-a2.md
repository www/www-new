---
author: 'dlslo'
date: 'January 30 2023 00:01'
---

The executive team would like to appoint the following:

- Head Community Representative: Ivy Lei (ihlei)
- Head of Design: Cadey Chen (yx32chen)
- Head of Discord (CodeyBot) Developers: Marcus Chan (mwcchan)
- Head Discord Moderator: Laura Nguyen (l69nguye)
- Heads of Events: Andrea Ma (a49ma), Joseph Perez (j22perez), Naman Chhawchharia (nchhawch)
- Head of External Affairs: Jason D’Souza (j35dsouz)
- Head of Marketing: Joyce Ye (j227ye)
- Head of Photography: Bonnie Peng (b38peng)
- Webmaster: Shahan NedaDahandeh (snedadah)
- Office Manager: Young Wang (y3285wan)
- Librarian: John Oss (joss)