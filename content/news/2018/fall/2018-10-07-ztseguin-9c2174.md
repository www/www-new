---
author: 'ztseguin'
date: 'October 07 2018 01:00'
---

We have been informed of a planned power outage that will affect CSC equipment on Oct. 9 at 11pm until Oct. 10 at 5am.

During this time, all CSC systems and services will be unavailable. Please ensure that you have saved all work before then and that you have a backup of any important files.

We expect that most services will recover automatically once power is restored, however anything requiring manual intervention will be restored later in the day.

If you have any questions or concerns, please contact the Systems Committee by email at [syscom@csclub.uwaterloo.ca](<mailto:syscom@csclub.uwaterloo.ca>).