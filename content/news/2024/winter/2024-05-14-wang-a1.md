---
author: 'Justin Wang'
date: 'May 14 2024 00:00'
---

The Spring 2024 Elections were held Monday, May 13th at 7PM EST. Here are the elected executives for the term!

- President: Gordon Lin (g3lin)
- Vice-President: Justin Wang (yw2wang)
- Assistant Vice-President: Sean Zhang (q434zhan)
- Treasurer: Andrea Ma (a49ma)
- Sysadmin: Nathan Chung (n4chung)

The executive team would like to appoint the following:

- Head of Discord (CodeyBot) Developers: Prabhav Khera (p2khera), Fan Yang (f25yang)
- Head of Class Profile: Alexander Liao (a23liao)
- Head of Design: Anny Wei (a25wei)
- Head Discord Moderator: Roger Cao (r44cao)
- Head of Events: Samuel Bai (sbai), Manasva Katyal (m3katyal)
- Head of External Affairs: Josephina Kim (j292kim)
- Head of Marketing:  Lila Hoang (Lbxhoang)
- Head of Photography: Enming Yang (emyang)
- Head Community Representative: Franklin Ramirez (fgramire)
- Webmaster: Tejas Srikanth (tcsrikan)
- Office Manager: Bryan Wang (b397wang)