---
author: 'pj2melan'
date: 'May 12 2016 01:00'
---

Elections for Spring 2016 have concluded. The following people were elected and ratified:

- President: Luqman Aden (`laden`)
- Vice-president: Melissa Tedesco (`matedesc`)
- Treasurer: Jon Bailey (`jj2baile`)
- Secretary: Aditya Shivam Askothar (`askothar`)
- SysAdmin: Jordan Xavier Pryde (`jxpryde`)
- Office Manager: Zachary Seguin (`ztseguin`)
- Librarian: Charlie Wang (`s455wang`)
- Fridge Regent: Marc Mailhot (`mnmailho`)

<!-- -->