---
author: 'pj2melan'
date: 'January 14 2016 00:00'
---

Elections for Winter 2016 have concluded. The following people were elected:

- President: Patrick Melanson (`pj2melan`)
- Vice-president: Patrick Melanson (`pj2melan`)
- Acting Vice-president: Theo Belaire (`tbelaire`)
- Treasurer: Luqman Aden (`laden`)
- Secretary: Naomi Koo (`m3koo`)
- SysAdmin: Zachary Seguin (`ztseguin`)
- Office Manager: Reila Zheng (`wy2zheng`)
- Librarian: Felix Bauckholt (`fbauckho`)
- Fridge Regent: Marc Mailhot (`mnmailho`)

<!-- -->