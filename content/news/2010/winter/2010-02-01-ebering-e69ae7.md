---
author: 'ebering'
date: 'February 01 2010 00:00'
---

### [Contest now open!](<http://contest.csclub.uwaterloo.ca/>)

This term's programming contest is now open. Check out [the website](<http://contest.csclub.uwaterloo.ca/>). The problem is Tron, and there will be prizes offered, but if you want details you should really check the website.