---
author: 'ebering'
date: 'November 03 2010 01:00'
---

The CSC has been invited to MathSoc's annual Charity Ball. If you are interested in attending with the CSC, please contact Gwynneth Leece [gnleece@csclub.uwaterloo.ca](<mailto:gnleece@csclub.uwaterloo.ca>). The Charitby Ball costs $20 per person and includes a three-course dinner. It is Saturday November 20th at 6pm.