---
author: 'ehashman'
date: 'February 27 2012 00:00'
---

The CSC and AMD are running an [OpenCL programming contest!](<opencl>) If you're interested in winning a laptop or a graphics card, you should [read more](<opencl>) and [register here](<opencl/register>).

**Registration has been reopened** for anyone who did not get a chance to register on time, so make sure you sign up!