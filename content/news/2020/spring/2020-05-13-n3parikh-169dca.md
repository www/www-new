---
author: 'n3parikh'
date: 'May 13 2020 01:00'
---

Due to the closure of campus this term, the Computer Science Club will be holding elections for Spring 2020 virtually. The president, vice-president, treasurer and assistant vice-president (formerly secretary) will be elected and the sysadmin will be ratified. The librarian and office manager will not be appointed, since the office will be closed for the Spring term.

If you'd like to run for any of these positions, please email cro@csclub.uwaterloo.ca. Nominations will close on Wednesday, May 20 2020 at midnight EDT.