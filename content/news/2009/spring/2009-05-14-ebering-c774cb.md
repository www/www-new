---
author: 'ebering'
date: 'May 14 2009 01:00'
---

Some of the talks from the Winter term are available on the website in the [media](<http://csclub.uwaterloo.ca/media/>) section, including the IQC talk and video from the lab tours. Joel Spolsky and Richard Stallman's talks are coming soon.