---
author: 'kspaans'
date: 'May 04 2009 01:00'
---

Kyle (The CRO) has opened the nominations for positions and set the election date. Elections will be held on **Tuesday May 12, 2009 at 4:30 PM** in the Comfy Lounge. [Nominees and voting policy](<http://csclub.uwaterloo.ca/~kspaans/S09-nominees.html>) for the election.