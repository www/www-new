---
title: Acceptable and Unacceptable Use
---

The CSC machines are intended for research, personal projects, and general use in accordance with the aims of the CSC (see the [CSC Constitution](/about/constitution) for further details). Projects that are of interest to the CSC may be given special priority by the CSC Systems Committee.

Users must adhere to the CSC's policies concerning machine usage.

The same policies apply to the CSC Cloud resources.
