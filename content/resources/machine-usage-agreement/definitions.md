---
title: Definitions
---

CSC

- The University of Waterloo [Computer Science Club](/) whose office is located in room 3036/3037 of Mathematics and Computer Building (UW campus), telephone number (519) 888-4657 x3870.

CSC Network

- The network of computer hardware and peripherals owned by, rented to, on loan to, or otherwise under the control of the CSC, including everything under the `csclub.uwaterloo.ca` DNS zone.

CSCF

- The [Computer Science Computing Facility](https://uwaterloo.ca/computer-science-computing-facility/) at the University of Waterloo.

Machine

- Computer, terminal or other piece of hardware.
- Non-CSC machines include CSCF's xterms and printers.

Systems Committee

- An appointed body responsible for the operation of the CSC network and the software that runs on it. A complete description is available in the [CSC Constitution](/about/constitution).

Network Bandwidth

- The percentage of bytes per unit of time that can be handled by the network(s) in question. These networks include the University of Waterloo on-campus network and the Internet.

Computing Resources

- Resources the CSC considers limited include
  - Public temporary disk space
  - Swap space
  - Other commonly held disk space (which may include the home file system)
  - inodes
  - Memory
  - CPU time
  - Processes
  - TTYs and pseudo-TTYs
  - Network bandwidth
  - Ports
  - CSC Cloud Services
