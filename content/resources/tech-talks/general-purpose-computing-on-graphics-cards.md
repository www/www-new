---
index: 22
title: 'General Purpose Computing on Graphics Cards'
presentors:
  - Katie Hyatt
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/kshyatt-gpgpu-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/kshyatt-gpgpu-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/kshyatt-gpgpu.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/kshyatt-gpgpu-480p.mp4'
    type: 'Talk (x246 480p)'
---

GPGPU (general purpose graphics processing unit) computing is an expanding area of interest, with applications in physics, chemistry, applied math, finance, and other fields. nVidia has created an architecture named CUDA to allow programmers to use graphics cards without having to write PTX assembly or understand OpenGL. CUDA is designed to allow for high-performance parallel computation controlled from the CPU while granting the user fine control over the behaviour and performance of the device.

In this talk, I'll discuss the basics of nVidia's CUDA architecture (with most emphasis on the CUDA C extensions), the GPGPU programming environment, optimizing code written for the graphics card, algorithms with noteworthy performance on GPU, libraries and tools available to the GPGPU programmer, and some applications to condensed matter physics. No physics background required!
