---
index: 24
title: 'How to build a brain: From single neurons to cognition'
presentors:
  - Dr. Chris Eliasmith
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/how-to-build-a-brain-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/how-to-build-a-brain-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/how-to-build-a-brain.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/how-to-build-a-brain.mpg'
    type: 'Talk (MPG)'
---

Theoretical neuroscience is a new discipline focused on constructing mathematical models of brain function. It has made significant headway in understanding aspects of the neural code. However, past work has largely focused on small numbers of neurons, and so the underlying representations are often simple. In this talk I demonstrate how the ideas underlying these simple forms of representation can underwrite a representational hierarchy that scales to support sophisticated, structure-sensitive representations.

Theoretical neuroscience is a new discipline focused on constructing mathematical models of brain function. It has made significant headway in understanding aspects of the neural code. However, past work has largely focused on small numbers of neurons, and so the underlying representations are often simple. In this talk I demonstrate how the ideas underlying these simple forms of representation can underwrite a representational hierarchy that scales to support sophisticated, structure-sensitive representations. I will present a general architecture, the semantic pointer architecture (SPA), which is built on this hierarchy and allows the manipulation, processing, and learning of structured representations in neurally realistic models. I demonstrate the architecture on Progressive Raven's Matrices (RPM), a test of general fluid intelligence.
