---
index: 49
title: 'Ralph Stanton 40th Anniversary of Math Faculty Talk'
presentors:
  - Ralph Stanton
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/ralph-stanton-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/ralph-stanton-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ralph-stanton.avi'
    type: 'XviD'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ralph-stanton-xvid.avi'
    type: 'DivX'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ralph-stanton.ogg'
    type: 'Ogg'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ralph-stanton.mpg'
    type: 'MPG'
---

Ralph Stanton reflects on the founding of the University of Waterloo Math Faculty.
