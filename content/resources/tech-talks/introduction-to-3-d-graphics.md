---
index: 51
title: 'Introduction to 3-d Graphics'
presentors:
  - The Prof
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/the-prof-graphics-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/the-prof-graphics-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/the-prof-graphics.avi'
    type: 'DivX'
    size: '272M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/the-prof-graphics-xvid.avi'
    type: 'XviD'
    size: '272M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/the-prof-graphics.mpg'
    type: 'MPG'
    size: '272M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/the-prof-graphics.ogg'
    type: 'Ogg/Theora'
    size: '274M'
---

A talk for those interested in 3-dimensional graphics but unsure of where to start. Covers the basic math and theory behind projecting 3-dimensional polygons on screen, as well as simple cropping techniques to improve efficiency. Translation and rotation of polygons will also be discussed.
