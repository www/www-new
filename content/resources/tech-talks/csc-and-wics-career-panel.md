---
index: 7
title: 'CSC and WiCS Career Panel'
presentors:
  - Joanne McKinley
  - Carol Kilner
  - Harshal Jethwa
  - Dan Collens
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/csc-wics-f15-panel-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/csc-wics-f15-panel.mp4'
    type: 'Talk (x264)'
---

The CSC is joining WiCS to host a career panel! Come hear from Waterloo alumni as they speak about their time at Waterloo, experience with coop, and life beyond the university. A great chance to network and seek advice!

The panelists are:

- Joanne Mckinley - Software Engineer, Google
- Carol Kilner - COO, BanaLogic Corporation
- Harshal Jethwa - Consultant, Infusion
- Dan Collens - CTO, Big Roads

<!-- -->
