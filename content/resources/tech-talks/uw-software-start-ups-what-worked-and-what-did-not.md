---
index: 52
title: 'UW Software Start-ups: What Worked and What Did Not'
presentors:
  - Larry Smith
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk2-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk2-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk2.avi'
    type: 'DivX'
    size: '332M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk2-xvid.avi'
    type: 'XviD'
    size: '332M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk2.mpg'
    type: 'MPG'
    size: '332M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk2.ogg'
    type: 'Ogg/Theora'
    size: '341M'
---

A discussion of software start-ups founded by UW students and what they did that helped them grow and what failed to help. In order to share the most insights and guard the confidences of the individuals involved, none of the companies will be identified.
