---
index: 30
title: 'A brief history of CS curriculum at UW'
presentors:
  - Prabhakar Ragde
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/prabhakar-history-of-uw-cs-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/prabhakar-history-of-uw-cs-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/prabhakar-history-of-uw-cs.avi'
    type: 'Talk (XviD)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/prabhakar-history-of-uw-cs.ogg'
    type: 'Talk (Ogg/Theora)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/prabhakar-history-of-uw-cs.mpg'
    type: 'Talk (MPG)'
---

I'll survey the evolution of our computer science curriculum over the past thirty-five years to try to convey the reasons (not always entirely rational) behind our current mix of courses and their division into core and optional. After some remarks about constraints and opportunities in the near future, I'll open the floor to discussion, and hope to hear some candid comments about the state of CS at UW and how it might be improved.

About the speaker:

Prabhakar Ragde is a Professor in the School of Computer Science at UW. He was Associate Chair for Curricula during the period that saw the creation of the Bioinformatics and Software Engineering programs, the creation of the BCS degree, and the strengthening of the BMath/CS degree.
