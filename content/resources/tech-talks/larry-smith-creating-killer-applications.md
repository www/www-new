---
index: 60
title: 'Larry Smith: Creating Killer Applications'
presentors:
  - Larry Smith
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-killer-applications-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-killer-applications-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-killer-applications.avi'
    type: 'DiVX'
    size: '686M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-killer-applications-xvid.avi'
    type: 'XviD'
    size: '686M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-killer-applications.mpg'
    type: 'MPG'
    size: '685M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-killer-applications.ogg'
    type: 'Ogg'
    size: '706M'
---

A discussion of how software creators can identify application opportunities that offer the promise of great social and commercial significance. Particular attention will be paid to the challenge of acquiring cross domain knowledge and setting up effective collaboration.
