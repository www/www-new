---
index: 44
title: 'Privacy by Design'
presentors:
  - Dr. Ann Cavoukian
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/privacy-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/privacy-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/privacy.avi'
    type: 'XviD'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/privacy.ogg'
    type: 'Ogg/Theora'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/privacy.mp4'
    type: 'MP4'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/privacy.mpg'
    type: 'MPG'
---

Globally, issues about information privacy in the marketplace have emerged in tandem with the dramatic and escalating increase in information stored in electronic formats. Data mining, for example, can be extremely valuable for businesses, but in the absence of adequate safeguards, it can jeopradize informational privacy. Dr. Ann Cavoukian talks about how to use technology to enhance privacy. Some of the technologies discussed included instant messaging, RFID tags and Elliptical Curve Cryptography (ECC). Then Dr. Cavoukian explained the “7 Privacy – Embedded Laws” followed by a discussion on a biometrics solution to encryption.
