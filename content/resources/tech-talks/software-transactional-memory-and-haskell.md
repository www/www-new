---
index: 37
title: 'Software Transactional Memory and Haskell'
presentors:
  - Brennan Taylor
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/b4taylor-stm-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/b4taylor-stm-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/b4taylor-stm.avi'
    type: 'Talk (XviD)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/b4taylor-stm.ogg'
    type: 'Talk (Ogg/Theora)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/b4taylor-stm.mp4'
    type: 'Talk (MP4)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/b4taylor-stm.mpg'
    type: 'Talk (MPG)'
---

Concurrency is hard. Well maybe not hard, but it sure is annoying to get right. Even the simplest of synchronization tasks are hard to implement correctly when using synchronization primitives such as locks and semaphores.



 In this talk we explore what Software Transactional Memory (STM) is, what problems STM solves, and how to use STM in Haskell. We explore a number of examples that show how easy STM is to use and how expressive Haskell can be. The goal of this talk is to convince attendees that STM is not only a viable synchronization solution, but superior to how synchronization is typically done today.
