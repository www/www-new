---
index: 20
title: 'Multi-processor Real-time Systems'
presentors:
  - Bill Cowan
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/wmcowan_multi_processor_realtime-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/wmcowan_multi_processor_realtime-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/wmcowan_multi_processor_realtime.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/wmcowan_multi_processor_realtime.mpg'
    type: 'Talk (MPG)'
---

Programming systems that obey hard real-time constraints is difficult. So is programming multiple CPUs that interact to solve a single problem.

On rare occasions it is possible to mix two difficult problems to create one easy problem and multi-CPU real-time is, on the face of it, just such an occasion. Give each deadline its own CPU and it will never be missed. This intuition is, unfortunately, incorrect, which does not, however, prevent it being tried in many real-time systems.

For three decades, fourth year students have been exploring this problem in CS452, using multiple tasks (virtual CPUs) running on a single CPU. It is now time to consider whether modern developments in CPU architecture make it possible to use multiple CPUs in CS452 given the practical constraint of a twelve week semester.

This talk will describe the nature of computation typical of real-time systems, architectural solutions currently employed in the course, and possible architectures for multi-CPU systems.
