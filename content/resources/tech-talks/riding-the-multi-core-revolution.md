---
index: 53
title: 'Riding The Multi-core Revolution'
presentors:
  - Stefanus Du Toit
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/sdt-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/sdt-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/sdt.avi'
    type: 'DivX'
    size: '406M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/sdt-xvid.avi'
    type: 'XviD'
    size: '406M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/sdt.mpg'
    type: 'MPG'
    size: '405M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/sdt.ogg'
    type: 'Ogg/Theora'
    size: '411M'
---

For decades, mainstream parallel processing has been thought of as inevitable. Up until recent years, however, improvements in manufacturing processes and increases in clock speed have provided software with free Moore's Law-scale performance improvements on traditional single-core CPUs. As per-core CPU speed increases have slowed to a halt, processor vendors are embracing parallelism by multiplying the number of cores on CPUs, following what Graphics Processing Unit (GPU) vendors have been doing for years. The Multi-core revolution promises to provide unparallelled increases in performance, but it comes with a catch: traditional serial programming methods are not at all suited to programming these processors and methods such as multi-threading are cumbersome and rarely scale beyond a few cores. Learn how, with hundreds of cores in desktop computers on the horizon, a local software company is looking to revolutionize the way software is written to deliver on the promise multi-core holds.
