---
index: 46
title: 'PMAMC&OC SASMS - Spring 2007'
presentors:
  - PMClub Various Members
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/pmc-sasms-spring-2007-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/pmc-sasms-spring-2007-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pmc-sasms-spring-2007.avi'
    type: 'XviD'
    size: '643M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pmc-sasms-spring-2007.ogg'
    type: 'Ogg/Theora'
    size: '598M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pmc-sasms-spring-2007.mp4'
    type: 'MP4'
    size: '625M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pmc-sasms-spring-2007.mpg'
    type: 'MPG'
    size: '641M'
---
