---
index: 17
title: 'Google Fiber: Speed is Hard'
presentors:
  - Avery Pennarun
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/google-speed-is-hard.png'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/google-speed-is-hard.png'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/speed-is-hard-at-uwaterloo.pdf'
    type: 'Talk (PDF)'
---

Our speaker, Avery Pennarun, will share some not-very-secret secrets from the team creating GFiber's open source router firmware, including some discussion of wifi, marketing truthiness, the laws of physics, something about coaxial cables, embedded ARM processors, queuing theory, signal processing, hardware design, and kernel driver optimization. If you're lucky, he may also rant about poor garbage collector implementations. Also, there will be at least one slide containing one of those swooshy circle-and-arrow lifecycle diagrams, we promise.
