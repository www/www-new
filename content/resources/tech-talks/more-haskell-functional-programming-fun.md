---
index: 41
title: 'More Haskell functional programming fun'
presentors:
  - Andrei Barbu
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu2-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu2-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu2.avi'
    type: 'XviD'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu2.ogg'
    type: 'Ogg/Theora'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu2.mp4'
    type: 'MP4'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu2.mpg'
    type: 'MPG'
---

TODO
