---
index: 59
title: 'Eric LaForest: Next Generation Stack Computing'
presentors:
  - Eric LaForest
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/ericlaforest-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/ericlaforest-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/eric-laforest2-720-480.avi'
    type: 'DiVX'
    size: '357M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ericlaforest-xvid.avi'
    type: 'XViD'
    size: '309M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/ericlaforest.mpg'
    type: 'Mpeg'
    size: '307M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/CSCtalkMar06.pdf'
    type: 'slides [pdf]'
    size: '1M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/CSCtalkMar06.ppt'
    type: 'slides [Power Point]'
    size: '1M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/CSCtalkMar06.odp'
    type: 'slides [Open Office]'
    size: '1M'
---

Eric LaForest delivers a crash-course on modern stack computing, the Forth programming language, and some projects of his own. Stack systems have faster procedure calls and reduced complexity (shorter pipeline, simpler compilation) relative to their conventional counterparts, as well as more consistent performance, which is very important for real-time systems. Many consider stack-based architecture's crowning feature, however, to be the unrivalled price-to-performance ratio.

Note: the slides are hard to make out in the video, so make sure to download the slides as well.
