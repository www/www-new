---
index: 48
title: 'Usability in the Wild'
presentors:
  - Dr. Michael Terry
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/mterry2-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/mterry2-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/mterry2.avi'
    type: 'XviD'
    size: '521M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/mterry2.ogg'
    type: 'Ogg/Theora'
    size: '535M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/mterry2.mp4'
    type: 'MP4'
    size: '509M'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/mterry2.mpg'
    type: 'MPG'
    size: '520M'
---

What is the typical monitor resolution of a GIMP user? How many monitors do they have? What size images do they work on? How many layers are in their images? The answers to these questions are generally unknown: no means currently exist for open source applications to collect usage data. In this talk, Professor Michael Terry will present ingimp, a version of GIMP that has been instrumented to automatically collect usage data from real-world users. Prof. Terry will discuss ingimp's design, the type of data we collect, how we make the data available on the web, and initial results that begin to answer the motivating questions. ingimp can be found at http://www.ingimp.org.

The slides from the talk are available here: [ingimp\_uw\_csc\_talk\_6\_27\_2007.pdf](<http://mirror.csclub.uwaterloo.ca/csclub/ingimp_uw_csc_talk_6_27_2007.pdf>).
