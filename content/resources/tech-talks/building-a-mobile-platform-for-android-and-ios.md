---
index: 19
title: 'Building a Mobile Platform for Android and iOS'
presentors:
  - Wesley Tarle
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/wtarle_mobile_platform_google-thumb-small.png'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/wtarle_mobile_platform_google.pdf'
    type: 'Talk (PDF)'
---

A Google engineer gives a talk on building a mobile platform for Android and iOS. Wesley Tarle has been leading development at Google in Kitchener and Mountain View, and building stuff for third-party developers on Android and iOS. He's contributed to Google Play services since its inception and continues to produce APIs and SDKs focused on mobile startups.
