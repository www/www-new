---
title: IRC
---

We host an instance of [The Lounge](https://chat.csclub.uwaterloo.ca/) for all of our members. The Lounge is a web-based IRC client which is simple to setup and use. It also has a Progressive Web App available for mobile devices.

For more information, see [this page](https://wiki.csclub.uwaterloo.ca/How_to_IRC) on our wiki.
