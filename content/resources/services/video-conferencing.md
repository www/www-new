---
title: Video Conferencing
---

We host an instance of [BigBlueButton](https://bbb.csclub.uwaterloo.ca/), a free and open-source video conferencing platform. BigBlueButton offers many useful features such as a multi-user whiteboard, breakout rooms, shared notes, and more.

To get the most out of BigBlueButton, you can watch some tutorial videos [here](https://bigbluebutton.org/html5).
