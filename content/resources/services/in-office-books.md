---
title: In-Office Books
---

The CS Club maintains an extensive collection of Computer Science-related books. Feel free to come by the office to take a look at our library.
