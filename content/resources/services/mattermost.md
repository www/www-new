---
title: Mattermost
---

We host an instance of [Mattermost](https://mattermost.csclub.uwaterloo.ca/) for all of our members. Mattermost is an open-source alternative to Slack. We currently bridge the `#csc` channel on libera.chat to Mattermost (if you are looking for a generic web-based IRC client, see [The Lounge](/resources/services/irc)). CSC executives currently use Mattermost for planning and logistics.

If you would like a dedicated Mattermost team for your club, please contact the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca).
