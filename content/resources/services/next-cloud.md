---
title: Nextcloud
---

We host an instance of [Nextcloud](https://files.csclub.uwaterloo.ca/) for all of our members. Nextcloud is an open-source software platform for hosting and storing files similar to Google Drive.