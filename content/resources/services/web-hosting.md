---
title: Web Hosting
---

Many of members take advantage of our web hosting service. Our web server runs on Apache, and has PHP, Python, and Perl modules installed. We also have MySQL and PostgreSQL databases available upon request.

If you are already a member, you can enable your web space as follows:

1. Log in to one of the CSC machines (e.g. csclub.uwaterloo.ca) using an [SSH](http://www.openssh.com/) client (e.g. [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/) on Windows or [OpenSSH](http://www.openssh.com/) on \*nix).
2. Create a directory called `www` in your home directory by typing `mkdir ~/www`. (This directory may already exist.)
3. Put the files you want on your web page in your new `www` directory. `index.{html,php}` will be loaded by default. You can upload files using an scp client (e.g. [WinSCP](http://winscp.net/) on Windows or [OpenSSH](http://www.openssh.com/) on \*nix).
4. Visit your new web page at `http://csclub.uwaterloo.ca/~username/`, where `username` should be replaced by your username.

Examples of configurations for more advanced web hosting setups can be found in [this wiki article](https://wiki.csclub.uwaterloo.ca/Web_Hosting).

If you're still having trouble getting your page up, contact the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca).
