---
title: Mailing Lists
---

Our [csc-general mailing list](https://mailman.csclub.uwaterloo.ca/postorius/lists/csc-general.csclub.uwaterloo.ca/) informs members about our current events.

Our [csc-industry mailing list](https://mailman.csclub.uwaterloo.ca/postorius/lists/csc-industry.csclub.uwaterloo.ca/) allows students to opt-in to receiving emails from industry representatives.

- Corporate events, job postings, info sessions, and related events may be posted here.
