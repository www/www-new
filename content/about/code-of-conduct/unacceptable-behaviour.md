---
title: Unacceptable Behaviour
---

**Unacceptable behaviours include:**

Intimidating, harassing, abusive, discriminatory, derogatory or demeaning speech or actions by any participant in our community online, at all related events and in one-on-one communications carried out in the context of Club business.

- Club event venues may be shared; please be respectful to all patrons of these locations.

**Harassment includes**:

Harmful or prejudicial verbal or written comments related to gender, sexual orientation, race, religion, disability; inappropriate use of nudity and/or sexual images in public spaces (including presentation slides); deliberate intimidation, stalking or following; harassing photography or recording; sustained disruption of talks or other events; inappropriate physical contact, and unwelcome sexual attention.

- Also refer to [Policy 33](https://uwaterloo.ca/secretariat/policies-procedures-guidelines/policy-33) for the definitions of discrimination and harassment.
