---
title: Addressing Grievances
---

If either the complainant or the subject disagree with the decision made by the Handling Officer, they can appeal to the Officers, who can overturn the decision with a majority vote of all the Officers.

No Officer who was personally involved in the complaint, or is in a close relationship with someone involved in the complaint, shall participate in the Officers deliberation or vote on the appeal.
If the subject of a complaint is expelled from the Club, then at their request, but no more often than once a year, the Officers will review the decision to expel them from the Club. The Officers can reinstate the subject with a two-thirds vote of the Officers.
