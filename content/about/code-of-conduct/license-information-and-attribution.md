---
title: License Information and Attribution
---

- The Code of Conduct is distributed under a [Creative Commons Attribution-ShareAlike License](http://creativecommons.org/licenses/by-sa/3.0/), derived from the [Women in Computer Science Code of Conduct](http://wics.uwaterloo.ca/deprecated/code-of-conduct/), the [UW Amateur Radio Club Code of Conduct](http://uwarc.uwaterloo.ca/policies-procedures/code-of-conduct/), and the [FASS Code of Conduct](http://fass.uwaterloo.ca/fassconstitution).
