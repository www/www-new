---
title: Finances
---

1. The Treasurer shall, each term, present to the Executive a financial statement for the previous term. They shall, before the end of the current term, ensure that the records are in a good condition to make this task as easy as possible for the next Treasurer.
2. The Treasurer shall prepare a budget each term, to be presented to MathSoc, and shall be responsible to ensure that the Club is represented at the MathSoc budget meeting.
3. The signing officers shall be the Treasurer, and one of the President or Vice-President.
4. At the end of each term, the President or his/her representative shall ensure that the financial records are complete and accurate.
