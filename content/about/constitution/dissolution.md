---
title: Dissolution
---

1. In the event of dissolution of the Club, all assets of the Club shall be transferred to the Mathematics Society of the University of Waterloo.
