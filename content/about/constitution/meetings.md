---
title: Meetings
---

1. A regular meeting of the Club shall be held each term. This meeting shall be called by the CRO and shall be the election meeting for that term.
2. A special meeting for a motion to remove an executive from office may be called at any time deemed necessary by the Executive Council, by the Faculty Advisor, by any thirty (30) members, or by 1/3 of the Club membership if there are fewer than thirty (30) members.
3. All members shall be notified at least two days prior to a forthcoming meeting of the meeting and of the business to be considered at that meeting. A message to the members' mailing list will be considered sufficient notification, though other forms of notification are also encouraged.
4. The Club shall hold meetings only in places that are open to all members of the Club.
5. The Club membership cannot act except at a general meeting.
6. A quorum necessary for the conduct of business is defined as twenty-five (25) full members or 2/3 of the full membership, whichever is smaller. If an election meeting lacks quorum, then the inquorate meeting can set a date and time for the elections, and can choose to either run the new elections with the same nominations or with a new nomination period (which does not need to meet the usual minimum requirement).
7. A motion to remove an officer, or to call new elections (except at a regular election meeting or in the case of vacancies), requires at least a week's notice; a quorum of fifty (50) full members or 2/3 of the full membership, whichever is smaller; and a 2/3 vote. Any other motion requires a majority vote.
8. If a motion is defeated, it cannot be brought again for sixty (60) days.
