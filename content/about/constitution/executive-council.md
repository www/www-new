---
title: Executive Council
---

1. The Executive Council shall consist of the present officers of the Club and the Faculty Advisor (as a non-voting member) and has the power to run the affairs of this club within the limits of this constitution. This includes the power to overrule or issue directions to any officer.
2. The Executive Council may appoint people to various positions to help manage the Club.
3. If members provide any points of feedback to the Executive Council at a meeting, then the Executive Council shall respond to them.
4. The Executive Council can act by consensus achieved on their mailing list.
5. Minutes of the Executive Council meetings shall be made available for inspection by any member of the Club and shall be filed with the Club records. On request, a member shall be shown the archive of any thread on the Executive Council mailing list which resulted in a decision being made.
