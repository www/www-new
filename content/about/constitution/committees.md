---
title: Committees
---

## Programme Committee

1. The Programme Committee shall be a standing committee chaired by the Vice-President.
2. The Vice-President shall appoint and remove members to and from the Programme Committee as needed.
3. The Programme Committee shall plan and arrange the events of the Club.
4. The Programme Committee shall be responsible to the Executive Council and to the Vice-President.

## Systems Committee
1. The Systems Committee (Syscom) shall be a standing committee chaired by one Systems Administrator (sysadmin) and consisting of non-alumni Systems Committee members. One non-alumni Systems Committee member may also serve as the Terminal Committee Lead. Alumni members may serve as "Systems Committee Emeritus" with limited access under the discretion of the Systems Administrator.
2. The role of Systems Administrator is restricted to individuals who are:
   1. Current students at the University of Waterloo
   2. Previously members of the Systems Committee
   3. In good standing with the club, Systems Committee, and the Executive
   4. Appointed by the Systems Committee and approved by the Executive
3. New members of the Systems Committee shall be appointed at the Systems Administrator's discretion. Members should only be appointed if they demonstrate interest and some existing ability in systems administration, and their appointment is approved by the Systems Administrator.
   1. When a member is added to or removed from the Systems Committee, the Systems Committee and the Executive must be notified via the appropriate mailing lists.
4. Members may be removed from the Systems Committee for cause (including, but not limited to, damage to equipment, neglect of duties, or removal from the club) or if they no longer show interest in systems administration.
5. The Systems Committee, under the leadership of the Systems Administrator, shall:
   1. Operate any and all computer systems in the possession of the Club.
   2. Maintain and upgrade the software on computer systems operated by the Club.
   3. Facilitate the use of equipment operated by the Club.
6. Non-alumni Systems Committee members shall have root access to all machines operated by the Club. Alumni members (Systems Committee Emeritus) may be granted root access under the purview of the current Systems Administrator.
7. In the event there are no Systems Committee members appointable to the position of Systems Administrator, members of the System Committee (alumni and non-alumni) may self-govern and run the committee, working together collectively. The Systems Committee must also appoint a Systems Administrator by the following term. 
   1. The Systems Committee must notify the Executive of all their major decisions.
   2. The current Executive may veto decisions made by the Systems Committee at their discretion. 

## Terminal Committee
1. The Terminal Committee shall be a standing committee chaired by one of the non-alumni Systems Committee members, named the Terminal Committee Lead.
2. New members must be approved by 2/3 of the Systems Committee, or the Terminal Committee Lead. Members should only be appointed if they show interest in systems administration. 
3. Members may be removed without reason as long as 2/3 of the Systems Committee or the Terminal Committee Lead agree.
4. When a member is added, or removed from the Terminal Committee, the Systems Committee are to be notified by email.
5. The Terminal Committee will be led by the Terminal Committee Lead, they shall;
   1. Operate the Terminals within the CSC office, and maintain them.
   2. Members shall gain limited access to any club resources necessary to complete projects assigned by the Systems Committee.
      Including but not limited to; Updating, Configuring Systems, Fixing Errors, Restarting programs, etc.

## Web Committee

1. The Web Committee (webcom) will be a standing committee, chaired by the Webmaster.
2. The Webmaster shall appoint and remove members to and from the Web Committee as needed.
3. The Web Committee shall maintain and develop the club website with infrastructure support from the Systems Committee, if necessary.


## Other Committees

1. The President, with approval of the Executive Council, may appoint such special committees as are deemed necessary.
