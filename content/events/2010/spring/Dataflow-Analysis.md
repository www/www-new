---
name: 'Dataflow Analysis'
short: 'Nomair Naeem, a P.H.D. Student at Waterloo, will be giving a talk about Dataflow Analysis'
startDate: 'July 06 2010 16:30'
online: false
location: 'MC2054'
---

After going through an introduction to Lattice Theory and a formal treatment to Dataflow Analysis Frameworks, we will take an in-depth view of the Interprocedural Finite Distributive Subset (IFDS) Algorithm which implements a fully context-sensitive, inter-procedural static dataflow analysis. Then, using a Variable Type Analysis as an example, I will outline recent extensions that we have made to open up the analysis to a larger variety of static analysis problems and making it more efficient.

The talk is self-contained and no prior knowledge of program analysis is necessary.

