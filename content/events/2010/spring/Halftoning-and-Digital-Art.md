---
name: 'Halftoning and Digital Art'
short: 'Edgar Bering will be giving a talk titled: Halftoning and Digital Art'
startDate: 'July 13 2010 16:30'
online: false
location: 'MC2066'
---

Halftoning is the process of simulating a continuous tone image with small dots or regions of one colour. Halftoned images may be seen in older newspapers with a speckled appearance, and to this day colour halftoning is used in printers to reproduce images. In this talk I will present various algorithmic approaches to halftoning, with an eye not toward exact image reproduction but non-photorealistic rendering and art. Included in the talk will be an introduction to digital paper cutting and a tutorial on how to use the CSC's paper cutter to render creations.

