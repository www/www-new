---
name: 'Mathematics and aesthetics in maze design'
short: '*by Dr. Craig S. Kaplan*. In this talk, I discuss the role of the computer in the process of designing mazes. I present some well known algorithms for maze construction, and more recent research that attempts to novel mazes with non-trivial mathematical or aesthetic properties.'
startDate: 'November 17 2010 16:30'
online: false
location: 'MC4061'
---

For thousands of years, mazes and labyrinths have played an important role in human culture and myth. Today, solving mazes is a popular pastime, whether with pencil on paper or by navigating through a cornfield.

The construction of compelling mazes encompasses a variety of challenges in mathematics, algorithm design, and aesthetics. The maze should be visually attractive, but it should also be an engaging puzzle. Master designers balance these two goals with wonderful results.

In this talk, I discuss the role of the computer in the process of designing mazes. I present some well known algorithms for maze construction, and more recent research that attempts to novel mazes with non-trivial mathematical or aesthetic properties.

