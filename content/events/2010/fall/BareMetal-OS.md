---
name: 'BareMetal OS'
short: '*By Ian Seyler, Return to Infinity*. BareMetal is a new 64-bit OS for x86-64 based computers. The OS is written entirely in Assembly, while applications can be written in Assembly or C/C++. High Performance Computing is the main target application.'
startDate: 'October 04 2010 16:30'
online: false
location: 'MC4021'
---

*By Ian Seyler, Return to Infinity*. BareMetal is a new 64-bit OS for x86-64 based computers. The OS is written entirely in Assembly, while applications can be written in Assembly or C/C++. High Performance Computing is the main target application.

