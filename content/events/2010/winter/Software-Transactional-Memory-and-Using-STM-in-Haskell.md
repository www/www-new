---
name: 'Software Transactional Memory and Using STM in Haskell'
short: 'The fourth Undergraduate Seminar in Computer Science will be presented by Brennan Taylor, a club member. He will be discussing various concurrent computing problems, and introducing Software Transactional Memory as a solution to them.'
startDate: 'March 09 2010 16:30'
online: false
location: 'DC1304'
---

Concurrency is hard. Well maybe not hard, but it sure is annoying to get right. Even the simplest of synchronization tasks are hard to implement correctly when using synchronization primitives such as locks and semaphores.

In this talk we explore what Software Transactional Memory (STM) is, what problems STM solves, and how to use STM in Haskell. We explore a number of examples that show how easy STM is to use and how expressive Haskell can be. The goal of this talk is to convince attendees that STM is not only a viable synchronization solution, but superior to how synchronization is typically done today.

