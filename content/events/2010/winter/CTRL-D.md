---
name: 'CTRL-D'
short: 'Once again the CSC will be holding its traditional end of term dinner. It will be at the Vice President''s house. If you don''t know how to get there meet at the club office at 6:30 PM, a group will be leaving from the MC then. The dinner will be potluck style so bring a dish for 4-6 people, or some plates or pop or something.'
startDate: 'April 01 2010 18:30'
online: false
location: 'CSC Office'
---

Once again the CSC will be holding its traditional end of term dinner. It will be at the Vice President's house. If you don't know how to get there meet at the club office at 6:30 PM, a group will be leaving from the MC then. The dinner will be potluck style so bring a dish for 4-6 people, or some plates or pop or something.

