---
name: 'Contest Closing'
short: 'The [contest](<http://contest.csclub.uwaterloo.ca>) is coming to a close tomorrow, so to finish it in style we will be having ice cream and code friday night. It would be a shame if Waterloo lost (we''re not on top of the [leaderboard](<http://csclub.uwaterloo.ca/contest/rankings.php>) right now) so come out and hack for the home team.'
startDate: 'February 26 2010 19:00'
online: false
location: 'CnD Lounge'
---

The [contest](<http://contest.csclub.uwaterloo.ca>) is coming to a close tomorrow, so to finish it in style we will be having ice cream and code friday night. It would be a shame if Waterloo lost (we're not on top of the [leaderboard](<http://csclub.uwaterloo.ca/contest/rankings.php>) right now) so come out and hack for the home team.

