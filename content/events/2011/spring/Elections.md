---
name: 'Elections'
short: 'CSC Elections have begun for the Spring 2011 term, nominations are open!'
startDate: 'May 09 2011 17:30'
online: false
location: 'Comfy Lounge'
---

It's time to elect your CSC executive for the Spring 2011 term. The elections will be held on Monday May 9th at 5:30PM in the Comfy Lounge on the 3rd floor of the MC. Nominations can be sent to the Chief Returning Officer, [cro@csclub.uwaterloo.ca](<mailto:cro@csclub.uwaterloo.ca>). Nominations will be open until 4:30PM on Monday May 9th. You can also stop by the office in person to write your nominations on the white board.

The executive positions open for nomination are:

- President
- Vice-President
- Treasurer
- Secretary

<!-- -->

 There are also numerous positions that will be appointed once the executive are elected including systems administrator, office manager, and librarian. Everyone is encouraged to run if they are interested, regardless of program of study, age, or experience. If you can't make the election, that's OK too! You can give the CRO a statement to read on your behalf. If you can't make it or are out of town, your votes can be sent to the CRO in advance of the elections. For the list of nominees, watch the CSC website, or ask the CRO.

Good luck to our candidates!

