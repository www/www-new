---
name: 'CTRL-D'
short: 'The end of another term is here, and so we''re having our End-of-Term dinner.	Everybody''s welcome to come to CTRL-D. We are running this like a potluck, so bringing food is suggested.'
startDate: 'July 29 2011 18:00'
online: false
location: 'home of askhader, see abstract'
---

[askhader's](<http://csclub.uwaterloo.ca/~askhader/>) house is at: 


	9 Cardill Cresent


	Waterloo, ON


