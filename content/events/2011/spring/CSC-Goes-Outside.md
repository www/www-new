---
name: 'CSC Goes Outside'
short: 'Do you like going outside? Are you	vitamin-D deficient from being in the MC too long? Do you think	marshmallows, hotdogs, and fire are a delicious combination?If so, you should join us as the CSC is going outside!Around 4PM, we''re going to Columbia Lake for some outdoor fun.	We''ll have Frisbees, kites, snacks, and some drinks. We''ll be	sticking around until dusk, when we''re going to have a campfire	with marshmallows and hotdogs. We plan to be there until 10PM, but	of course you''re welcome to come for any subinterval.'
startDate: 'July 09 2011 16:00'
online: false
location: 'Columbia Lake Firepit'
---

Do you like going outside? Are you	vitamin-D deficient from being in the MC too long? Do you think	marshmallows, hotdogs, and fire are a delicious combination?

If so, you should join us as the CSC is going outside!

Around 4PM, we're going to Columbia Lake for some outdoor fun.	We'll have Frisbees, kites, snacks, and some drinks. We'll be	sticking around until dusk, when we're going to have a campfire	with marshmallows and hotdogs. We plan to be there until 10PM, but	of course you're welcome to come for any subinterval.

