---
name: 'CSC goes to Design Our Tomorrow'
short: '*by Calum T. Dalek*. The Computer Science Club has a limited number of tickets available for the [Design Our Tomorrow Conference](<http://designourtomorrow.com/>) at the University of Toronto on Saturday, November 12, 10:00 - 16:30. See event information for ticket details.'
startDate: 'November 12 2011 07:30'
online: false
location: 'Davis Centre'
---

The Computer Science Club has tickets available for the Design Our Tomorrow Conference at the University of Toronto on Saturday, November 12, 10:00 - 16:30, and would like to invite you to attend. The DOT Conference is a TED-style event geared towards students in high school, undergraduate, and graduate studies. The goal of the event is to inspire young people to create, innovate, better themselves, and in the process, better the world. The conference is free for students and is valued at $500 a ticket for non-students. For more details about the conference, visit [http://designourtomorrow.com/](<http://designourtomorrow.com/>).

Tickets have been reserved for the CSC, and transportation to the conference has been funded by the David R. Cheriton School of Computer Science; a $5 deposit is required to secure a seat on the bus, which will be refunded to attendees upon departure. To sign up, visit the CSC office at MC 3036/3037 with exact change. You will need to provide your full name, e-mail, and student ID number. Please note that students who have already registered for the conference \*should not\* try to register through the CSC. For more details, visit the CSC website at [http://csclub.uwaterloo.ca/](<http://csclub.uwaterloo.ca/>).

This event is not restricted to CSC members—any student is free to attend. Tickets are very limited, so please sign up as soon as possible.

On the morning of November 12, attendees should meet in front of the Davis Center at 7:30 am. The bus will be leaving promptly at 8:00 am, so please arrive no later than 7:30 so we can process refunds and depart on time.

We hope that you will join us.

