---
name: 'Code Party 2'
short: '*by Calum T. Dalek*. The Computer Science Club is having our second code party of the term! Everyone is welcome; please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.'
startDate: 'October 21 2011 19:00'
online: false
location: 'Comfy Lounge'
---

The Computer Science Club is having our second code party of the term! Everyone is welcome; please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.

There will be 3 more code parties this term.

