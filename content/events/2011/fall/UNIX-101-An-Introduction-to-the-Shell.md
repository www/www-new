---
name: 'UNIX 101: An Introduction to the Shell'
short: '*by Calum T. Dalek*. New to Unix? No problem, we''ll teach you to power use circles around your friends!'
startDate: 'September 29 2011 16:00'
online: false
location: 'MC 3004'
---

Unix 101 is the first in a series of tutorials on using Unix. This tutorial will present an introduction to the Unix shell environment, both on the student servers and on other Unix environments. Topics covered include: using the shell, both basic interaction and more advanced topics like scripting and job control, the filesystem and manipulating it, and secure shell. If you feel you're already familiar with these topics, don't hesitate to come to Unix 102 to learn about documents, editing, and other related tasks, or watch out for Unix 103, 104, and 201 that get much more in depth with power tools and software authoring on Unix.

