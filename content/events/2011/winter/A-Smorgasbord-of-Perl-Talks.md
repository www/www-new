---
name: 'A Smorgasbord of Perl Talks'
short: '*by KW Perl Mongers*. These talks are intended for programmers who are curious about the Swiss Army Chainsaw of languages, Perl.'
startDate: 'February 17 2011 19:00'
online: false
location: 'MC2017'
---

Tyler Slijboom will present:

- Prototyping in Perl,
- Perl Default Variables,
- HOWTO on OO Programming, and
- HOWTO on Installing and Using Modules from CPAN

<!-- -->

Daniel Allen will present:

- Coping with Other Peoples' Code

<!-- -->

Justin Wheeler will present:

- Moose: a Modern Perl Framework

<!-- -->

