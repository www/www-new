---
name: 'UNIX 102: Documents and Editing in the Unix environment'
short: '*by Calum T. Dalek*. The next installment in the CS Club''s popular Unix tutorials UNIX 102 introduces powerful text editing tools for programming and document formatting.'
startDate: 'February 02 2011 16:30'
online: false
location: 'MC3003'
---

Unix 102 is a follow up to Unix 101, requiring basic knowledge of the shell. If you missed Unix 101 but still know your way around you should be fine. Topics covered include: "real" editors, document typesetting with LaTeX (great for assignments!), bulk editing, spellchecking, and printing in the student environment and elsewhere. If you aren't interested or feel comfortable with these tasks, watch out for Unix 103 and 104 to get more depth in power programming tools on Unix.

