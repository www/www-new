---
name: 'Code Party'
short: '*by Calum T. Dalek*. Come one, come all to the Code Party happening in the Comfy Lounge this Friday. The event starts at 7:00PM and will run through the night.'
startDate: 'February 04 2011 19:00'
online: false
location: 'Comfy Lounge'
---

Why sleep when you could be hacking on $your\_favourite\_project or doing $something\_classy in great company? Join us for a night of coding and comraderie! Food and caffeine will be provided.

