---
name: 'RES 135'
short: 'Join us for resume and WaterlooWorks tips for the upcoming coop search.'
startDate: 'January 14 2021 19:00'
online: true
location: 'Online'
---

Class is in session and the job search is starting! Want your resume reviewed before applying?

With WaterlooWorks applications opening next week, we want to help you prepare for the coop hunt! Come join us on January 14th at 7pm EST on Twitch to get some WaterlooWorks tips and tricks and watch upper-year/alum students critique your resumes live. Take notes as we provide resume advice from the perspective of engineers currently working in the tech industry!

Drop your resume in the registration link for a chance to get your resume reviewed live!

Not interested in a public critique? You can also drop your resume in the registration link for emailed feedback! More details on the RES135 registration form

The event will be streamed at [twitch.tv/uwcsclub](<https://twitch.tv/uwcsclub>)

Register at [https://forms.gle/KFygHB6N6mmnwWpm7](<https://forms.gle/KFygHB6N6mmnwWpm7>)

