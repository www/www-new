---
name: 'Afterhours: Personal Relationships'
short: 'Join CSC as we talk about personal relationships.'
startDate: 'March 06 2021 19:00'
online: true
location: 'Online'
---

The past year has been tough for all of us, having to deal with the pandemic while studying or working remotely. If you've felt that meeting new people and sustaining relationships with others has never been more challenging, we feel that too, and we want to talk about it.

CSC brings you the third chapter of Afterhours, and this time we're discussing Personal Relationships. Join us for a chat about how our relationships (platonic and romantic) have been affected, whether that be due to co-op, sequence changes, or COVID. We'll be sharing our own personal stories and we'd love for you all to join in on the discussion.

 Registration is required for attendance, so don't miss out! - Event Date: Sat. March 6 at 7 PM EST via Zoom
- Register at [https://forms.gle/Gzeqvg9KpEghCH4H9](<https://forms.gle/Gzeqvg9KpEghCH4H9>)! Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.
- Deadline to Register: Sat. March 6 at 12PM EST

<!-- -->

