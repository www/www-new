---
name: 'Theatre Thursday'
short: 'Come watch a movie with CSC!'
startDate: 'February 18 2021 20:00'
online: true
location: 'Online'
---

Come watch a movie with CSC! Bring your popcorn and your movie commentary, we'll be looking for your input on what we watch so keep an eye out for a poll on our Discord.

This event will be occurring on our Discord: [https://discord.gg/pHfYBCg](<https://discord.gg/pHfYBCg>)

