---
name: 'Alt-Tab'
short: 'Join us for lightning tech talk presented by students.'
startDate: 'March 27 2021 14:30'
online: true
location: 'Online'
---

CSC is proud to present to you Alt-Tab! Join us in a lightning tech talk series presented to you by our very own students. Alt-Tab consists of 10 to 15-minute talks about anything related to tech. Learn more about exciting topics that range from competitive programming to cryptography!

We will have four incredible presenters that are eager to share their insights with you. Stay tuned as we'll be introducing them and the topics that they will be discussing soon!.

Registration is not required to attend! We'll just be sending you an email reminder, as well as inviting you to our calendar event.

Event Date: March 27th EDT via Twitch ([https://www.twitch.tv/uwcsclub](<https://www.twitch.tv/uwcsclub>)) at 2:30-4pm

You can also attend here: [https://live.csclub.uwaterloo.ca/2021-03-27-alt-tab.html](<https://live.csclub.uwaterloo.ca/2021-03-27-alt-tab.html>).

Register at [http://bit.ly/uwcsclub-alt-tab-signup!](<http://bit.ly/uwcsclub-alt-tab-signup!>) Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.

See you all soon!

