---
name: 'Afterhours - Returning from COVID'
short: 'This Afterhours will focus on a discussion about the change from online to in-person interactions and the challenges related to building and rebuilding in-person connections.'
startDate: 'October 23 2021 20:00'
online: true
location: 'Zoom'
poster: 'images/events/2021/fall/afterhours-returning-from-covid.png'
registerLink: https://bit.ly/csc-afterhours-RfC
---
Everyone is welcome to attend our upcoming Afterhours event! These events give us an opportunity to have a conversation and share our experiences about uncomfortable topics, in addition to advice on how to overcome these obstacles.

Even though we've all been awaiting the end of Covid, as activities and opportunities return to being in-person, the transition can be disorienting and challenging; both speakers and participants will have a chance to share their stories and discuss their experiences. 😊 This event will focus on the change from online to in-person interactions and the challenges related to building and rebuilding in-person connections. 🤝

Registration is not required for attendance, we'll just be sending you a calendar invite, as well as an email reminder on the day of the event.

📅 Event Date: Saturday, October 23rd, 8:00- 9:00pm ET via Zoom 💻

Zoom Link:
https://us06web.zoom.us/j/83120730842?pwd=U1RnUDU1Q2tNOUZwOTZwbDdmSWFiQT09

👉 Sign up using the register button. Alternatively, you can also email us at exec@csclub.uwaterloo.ca.

Hope to see you there! 🌸 
