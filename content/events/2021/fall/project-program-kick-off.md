---
name: 'Project Program Kick-Off'
short: 'Attend to get details about Project Program, a month-long event that will have mentors support your team in creating a side-project.'
startDate: 'September 23 2021 19:00'
online: true
location: 'Zoom'
poster: 'images/events/2021/fall/project-program-kickoff.png'
registerLink: https://bit.ly/project-program-kick-off
---

📢 Project Program is back for Fall 2021, and we’re excited to see mentors support you to create a month-long project!

DSC and CSC are collaborating again to help you create your side project by guiding your group of mentees through brainstorming project ideas, creating roadmaps with milestones and achievements, and finally presenting your project for the chance to win prizes! 🏆

📌 The details of the program will be discussed during this event, so if you’re interested in participating, be sure to attend!

📅 Event Date: Thursday, September 23rd from 7:00-8:00pm EDT on [Zoom](https://us06web.zoom.us/j/89144349767?pwd=Z3BZREI0MGNRWXdFWmZMM1JRVU5CQT09). 💻


👉  No need to register, but we'll send you an email alert if you do! Click the register button to sign up. Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.

See you then! 👋
