---
name: 'Alumni Fireside Chat'
short: 'Life After Uni'
startDate: 'November 06 2021 19:00'
online: true
location: 'Zoom'
poster: 'images/events/2021/fall/life-after-uni-fireside-chat.png'
registerLink: https://bit.ly/csc-sesoc-alumni-fireside-chat!
---
CSC and SESoc are proud to present the Alumni Fireside Chat!

An Sheng, Linda Wang, Evy Kassirer, Bilal Akhtar, and Nancy Iskander will be talking about their careers, experiences, and various decisions they have made after graduating university. During the event, you will have the opportunity to listen to the discussion, and then ask questions and chat with alumni! 😊

Registration is optional; we’ll just be sending you a reminder on the day of, as well as a calendar invite. 

📅 Event Date: Nov.6th 2021, 7-8:30pm ET
💻 Zoom: 
Topic: CSC x SESoc Alumni Fireside Chat: Life After Uni
Time: Nov 6, 2021 07:00 PM Eastern Time (US and Canada)

Join Zoom Meeting
https://us06web.zoom.us/j/82525216481?pwd=bW85Qmd0NVdOaXZ3anpNSkhqbEJFQT09

👉 Sign up using the register button. Alternatively, you can also email us at exec@csclub.uwaterloo.ca.

Hope to see you there! 🌸 
