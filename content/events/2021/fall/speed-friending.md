---
name: 'Speed Friending'
short: 'Make some new friends with CSC!'
startDate: 'October 01 2021 20:00'
online: true
location: 'Zoom'
registerLink: https://forms.gle/j7knjxKND6UkfF2U7
---
📢 New fall term, lots of new members! Whether you’re a first year joining us for the first time, or a seasoned friend-making veteran, CSC is happy to announce that our Speed-Friending event is back! 🥰  Come hop on a Zoom call with us to get a chance to connect with others in our community. 🙌

Like last term’s event, you will be able to meet others in groups of 3 or 4, with around 10 minutes to talk. 🎤  An optional prompt will act as an icebreaker, but feel free to discuss anything you want! Remember to connect with the people in your groups if you’d like to stay in contact afterwards!

Registration is optional; we’ll just be sending you a reminder on the day of, as well as a calendar invite. 

📅 Event Date: Oct.1st 2021, 8-9pm ET
💻 Zoom: 
Topic: CSC Social: Speed Friending
Time: Oct 1, 2021 08:00 PM Eastern Time (US and Canada)

Join Zoom Meeting
https://us06web.zoom.us/j/87952442346?pwd=NnRrbU93VlpacTdiK2NXR2l2WDFxQT09

👉 Sign up using the register button. Alternatively, you can also email us at exec@csclub.uwaterloo.ca.

Hope to see you there! 🌸 
