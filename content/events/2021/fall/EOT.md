---
name: 'CSC EOT Social'
short: 'Want to play some fun in-person games? Want to make some new friends? Join us for our first in-person social event, or join us for our online social event which is running at the same time!'
startDate: 'December 5 2021 18:00'
online: false
location: 'DC 1350 & DC 1351'
poster: 'images/events/2021/fall/EOT.png'
registerLink: https://bit.ly/csc-eot-event
---

📢 CSC’s last event of the term is here! Come join us for our EOT event where we will be giving out many free bubble tea vouchers on attendance and talking about how to get involved in Winter 2022. 🎉 We will also be hosting online and in-person games.

✨ Stop by DC1350 / DC1351 from 6:00 - 8:00pm on December 5th! Our in-person and online events are running at the same time, so if you’re not in Waterloo, be sure to head to our Discord at https://discord.gg/pHfYBCg.

Registration is required to attend our in-person event, since there is a maximum of 50 people.

📆 Event Date: Sunday, December 5th at 6:00 - 8:00pm ET at DC1350 / DC1351 or on our Discord.
👉 Register at https://bit.ly/csc-eot-event. Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up.
