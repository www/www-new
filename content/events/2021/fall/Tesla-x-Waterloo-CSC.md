---
name: 'Tesla x Waterloo CSC'
short: 'Come join Tesla on Microsoft Teams for an intimate and informal talk with some of their software engineers!'
startDate: 'November 17 2021 18:00'
online: true
location: 'Zoom'
poster: 'images/events/2021/fall/Tesla-x-Waterloo-CSC.png'
registerLink: https://bit.ly/tesla-x-csc-sign-up-form
---
📢 Interested in an internship to help accelerate the world’s transition to sustainable energy? If you’re looking to intern at Tesla in Summer 2022 or want to learn more, come join Tesla on Microsoft Teams for an intimate and informal talk with some of their software engineers! 💻 A private resume link to apply for Tesla will be provided on attendance.

📅 Event Date: Wednesday, November 17th from 6:00-7:00 pm ET.

👉 Register using the register button with your University of Waterloo email.

Excited to see you at this tech talk! 📌
