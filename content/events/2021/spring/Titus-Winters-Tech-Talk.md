---
name: 'Titus Winters Tech Talk'
short: 'Become inspired in an upcoming talk by Titus Winters on June 10th from 7-8pm ET, who will share how good team culture can positively impact code!'
startDate: 'June 10 2021 19:00'
online: true
location: 'Online'
---

Become inspired in an upcoming talk by Titus Winters on June 10th from 7-8pm ET, who will share how good team culture can positively impact code!

Titus Winters is a senior staff software engineer at Google, the chair of the global subcommittee for the design of the C++ standard library, a teacher and an author. Over the years, Titus has started several Google projects that are believed to be in the top 10 largest refactorings in human history. This unique scale and perspective has informed his thinking on the care and feeding of software systems.

In this tech talk, Titus will discuss the roles of individual contributors, team leads, and managers in fostering a good culture, and show how this has a positive impact on project outcomes. Titus's talk will hit many teamwork and leadership themes of the "Software Engineering at Google" book, and the habits that have made his teams successful.

Zoom: [https://zoom.us/j/97312996497?pwd=Vnl5ZXNFTEJHQkRqMndnR3N0VjlJZz09](<https://zoom.us/j/97312996497?pwd=Vnl5ZXNFTEJHQkRqMndnR3N0VjlJZz09>)

Registration: [http://bit.ly/csc-sesoc-google-tech-talk-signups](<http://bit.ly/csc-sesoc-google-tech-talk-signups>)

