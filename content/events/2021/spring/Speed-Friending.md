---
name: 'Speed-Friending'
short: 'Come hop on a Zoom call with us to to connect with others in our community!'
startDate: 'June 15 2021 20:00'
online: true
location: 'Online'
---

We know a virtual Spring term is making meeting new friends even harder, but we are happy to announce that our Speed-Friending event is back! Come hop on a Zoom call with us to get a chance to connect with others in our community.

Like last term's event, you will be able to meet others in groups of 3 or 4, with around 10 minutes to talk. An optional prompt will act as an icebreaker, but feel free to discuss anything you want! Remember to connect with the people in your groups if you'd like to stay in contact afterwards!

Zoom: https://zoom.us/j/94998577861?pwd=REFJK0FUZXUzWUdlQVRjQVo4WGtJZz09

