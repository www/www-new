---
name: 'Build Networks in Tech!'
short: 'Join us for an evening of connections, collaborations, and conversations that will elevate your professional journey with Scotia Bank, Eclipse Automation and more!'
startDate: 'November 23 2023 18:00'
endDate: 'November 23 2023 20:00'
online: false
location: 'TBD'
poster: 'images/events/2023/fall/1700094272182--Build-Networks-in-Tech.jpg'
registerLink: 'https://docs.google.com/forms/d/e/1FAIpQLSeSfDEHwUlyL17pgd0GrqckJ2RMh06m9CmUqB611H0NpJZ7gA/viewform'
---

💼 Are you hunting for co-op in the near future? Having a hard time finding ways to network with recruiters/alumni? Look no further! Join us at 'Building Networks in Tech', the CSC's exclusive Recruiter Panel Event, where several representatives and alumni from organizations such as Scotiabank, Eclipse Automation and more, deliver a mini-panel and answer your questions! Join us for an evening of connections, and conversations that will elevate your professional journey!

📢 Sign up via the link in our bio to register for the event!

📆 When? November 23rd, 2023 at 6:00pm - 8:00pm EST. (A schedule for the event will be emailed and updated on ig)

📍 Where? Room TBD – check the email you used to register, as well as our Discord and Insta story for updates.

