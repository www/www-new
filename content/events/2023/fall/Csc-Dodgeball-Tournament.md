---
name: 'CSC Dodgeball Tournament'
short: 'Join us for CSC’s very own Dodgeball Tournament!'
startDate: 'November 15 2023 17:00'
endDate: 'November 15 2023 19:30'
online: false
location: 'CIF Gym 3'
poster: 'images/events/2023/fall/1707694707467--CSC-Dodgeball.png'
---

📢 Join us for CSC’s very own Dodgeball Tournament! Play in teams of 5 and compete to be crowned as CSC’s Dodgeball Champions. Sign up via the link in our bio to register for the event either individually or with a team! Those who sign up individually will be assigned teams on the day of the event.

📆 Nov 15th, 5-7:30pm

📍 Where? CIF Gym 3

