---
name: "Trivia Night"
short: "Test your knowledge at CSC's very own trivia night! Answer questions in a variety of categories for fun and prizes!"
startDate: "July 5 2023 19:00"
endDate: "July 5 2023 21:00"
online: false
location: "DC 1351"
poster: "images/events/2023/spring/Trivia-Night.png"
---

🔊 Calling all UWaterloo trivia enthusiasts! This is your chance to put your knowledge to the test and compete against your peers in a night of brain-twisting questions and friendly competition.

⚠️ Sign up as an individual or as a team of 3! Individuals will be matched with a team on Wednesday.

🤔 With questions covering a wide range of topics, not just computer science, this event is sure to keep you on your toes. So gather your friends, bring your thinking caps, and get ready to win!

📅 Date: July 5th, 2023, at 7-9 PM. EST
📍 Location: DC 1351

😋 We can't wait to see you there! All spectators are welcome!