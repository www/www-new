---
name: 'Bootcamp Resume Reviews'
short: 'Get valuable feedback on resumes and mock interviews as a mentee, or help others in the tech community with job search as a mentor!'
startDate: 'May 20 2023 18:00'
endDate: 'May 21 2023 22:00'
online: true
location: 'Discord'
poster: 'images/events/2023/spring/Bootcamp.jpg'
---

CSC is joined by Tech Plus, Data Science Club, Women in Computer Science, and Blueprint to help you put your best foot forward in your next co-op hunt.

⭐️ As a Bootcamp mentee, you will be able to receive valuable feedback on your resume and sharpen your interviewing skills through our mock interviews. You will be matched by field/career interest with one of our many experienced mentors to bring you guidance from various backgrounds!

⭐️ As a Bootcamp mentor, you will have the opportunity to provide resume critiques and/or conduct mock interviews with students to prepare them for their job search. It's a great chance to give back to the student community and share your knowledge!

📅 The Resume Reviews will take place on the weekend of May 13th and 14th, 2023 from 6:00-10:00 p.m. ET both days, and the Mock Interviews will take place on the weekend of May 20th and 21st, 2023 from 6:00-10:00 p.m. ET both days. Both events will be held virtually on our Bootcamp Discord Server.

⏳ The Resume Reviews and Mock Interviews are both drop-in events, which means you can show up at whichever times work best for you within the event period!

⚠️ The deadline to sign up is May 11th, 2023 at 11:59 p.m. ET.

🤩 Don’t miss out on this amazing opportunity! We hope to see you at Bootcamp!!