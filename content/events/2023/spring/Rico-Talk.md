---
name: 'Rico Talk'
short: 'Join us for a Q&A with former president of CSC and current VP architect at Microsoft!'
startDate: 'August 04 2023 17:00'
endDate: 'August 04 2023 18:00'
online: false
location: 'MC 2017'
poster: 'images/events/2023/spring/Rico-Talk.png'
---

📢 Want to hear from a software engineer at Microsoft for almost 30 years and a former CSC president at Waterloo? Join us in CSC’s Q&A event featuring former CSC President Rico Mariani!

🤩 You will have the chance to hear the perspective of a UW alumnus and CSC member from a different generation! Come prepared with any questions about what life was like at UW in the ‘80s, his experiences at Microsoft and Meta, or anything in between!

📆 Event Date: This Friday (August 4) from 5-6 PM

📌 Location: MC 2017

😮 Sign-ups are not mandatory to attend the event, but if you’d like to stay notified for any details and reminders for the event, sign up at https://csclub.ca/rico-ama!

See you then! 👋