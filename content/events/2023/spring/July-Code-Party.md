---
name: 'July Code Party'
short: 'Come hang out at our second Code Party of the term- a traditional event for CSC where people can code, work on projects, as well as socialize over food, and board games!'
startDate: 'July 24 2023 19:00'
endDate: 'July 24 2023 21:00'
online: false
location: 'STC 0020'
poster: 'images/events/2023/spring/July-Code-Party.png'
---

📣 Attention CSC members! Code party is back! Come join us and work on assignments or side projects, study, or just hang out with your friends :D

🌭 We’re serving free Chung Chun rice dogs for those who come to hang out (first come first serve, must be a CSC member!)

📆 Event Date: Monday July 24th, 7-9pm

📌 Location: STC 0020