---
name: 'Code Party'
short: 'Come hang out at our first Code Party of the term- a traditional event for CSC where people can code, work on projects, as well as socialize over pizza, hot chocolate, and board games!'
startDate: 'February 15 2023 19:00'
endDate: 'February 15 2023 21:00'
online: false
location: 'STC 0010'
poster: 'images/events/2023/winter/code-party.png'
registerLink: 'https://docs.google.com/forms/d/e/1FAIpQLSeIVVc_xz2hdJKJRvxgH4aDSrv3oLUWtaJbRQKL-BITY4S4Hg/viewform'
---

📣📣 Attention CSC Members! Come join us for our ❤️ Valentine’s Day ❤️ themed code party! There will be free pizza 🍕and hot chocolate  ☕ served while you are working and playing board games. Come study and hang out with other CSC members!

✅ Register by clicking the register button! 

📅 Event Details : February 15 7-9 pm 

📌Location : STC 0010 

See you there! <3 
