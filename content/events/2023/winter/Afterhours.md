---
name: 'Afterhours'
short: 'Afterhours is back! Come join us for a chill, informal group discussion about a variety of topics, including finding balance, building self-confidence, dealing with imposter syndrome and more!' 
startDate: 'March 21 2023 20:00'
endDate: 'March 21 2023 22:00'
online: false
location: 'SLC Multipurpose Room'
poster: 'images/events/2023/winter/Afterhours.jpg'
---

📣 Afterhours is back!!

😌 Come join us for a chill, informal group discussion about a variety of topics, including finding balance, building self-confidence, dealing with imposter syndrome and burnout, and any other topics you’d like to bring into the conversation!

🤩 You’ll get the chance to hear personal stories from moderators and other attendees, as well as share your own experiences in a close-knit, non-judgmental environment.

🥰 Snacks and drinks will be provided. Hope to see you there :)

📆 Date: Tuesday, March 21, 8-10 PM 

📍 Location: SLC Multipurpose Room