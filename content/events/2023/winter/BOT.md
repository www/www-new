---
name: 'CSC Beginning of Term Kickoff!'
short: 'Kick off the fall term with CSC’s BOT event and meet others in the CS community!'
startDate: 'January 17 2023 19:00'
endDate: 'January 17 2023 21:00'
online: false
location: 'DC 1350'
poster: 'images/events/2023/winter/BOT.jpg'
registerLink: https://docs.google.com/forms/d/1E3oRi3dKhwG4u5n54OPobd053uxJ83TQkxE8XHc9MgE/viewform?edit_requested=true
---

📢 Kick off the winter term ❄️with CSC’s BOT event! Are you interested in attending upcoming CSC events? Want to meet others in the CS community? Come to our beginning of term event!
 
🎉 Come join us for a night of fun games, speed friending, and FREE PIZZA! You’ll also be able to learn more about the types of events CSC runs!
 
📆 When? January 17th 2023 at 7:00 - 9:00pm EST, in DC 1350.
 
👉 Register by clicking the register button!








