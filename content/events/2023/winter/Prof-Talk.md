---
name: 'Prof Talk - Dr. Gautam Kamath'
short: 'Join us for an insightful talk by Dr. Gautam Kamath on his groundbreaking research in Differential Privacy!' 
startDate: 'March 24 2023 17:00'
endDate: 'March 24 2023 18:00'
online: false
poster: 'images/events/2023/winter/Prof-Talk.png'
location: 'MC 2034'
registerLink: 'https://docs.google.com/forms/d/e/1FAIpQLSc4GIEpNAM68-OKp5wZv9C-u6vT2R4dmfv2SHnkQbFziNoNdg/viewform'
---

🌐 Join us for an insightful talk by Dr. Gautam Kamath on his groundbreaking research in Differential Privacy! Followed by a 15-minute Q&A with prof Kamath.

🎓 Don't miss out on the chance to learn about the impact of this technology on the tech community. 👨‍💻

📆 Date: Friday, March 24th, 5-6 PM

📍 Location: MC 2034
