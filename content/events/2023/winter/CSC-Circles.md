---
name: 'CSC Circles'
short: 'Come take part in this term’s Circles event and make new friends. There will be a group reveal and you will get to meet your circle members face-to-face. There will be board games, quiz bowl, and food.'
startDate: 'February 09 2023 19:00'
endDate: 'February 09 2023 21:00'
online: false
location: 'M3 1006'
poster: 'images/events/2023/winter/CSC-Circles.png'
---

📣CSC Circles is Back!

✨Come take part in this term’s circles event and make new friends. There will be a group reveal and you will get to meet your circle members face to face.

There will be board game 🎲, quiz bowl and food🍕

🗓️ Event Date: February 9th from 7-9PM
📍Location: M3 1006

✨If you're interested in being part of a CSC Circle, sign up by ASAP using the link in our bio!

🗓️ Deadline to sign up: February 6th at 11:59 pm ET