---
name: '``Optical Snow'''': Motion parallax and heading computation in densely cluttered scenes. -or- Why Computer Vision needs the Fourier Transform!'
short: 'A talk by Richard Mann; School of Computer Science'
startDate: 'June 17 2004 16:00'
online: false
location: 'MC 2066'
---

When an observer moves through a 3D scene, nearby surfaces move faster in the image than do distant surfaces. This effect, called motion parallax, provides an observer with information both about their own motion relative the scene, and about the spatial layout and depth of surfaces in the scene.

Classical methods for measuring image motion by computer have concentrated on the cases of optical flow in which the motion field is continuous, or layered motion in which the motion field is piecewise continuous. Here we introduce a third natural category which we call \`\`optical snow''. Optical snow arises in many natural situations such as camera motion in a highly cluttered 3-D scene, or a passive observer watching a snowfall. Optical snow yields dense motion parallax with depth discontinuities occurring near all image points. As such, constraints on smoothness or even smoothness in layers do not apply.

We present a Fourier analysis of optical snow. In particular we show that, while such scenes appear complex in the time domain, there is a simple structure in the frequency domain, and this may be used to determine the direction of motion and the range of depths of objects in the scenes. Finally we show how Fourier analysis of two or more image regions may be combined to estimate heading direction.

This talk will present current research at the undergraduate level. All are welcome to attend.

