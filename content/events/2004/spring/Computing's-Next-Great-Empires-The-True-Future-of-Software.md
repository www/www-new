---
name: 'Computing''s Next Great Empires: The True Future of Software'
short: 'A talk by Larry Smith'
startDate: 'May 26 2004 17:30'
online: false
location: 'DC 1350'
---

Larry will challenge conventional assumptions about the directions of computing and software. The role of AI, expert systems, communications software and business applications will be presented both from a functional and commercial point of view. The great gaps in the marketplace will be highlighted, together with an indication of how these vacant fields will become home to new empires.

