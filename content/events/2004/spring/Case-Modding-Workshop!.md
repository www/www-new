---
name: 'Case Modding Workshop!'
short: 'Come and learn how to make your computer 1337!'
startDate: 'July 17 2004 11:30'
online: false
location: 'RCH 308'
---

Are you bored of beige?


 Tired of an overheating computer?


 Is your computer's noise level on par with a jet engine?

Got a nifty modded case?


 Want one?

The Computer Science Club will be holding a Case Modding Workshop to help answer these questions.

There will be demonstrations on how to make a case window, how to paint your case, managing cables and keeping your computer quiet and cool.

The event is FREE and there will be FREE PIZZA. All are welcome!

To help you on your way to getting a wicked computer case, we have a limited number of "Case Modding Starters Kits" available. They come with an LED fan, a fan grill, a sheet of Plexan, thumbscrews, wire ties, and more! They're only $10 and will be on sale at the event. Here's a [picture](<redkit.jpg>).

If you already have a modded case, we encourage you to bring it out and show it off! There will be a prize for the best case!!

We hope to see you there!

This event is sponsored by Bigfoot Computers.

