---
name: 'Managing your home directory using CVS'
short: 'A talk by Simon Law'
startDate: 'March 09 2004 18:00'
online: false
location: 'MC4062'
---

If you have used Unix for a while, you know that you've created configuration files, or dotfiles. Each program seems to want its own particular settings, and you want to customize your environment. In a power-user's directory, you could have hundreds of these files.

Isn't it annoying to migrate your configuration if you login to another machine? What if you build a new computer? Or perhaps you made a mistake to one of your configuration files, and want to undo it?

In this talk, I will show you how to manage your home directory using CVS, the Concurrent Versions System. You can manage your files, revert to old versions in the past, and even send them over the network to another machine. I'll also discuss how to keep your configuration files portable, so they'll work even on different Unices, with different software installed.

