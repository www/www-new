---
name: 'KW Perl Mongers'
short: 'Perl Modules: A look under the hood'
startDate: 'February 18 2004 19:00'
online: false
location: 'DC2305'
---

In Perl, a module is the basic unit of code-reuse. The talk will be mostly a look into GD::Text::Arc, a module written to draw TrueType text around the edge of a circle. The talk will consider:

- using and writing object-oriented perl code
- the Virtue of Laziness: or, reusing other peoples' code.
- writing tests while coding
- beer coasters

<!-- -->

