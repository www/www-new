---
name: 'Graphing webs-of-trust'
short: 'A talk by Simon Law'
startDate: 'March 02 2004 18:00'
online: false
location: 'MC4042'
---

In today's world, people have hundreds of connexions. And you can express these connexions with a graph. For instance, you may wish to represent the network of your friends.

Originally, webs-of-trust were directed acyclic graphs of people who had identified each other. This way, if there was a path between you and the person who want to identify, then you could assume that each person along that path had verified the next person's identity.

I will show you how to generate your own web-of-trust graph using Free Software. Of course, you can also use this knowledge to graph anything you like.

