---
name: 'CSC
Elections'
short: 'Come out and vote for the Fall 2004 executive!'
startDate: 'September 17 2004 16:00'
online: false
location: 'The Comfy Lounge'
---

The Computer Science Club will be holding its elections for the Fall 2004 term on Friday, September 17. The elections will be held at 4:00 PM in the Comfy Lounge, on the 3rd floor of the MC. Please remember to come out and vote!

We are accepting nominations for the following positions: President, Vice-President, Treasurer, and Secretary. The nomination period continues until 4:30 PM on Thursday, September 16. If you are interested in running for a position, or would like to nominate someone else, please email cro@csclub.uwaterloo.ca before the deadline.

