---
name: 'Knitting needles, hairpins and other tangled objects'
short: 'In this talk, I''ll study linkages (objects built from sticks that are connected with flexible joints), and explain some interesting examples that can or cannot be straightened out'
startDate: 'December 01 2004 14:30'
online: false
location: 'MC 4058'
---

In this talk, I'll study linkages (objects built from sticks that are connected with flexible joints), and explain some interesting examples that can or cannot be straightened out

