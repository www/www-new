---
name: 'Changing the World Conference'
short: 'Organized by Queen''s students, Changing the World aims to bring together the world''s greatest visionaries to inspire people to innovate and better our world. Among these speakers include Nobel Peace Prize winner, Eric Chivian. He was a recipient for his work on stopping nuclear war.'
startDate: 'November 15 2008 06:30'
online: false
location: 'Toronto'
---

Organized by Queen's students, Changing the World	aims to bring together the world's greatest visionaries to	inspire people to innovate and better our world. Among these	speakers include Nobel Peace Prize winner, Eric Chivian. He	was a recipient for his work on stopping nuclear war.

The conference is modeled after TED (Technology,	Entertainment, Design), an annual conference uniting the	world's most fascinating thinkers and doers, and like TED,	each speaker is given 18 minutes to give the talk of their	lives.

Specifically for students in CS/Math, 50 tickets have	been reserved (non-students: $500). For those who would like	to attend, please pick up your ticket in the Computer Science	Club office. The tickets are limited and they are first come	first serve.

