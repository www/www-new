---
name: 'A Brief History of Blackberry and the Wireless Data Telecom Industry'
short: 'Tyler Lessard'
startDate: 'February 08 2008 16:30'
online: false
location: 'MC4042'
---

Tyler Lessard from RIM will present a brief history of BlackBerry technology and will discuss how the evolution of BlackBerry as an end-to-end hardware, software and services platform has been instrumental to its success and growth in the market. Find out how the BlackBerry service components integrate with wireless carrier networks and get a sneak peek at where the wireless data market is going.

