---
name: 'Quantum Information Processing'
short: 'Raymond Laflamme'
startDate: 'February 29 2008 17:00'
online: false
location: 'BFG2125'
---

Information processing devices are pervasive in our society; from the 5 dollar watches to multi-billions satellite network. These devices have allowed the information revolution which is developing around us. It has transformed not only the way we communicate or entertain ourselves but also the way we do science and even the way we think. All this information is manipulated using the classical approximation to the laws of physics, but we know that there is a better approximation: the quantum mechanical laws. Would using quantum mechanics for information processing be an impediment or could it be an advantage? This is the fundamental question at the heart of quantum information processing (QIP). QIP is a young field with an incredible potential impact reaching from the way we understand fundamental physics to technological applications. I will give an overview of the Institute for Quantum Computing, comment on the effort in this field at Waterloo and in Canada and, time permitted visit some of the IQC labs.

