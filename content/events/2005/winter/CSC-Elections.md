---
name: 'CSC
Elections'
short: 'Come out and vote for the Winter 2005 executive!'
startDate: 'January 13 2005 16:30'
online: false
location: 'The Comfy Lounge'
---

The Computer Science Club will be holding its elections for the Winter 2005 term on Thursday, January 13. The elections will be held at 4:30 PM in the Comfy Lounge, on the 3rd floor of the MC. Please remember to come out and vote!

We are accepting nominations for the following positions: President, Vice-President, Treasurer, and Secretary. The nomination period continues until 4:30 PM on Wednesday, January 12. If you are interested in running for a position, or would like to nominate someone else, please email cro@csclub.uwaterloo.ca before the deadline.

