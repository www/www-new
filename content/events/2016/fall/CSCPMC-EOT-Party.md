---
name: 'CSC/PMC EOT Party'
short: 'The CSC and the PMAMC&OC (aka pure math club) are hosting our end of term events together! We''ll be taking over MC Comfy to hang out, eat lots of food (from Kismet!), and play board games.'
startDate: 'December 05 2016 18:00'
online: false
location: 'MC Comfy'
---

The CSC and the PMAMC&OC (aka pure math club) are hosting our end of term events together! We'll be taking over MC Comfy to hang out, eat lots of food (from Kismet!), and play board games.

