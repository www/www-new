---
name: 'ALT+TAB Talks'
short: 'Various members of the CSC will be giving brief, 25 minute talks on CS-related topics. An list of the talks being delivered can be found if you follow the event page link in this description. There will be food provided.'
startDate: 'November 30 2016 18:00'
online: false
location: 'MC 4063'
---

The CSC is hosting ALT+TAB this Wednesday. ALT+TAB is similar to the PMC's SASMS events; several members of the CSC will give brief, 25 minute talks on various interesting topics in CS. There will be food provided at the event. The talks being delivered are:

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

