---
name: 'Code Party'
short: 'The CS Club is having its termly code party! Come out and work on projects, assignments, and more. Food is provided!'
startDate: 'February 04 2016 18:00'
online: false
location: 'STC 0010'
---

Want help installing Linux? Bring a USB, we'll help you. Want to work on a project, CS homework, or an IRC bot? Come over, we'll have food. Want to see what it's like to be in the new STC? Plugs at every desk, I'm telling you. (This term it's going to be in the new STC not in the comfy. We're going for some adventure this term.)

Be there, we'll have dinner!

