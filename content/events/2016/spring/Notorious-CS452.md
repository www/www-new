---
name: 'Notorious CS452'
short: 'Bill Cowan is the Director of the Computer Graphics Lab, and	teaches the notorious CS452, lovingly known as the trains course	by CS students. He will be giving a talk on that very course.'
startDate: 'July 21 2016 18:00'
online: false
location: 'MC 4045'
---

CS452, aka the trains course, has for some time enjoyed notoriety	as a playground for over-achieving masochists. To maintain its	reputation it receives a periodic upgrade, which is now due. This	talk discusses possible directions for the upgrade in the context	of the philosophy that has guided its evolution over the decades	of its existence.

