---
name: 'Spring 2016 Elections'
short: 'The Computer Science Club will be holding elections for the Spring 2016 for President, Vice-President, Secretary, and Treasurer. Additionally, librarian, office manager, sysadmin, and fridge regent will be appointed and ratified.'
startDate: 'May 12 2016 19:00'
online: false
location: 'MC 3001 (Comfy)'
---

The Computer Science Club will be holding elections for the Spring 2016 term on Thursday, May 12th at 19:00 in the MC Comfy (MC 3001). During the meeting, the president, vice-president, treasurer and secretary will be elected, the sysadmin will be appointed and ratified, and the librarian and office manager will be appointed. There may be timbits.

If you'd like to run for any of these positions or nominate someone, you can put your name in a special box on top of the fridge in the CSC office (MC 3036/3037) or send me (Patrick) an email at cro@csclub uwaterloo.ca. It is highly recommended to send me an email in addition to nominating yourself by paper in the office. You can also deposit nominations in the CSC mailbox in MathSoc or present them to me in person. Nominations will close at 19:00 on Wednesday, May 11th (24 hours before the start of elections).

Voting is done heads-down hands-up, and is restricted to Mathsoc social members.

For the part of the constitution pertaining to elections, see http://csclub.uwaterloo.ca/about/constitution#officers

All members are welcome to run! Especially new members and anyone interested in being a new exec! Most of the roles have a small guide on the wiki at https://wiki.csclub.uwaterloo.ca/Exec\_Manual and I will print out a hard copy of a more comprehensive exec manual and bind it myself, I swear.

