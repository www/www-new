---
name: 'Feynman Messenger Lecture Series'
short: '*by Calum T. Dalek*. Join the Computer Science Club and PhysClub every Wednesday evening for the rest of the term for our five screenings of the classic 1964 Messenger Lecture Series by Richard Feynman in PHY 150. Dinner provided!'
startDate: 'March 07 2012 17:30'
online: false
location: 'PHY 150'
---

The Physics Club and the Computer Science Club are proud to present the 1964 Feynman Messenger Lecture Series in PHY 150 on Wednesday evenings at 5:30 PM. The screenings will be taking place as follows (please note times and dates):

- **Feb. 29, 5:30-6:30 PM:** *Law of Gravitation: An Example of Physical Law*
- **Mar. 7, 5:30-7:30 PM:***The Relation of Mathematics and Physics* and *The Great Conservation Principles* (double feature)
- **Mar. 14, 5:30-6:30 PM:***Symmetry in Physical Law*
- **Mar. 21, 5:30-7:30 PM:***The Distinction of Past and Future* and *Probability and Uncertainty: The Quantum Mechanical View* (double feature)
- **Mar. 28, 5:30-6:30 PM:***Seeking New Laws*

<!-- -->

Dinner will be provided, so come on out, relax in the comfy PHY 150 theatre, and enjoy. Hope to see you there!

