---
name: 'Code Party 0'
short: '*by Calum T. Dalek*. The Computer Science Club is running the first code party of the term! Come join us and hack on open source software, your own projects, or whatever comes up. Everyone is welcome; please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.'
startDate: 'January 27 2012 18:30'
online: false
location: 'Math CnD'
---

The Computer Science Club is running the first code party of the term! Come join us and hack on open source software, your own projects, or whatever comes up. Everyone is welcome; please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.

