---
name: 'OpenCL Code Party'
short: '*by Calum T. Dalek*. The University of Waterloo Computer Science Club and AMD''s OpenCL programming competition comes to a close, as the contest ends at midnight and prizes are awarded! Open submissions will be judged, so make sure to come out and watch.'
startDate: 'March 02 2012 19:00'
online: false
location: 'MC 3001'
---

The University of Waterloo Computer Science Club and AMD's [OpenCL programming competition](<http://csclub.uwaterloo.ca/opencl>) comes to a close, as the contest ends at midnight and prizes are awarded! Open submissions will be judged, so make sure to come out and watch.

