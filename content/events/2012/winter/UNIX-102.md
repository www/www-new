---
name: 'UNIX 102'
short: '*by Calum T. Dalek*. The Computer Science Club will be running the second installment of our introductory UNIX tutorials for the term. We will be covering topics intended to show off the development-friendliness of the UNIX computing environment: "real" document editors, development tools, bash scripting, and version control.'
startDate: 'March 08 2012 17:30'
online: false
location: 'MC 3003'
---

New to the UNIX computing environment? If you seek an introduction, look no further. We will be covering more advanced topics in the second installment of our introductory tutorials, that will help you become a more effective developer.

We will be introducing "real" document editors, bash scripting, and version control. We'll prove to you how much more efficient you can develop with these tools and teach you how to do it for yourself. It will save you hours of work!

