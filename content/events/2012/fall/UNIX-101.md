---
name: 'UNIX 101'
short: '*by Calum T. Dalek*. New to the Unix computing environment? If you seek an introduction, look no further. We will be holding a series of tutorials on using Unix, beginning with Unix 101 this upcoming Monday. Topics that will be covered include basic interaction with the shell and use of myriad powerful tools.'
startDate: 'November 19 2012 15:30'
online: false
location: 'MC 3003'
---

New to the Unix computing environment? If you seek an introduction, look no further. We will be holding a series of tutorials on using Unix, beginning with Unix 101 this upcoming Monday. Topics that will be covered include basic interaction with the shell and use of myriad powerful tools.

If you're interested in attending, make sure you can log into the Macs on the third floor, or show up to the CSC office (MC 3036) 20 minutes early for some help. If you're already familiar with these topics, don't hesitate to come to Unix 102, which will be held the week of the 26th.

