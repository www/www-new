---
name: 'Code Party 1: FROSH-A-THON'
short: 'The Computer Science Club is running our first "welcome back" code party of the term! Whether you''re a hacking guru or a newbie to computer science, you''re welcome to attend; there will be activities for all! Our party is loosely themed as a Linux installfest, where we will have a team of members dedicated to helping individuals install and learn to use one of many flavours of Linux.'
startDate: 'September 14 2012 19:00'
online: false
location: 'MC 3001'
---

The Computer Science Club is running our first "welcome back" code party of the term! Whether you're a hacking guru or a newbie to computer science, you're welcome to attend; there will be activities for all! Our party is loosely themed as a Linux installfest, where we will have a team of members dedicated to helping individuals install and learn to use one of many flavours of Linux. Everyone is welcome, so please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.

