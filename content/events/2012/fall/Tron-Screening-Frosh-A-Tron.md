---
name: 'Tron Screening: Frosh-A-Tron'
short: 'ehashman''s lousy frosh event naming scheme continues as we prepare for this week''s movie night---a screening of the original TRON in PHY 150. Come watch the groundbreaking film that defined the role of computer graphics and the quality of special effects in modern cinema. And bring your friends!'
startDate: 'September 28 2012 19:00'
online: false
location: 'PHY 150'
---

ehashman's lousy frosh event naming scheme continues as we prepare for this week's movie night---a screening of the original TRON in PHY 150. Come watch the groundbreaking film that defined the role of computer graphics and the quality of special effects in modern cinema. And bring your friends!

