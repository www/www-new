---
name: 'The
Cryptographic and Game-Theoretical Fundamentals behind Bitcoin'
short: '*by Vitalik Buterin.* In this talk, we will cover the cryptographic and game-theory principles behind the currency, including how the issues of double-spending, the "51% attack," and "mining" are addressed, the game-theory incentives to use Bitcoins honestly, and other issues being faced today in practice, such as implementation, attacks, and future scalability.'
startDate: 'October 18 2012 16:00'
online: false
location: 'MC 2034'
---

*by Vitalik Buterin.* Interested in learning more about Bitcoin, the independent digital cryptographic cash? Then this is the talk for you!

In his talk, Vitalik will cover the cryptographic and game-theory principles behind the currency, including how the issues of double-spending, the "51% attack," and "mining" are addressed, the game-theory incentives to use Bitcoins honestly, and other issues being faced today in practice, such as implementation, attacks, and future scalability.

Refreshments will be provided.

