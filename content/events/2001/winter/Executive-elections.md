---
name: 'Executive elections'
short: 'Winter 2001 CSC Elections.'
startDate: 'January 15 2001 16:30'
online: false
location: 'MC3036'
---

Would you like to get involved in the CSC? Would you like to	have a say in what the CSC does this term? Come out to the CSC	Elections! In addition to electing the executive for the	Winter term, we will be appointing office staff and other	positions. Look for details in uw.csc.

Nominations for all positions are being taken in the CSC	office, MC 3036.

