---
name: 'Debian in the Enterprise'
short: 'A talk by Simon Law'
startDate: 'October 17 2002 17:30'
online: false
location: 'MC2065'
---

The Debian Project produces a "Universal Operating System" that is comprised entirely of Free Software. This talk focuses on using Debian GNU/Linux in an enterprise environment. This includes:

- Where Debian can be deployed
- Strategic advantages of Debian
- Ways for business to give back to Debian

<!-- -->

