---
name: 'A GNU Approach to Virtual Memory Management in a Multiserver Operating System'
short: 'Neal Walfield, a GNU Hurd developer, talks about a possible Virtual Memory Management subsystem for the GNU Hurd'
startDate: 'October 26 2002 16:30'
online: false
location: 'MC2066'
---

Virtual memory management is one of the cornerstones of multiuser operating systems. Most systems available today place all of the policy in a monolithic virtual memory manager, VMM, isolated from the rest of the system. Although secure and lightweight, users have no way to communicate their anticipated memory needs and usage to the system pager. As a result, the VMM can only implement a global paging policy (typically, an approximation of LRU) which may be good on average but is best for nobody.

With the port of Hurd to the L4 microkernel, this situation is being readdressed. Due to its more distributed nature, a centralized resource manager is not only more difficult to implement efficiently but also contrary to the philosophy of the rest of the system. We are currently exploring a model whereby each program is fully self-paged and all compete for memory from a physical memory server. This talk will first discuss how paging currently works in Mach and other systems. An argument for an external paging policy will then be presented followed by the requirements of such a design and the design itself.

---

Neal Walfield, a GNU Hurd developer, is from the University of Massachusetts Lowell. Neal spent the summer of 2002 at University of Karlsruhe working on porting the GNU Hurd to L4.

