---
name: 'GNU/Linux InstallFest with KW-LUG and UW-DIG'
short: 'Bring over your computer and we''ll help you install GNU/Linux'
startDate: 'November 02 2002 11:00'
online: false
location: 'MC3002 (Math Coffee and Donut Store)'
---

The [CSC](<http://www.csclub.uwaterloo.ca/>), the [KW-Linux User Group](<http://www.kwlug.org/>), and the [UW Debian Interest Group](<http://uw-dig.uwaterloo.ca/>) are jointly hosting a GNU/Linux InstallFest. GNU/Linux is a powerful, free operating system for your computer. It is mostly written by talented volunteers who like to share their efforts and help each other.

Perhaps you have are you interested in installing GNU/Linux. If so, bring your computer, monitor and keyboard; and we will help you install GNU/Linux on your machine. You can also find knowledgeable people who can answer your questions about GNU/Linux.

---

### Frequently Asked Questions

**Q: **What is GNU/Linux?


**A: **GNU/Linux is a free operating system for your computer. It is mostly written by talented volunteers who like to share their efforts.

**Q: **Free?


**A: **GNU/Linux is available for zero-cost. As well, it allows you such freedom to share it with your friends, or to modify the software to your own needs and share that with your friends. It's very friendly.

**Q: **What is an InstallFest?


**A: **An InstallFest is a meeting where volunteers help people install GNU/Linux on their computers. It's also a place to meet users, and talk to them about running GNU/Linux.

**Q: **What kind of computer do I need to use GNU/Linux?


**A: **Almost any recent computer will do. If you have an old machine kicking around, you can install GNU/Linux on it as well. If it is at least 5 years old, it should be good enough.

**Q: **Can I have Windows and GNU/Linux on the same computer?


**A: **If you can run Windows now, and you have an extra gigabyte (GB) of disk space to spare; then it should be possible.

**Q: **What should I bring if I want to install GNU/Linux?


**A: **You will want to bring:

1. Computer
2. Monitor and monitor cable
3. Power cords
4. Keyboard and mouse

<!-- -->

