---
name: 'Metaprogramming GPUs'
short: 'A talk by Michael McCool of the Computer Graphics Lab.'
startDate: 'November 19 2002 16:30'
online: false
location: 'MC4058'
---

Modern graphics accelerators, or "GPUs", have embedded high-performance programmable components in the form of vertex and fragment shading units. Recently, these units have evolved from 8-bit computations to floating-point, and other operations provide array gather, scatter, and summation. These capabilities make GPUs akin to array processors of the past, but with a difference: every PC now has one! I am interested in finding the best way to exploit this computational capacity for not only graphics but for general-purpose computation.

Current APIs permit specification of the programs for GPUs using an assembly-language level interface. Compilers for high-level shading languages are available, such as NVIDIA's Cg, and OpenGL 2.0 and DirectX will also include standardized shading languages. This talk will review these. However, compilers for these languages read in an external string specification, which can be inconvenient.

However, it is possible, using standard C++, to define a high-level shading language directly in the API. Such a language can be nearly indistinguishable from a special-purpose programming language, yet permits more direct interaction with the specification of textures (arrays) and parameters, simplifies implementation, and enables on-the-fly generation, manipulation, and specialization of shader programs. A shading language built into the API also permits the lifting of C++ host language type, modularity, and scoping constructs into the shading language without any additional implementation effort. Such an embedded language could be used to program other embedded processors (such as DSP chips in sound cards) or even to generate machine language on the fly for the host CPU.

