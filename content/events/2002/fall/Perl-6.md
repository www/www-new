---
name: 'Perl 6'
short: 'A talk by Simon Law'
startDate: 'November 21 2002 18:00'
online: false
location: 'MC2066'
---

Perl, the Practical Extraction and Reporting Language can only	be described as an eclectic language, invented and refined by	a deranged system administrator, who was trained as a	linguist. This man, however, has declared:

> * Perl 5 was my rewrite of Perl. I want Perl 6 to be the community's rewrite of Perl and of the community.	*

> 
> \--- Larry Wall

Whenever a language is designed by a committee, it is common	wisdom to avoid it. Not so with Perl, for it cannot get	worse. However strange these Perl people seem, Perl 6 is a	good thing coming. In this talk, I will demonstrate some Perl	5 programs, and talk about their Perl 6 counterparts, to show	you that Perl 6 will be cleaner, friendlier, and prettier.

