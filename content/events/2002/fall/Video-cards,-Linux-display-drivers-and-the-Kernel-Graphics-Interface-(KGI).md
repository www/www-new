---
name: 'Video cards, Linux display drivers and the Kernel Graphics Interface (KGI)'
short: 'A talk by Filip Spacek, KGI developer'
startDate: 'October 08 2002 16:30'
online: false
location: 'MC4045'
---

 Linux has proven itself as a reliable operating system but arguably, it still lacks in support of high performance graphics acceleration. This talk will describe basic components of a PC video card and the design and limitations the current Linux display driver architecture. Finally a an overview of a new architecture, the Kernel Graphics Interface (KGI), will be given. KGI attempts to solve the shortcomings of the current design, and provide a lightweight and portable interface to the display subsystem. 