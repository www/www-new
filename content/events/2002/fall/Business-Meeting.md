---
name: 'Business Meeting'
short: 'Vote on a constitutional change.'
startDate: 'September 30 2002 18:30'
online: false
location: 'Comfy lounge, MC3001'
---

The executive has unanimously decided to try to change our constitution to comply with MathSoc policy. The clause we are trying to change is the membership clause. The following is the proposed new reading of the clause.

*	In compliance with MathSoc regulations and in recognition of the club being primarily targeted at undergraduate students, full membership is open to all undergraduate students in the Faculty of Mathematics and restricted to the same.*

The proposed change is illustrated [on	a web page](<http://www.csclub.uwaterloo.ca/about/constitution-change-20020920>).

There will be a business meeting on 30 Sept 2002 at 18:30 in	the comfy lounge, MC 3001. Please come and vote

