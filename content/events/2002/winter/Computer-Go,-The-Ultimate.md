---
name: 'Computer Go, The Ultimate'
short: 'Thomas Wolf from Brock University will be holding a talk on the Asian game of Go. All are welcome.'
startDate: 'March 01 2002 17:00'
online: false
location: 'MC4060'
---

The Asian game go is unique in a number of ways. It is the oldest board game known. It is a strategy game with very simple rules. Computer programs are very weak despite huge efforts and prizes of US$ > 1.5M for a program beating professional players. The talk will quickly explain the rules of go, compare go and chess, mention various attempts to program go and describe our own efforts in this field. Students will have an opportunity to solve computer generated go problems. Prizes will be available.

