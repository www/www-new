---
name: 'An Introduction to GNU Hurd'
short: 'Bored of GNU/Linux? Try this experimental operating system!'
startDate: 'January 26 2002 14:00'
online: false
location: 'Comfy Lounge MC3001'
---

GNU Hurd is an operating system kernel based on the microkernel architecture design. It was the original GNU kernel, predating Linux, and is still being actively developed by many volunteers.

The Toronto-area Hurd Users Group, in co-operation with the Computer Science Club, is hosting an afternoon to show the Hurd to anyone interested. Jeff Bailey, a Hurd developer, will give a presentation on the Hurd, followed by a GnuPG/PGP keysigning party. To finish it off, James Morrison, also a Hurd developer, will be hosting a Debian GNU/Hurd installation session.

All interested are invited to attend. Bring your GnuPG/PGP fingerprint and mail your key to sjdutoit@uwaterloo.ca with the subject \`\`keysigning'' (see separate announcement).

Questions? Suggestions? Contact [James Morrison](<ja2morri@uwaterloo.ca>).

