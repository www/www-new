---
name: 'GnuPG/PGP Keysigning Party'
short: 'Get more signatures on your key!'
startDate: 'January 26 2002 14:30'
online: false
location: 'Comfy Lounge MC3001'
---

GnuPG and PGP provide public-key based encryption for e-mail and other electronic communication. In addition to preventing others from reading your private e-mail, this allows you to verify that an e-mail or file was indeed written by its perceived author.

In order to make sure a GnuPG/PGP key belongs to the respective person, the key must be signed by someone who has checked the user's key fingerprint and verified the user's identification.

A keysigning party is an ideal occasion to have your key signed by many people, thus strengthening the authority of your key. Everyone showing up exchanges key signatures after verifying ID and fingerprints. The Computer Science Club will be hosting such a keysigning party together with the Hurd presentation by THUG (see separate announcement). See [ the keysigning party homepage](<http://www.student.math.uwaterloo.ca/~sjdutoit/>) for more information.

Before attending it is important that you mail your key to sjdutoit@uwaterloo.ca with the subject \`\`keysigning.'' Also make sure to bring photo ID and a copy of your GnuPG/PGP fingerprint on a sheet of paper to the event.

