---
name: 'DVD-Video Under Linux'
short: 'Billy Biggs will be holding a talk on DVD technology (in particular, CSS and playback issues) under Linux, giving some technical details as well as an overview of the current status of Free Software efforts. All are welcome.'
startDate: 'February 13 2002 16:00'
online: false
location: 'MC4060'
---

DVD copy protection: Content Scrambling System (CSS)

- A technical introduction to CSS and an overview of the ongoing legal battle to allow distribution of non-commercial DVD players
- The current Linux software efforts and open issues
- How applications and Linux distributions are handling the legal issues involved

<!-- -->

DVD-Video specifics: Menus and navigation

- An overview of the DVD-Video standard
- Reverse engineering efforts and their implementation status
- Progress of integration into Linux media players

<!-- -->

