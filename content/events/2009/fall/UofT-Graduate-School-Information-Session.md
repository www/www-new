---
name: 'UofT Graduate School Information Session'
short: '"Is Graduate School for You?" Get the answers to your grad school questions - and have a bite to eat, our treat'
startDate: 'October 14 2009 14:30'
online: false
location: 'DC1304'
---

Join Prof. Greg Wilson, faculty member in the Software Engineering research group in the UofT's Department of Computer Science, as he gives insight into studying at the graduate level-what can be expected, what does UofT offer, is it right for you? Pizza and pop will be served. **Come see what grad school is all about!**. All undergraduate students are welcome; registration is not required.

For any questions about the program, visit [UofT's website](<http://www.cs.toronto.edu/dcs/prospective-grad.html>). This event is not run by the CS Club, and is announced here for the benefit of our members.

