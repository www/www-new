---
name: 'CTRL-D'
short: 'Join the Club That Really Likes Dinner for the End Of Term party! Inquire closer to the date for details.'
startDate: 'April 03 2009 18:00'
online: false
location: 'TBA'
---

This is not an official club event and receives no funding. Bring food, drinks, deserts, etc.

