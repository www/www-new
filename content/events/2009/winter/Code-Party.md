---
name: 'Code Party'
short: 'CSC Code Party! Same as always - no sleep, lots of caffeine, and really nerdy entertainment. Bonus: Free Cake!'
startDate: 'March 27 2009 18:00'
online: false
location: 'Comfy Lounge (MC)'
---

This code party will have the usual, plus it will double as the closing of the programming contest. Our experts will be available to help you polish off your submission.

