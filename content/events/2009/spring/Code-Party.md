---
name: 'Code Party'
short: 'Have an assignment or project you need to work on? We will be coding from 7:00pm until 7:00am starting on Friday, June 5th in the Comfy lounge. Join us!'
startDate: 'June 05 2009 19:00'
online: false
location: 'MC 3001'
---

Have an assignment or project you need to work on? We will be coding from 7:00pm until 7:00am starting on Friday, June 5th in the Comfy lounge. Join us!

