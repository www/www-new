---
name: 'Software development gets on the Cluetrain'
short: 'or How communities of interest drive modern software development.'
startDate: 'July 24 2006 16:30'
online: false
location: 'MC 4063'
---

Simon Law leads the Quality teams for Ubuntu, a free-software operating	system built on Debian GNU/Linux. As such, he leads one of the largest	community-based testing efforts for a software product. This does get a	bit busy sometimes.

In this talk, we'll be exploring how the Internet is changing how	software is developed. Concepts like open source and technologies like	message forums are blurring the lines between producer and consumer.	And this melting pot of people is causing people to take note, and	changing the way they sling code.

Co-Sponsored with CS-Commons Committee

