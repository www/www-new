---
name: 'Code Party 0'
short: 'Code Party 0 on January 28th, 2020, at 6:30 PM in STC 0010'
startDate: 'January 28 2020 18:30'
online: false
location: 'STC 0010'
---

Code Party 0 on January 28th, 2020, at 6:30 PM in STC 0010

The CS Club is hosting our first Code Party of the term from 6:30 pm until 9 pm in STC 0010, on Tuesday, January 28. Come code with us, eat some food, do some things.

Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 0 and do it, with great company and great food. Come any time after 6:30 pm.

