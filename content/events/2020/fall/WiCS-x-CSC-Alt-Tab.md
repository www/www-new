---
name: 'WiCS x CSC: Alt-Tab'
short: 'CSC and WiCS are hosting Alt-Tab on November 19th, 5-7 PM EST.'
startDate: 'November 19 2020 17:00'
online: true
location: 'Online'
---

Come join the UW Computer Science Club (CSC) and Women in Computer Science (WiCS) on Thursday, November 19th from 5-7PM EST for Alt-Tab!

Alt-Tab is a lightning tech talk series presented by students and alumni. This term's Alt-Tab event will comprise of 4 different speakers who will be talking about topics including Ethereum NFT gaming, proof assistants and vanity Tor URLS. There will also be a raffle to win $20 gift cards!

