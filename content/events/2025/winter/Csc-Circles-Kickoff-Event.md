---
name: 'CSC Circles Kickoff Event'
short: 'There will be a group reveal and you will get to meet your circle members. There will be board games, quiz bowl, and food!'
startDate: 'February 07 2025 18:30'
endDate: 'February 07 2025 20:00'
online: false
location: 'STC 0050'
poster: 'images/events/2025/winter/'
registerLink: 'https://forms.gle/Te7SYHFXQNr74Gg88'
---

👀 Guess what? CSC Circles is BACK for W25! 🥳

🌟 Whether you're a CSC veteran or want to meet new people, joining CSC Circles is an experience you don't want to miss!

✨ Kick things off with us at our first event of the term, where you'll get to meet your Circle members and enjoy fun activities together (with free food)!

🕑 : Febuary 7th, 6:30 - 8:30 PM

📍 : STC 0050

🤔 So… how do I join?

📌 Click the Register button to sign up!

We'll group you up with roughly 4 others with similar interests; we'll arrange fun events with your Circle **happening through the entire term!**

(P.S bring a friend and indicate that you want to be paired together in the form!)

See you there!
