---
name: 'MathSoc x CSC Halloween Quest'
short: 'Ready for some Halloween fun? Join the MathSoc x CSC Halloween Quest on October 29th!'
startDate: 'October 29 2024 19:00'
endDate: 'October 29 2024 23:00'
online: false
location: 'MC Comfy & MC 3001'
poster: 'images/events/2024/fall/Csc-MathSoc-Halloween-Night.jpg'
---

Ready for some Halloween fun? Join the MathSoc x CSC Halloween Quest on October 29th!

🕸️ Time: 7-9 PM for quests, 9-11 PM for a spooky movie! 🎬

Complete interactive Halloween-themed quests, earn points with your team, and enjoy snacks at every station! 🍫
Stick around for a fun movie night afterward! 🍿

✨ You might even score some fun prizes!
📍Base Station: MC Comfy (MC 3001)

Get your team ready, and let’s make this Halloween a blast! 🎉
