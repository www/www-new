---
name: 'CSC Circles #2'
short: 'CSC Circles is here once again! Join us for a relaxing evening of arts and crafts!'
startDate: 'October 11 2024 17:00'
endDate: 'October 11 2024 19:00'
online: false
location: 'STC 0050'
poster: 'images/events/2024/fall/Csc-Circles-2.jpg'
---

CSC Circles is here once again! Join us for a relaxing evening of arts and crafts! Whether you're into painting, sketching, origami, or just want to try something new, this session is to get crafty and have some fun together! No prior experience needed!