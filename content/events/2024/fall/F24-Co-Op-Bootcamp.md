---
name: 'F24 Co-op Bootcamp Kickoff'
short: 'Want to learn more about co-op at UW + WaterlooWorks and get your resume reviewed and practice with mock interviews?'
startDate: 'September 19 2024 19:00'
endDate: 'September 19 2024 21:00'
online: false
location: 'DC 1302'
poster: 'images/events/2024/fall/F24-Co-Op-Bootcamp.jpg'
---

📣 Want to learn more about co-op at UW + WaterlooWorks and get your resume reviewed and practice with mock interviews?

📋 Come join our co-op bootcamp kick-off event this Thursday, September 19th, from 7 PM to 9 PM EDT in DC 1302!

✨ Whether you’re new to the co-op program or returning after a few terms, you’re welcome to join and learn more about how to increase your chances of landing your dream internship! See you there!