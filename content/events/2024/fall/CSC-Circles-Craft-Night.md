---
name: 'CSC Circles: Craft Night'
short: 'Join us for a relaxing evening of arts and crafts! Whether you’re into painting, sketching, origami, or just want to try something new, this session is to get crafty and have some fun together!'
startDate: 'November 1 2024 18:00'
endDate: 'November 1 2024 20:00'
online: false
location: 'STC 0050'
poster: 'images/events/2024/fall/CSC-Circles-Craft-Night.jpg'
---

🎉 CSC Circles: Craft Night is back!

Join us for a relaxing evening of arts and crafts! Whether you’re into painting, sketching, origami, or just want to try something new, this session is to get crafty and have some fun together! No prior experience needed!

📅 Date: November 1st
🕖 Time: 6:00 PM - 8:00 PM
📍 Location: STC 0050
When: Monday, October 7th

Time: 7 to 9 pm

Location: PHY145