---
name: 'CSC Circles Kickoff'
short: 'CSC Circles is back and better than ever!'
startDate: 'September 20 2024 19:00'
endDate: 'September 20 2024 21:00'
online: false
location: 'STC 0020'
poster: 'images/events/2024/fall/CSC-Circles-Kickoff.jpg'
registerLink: 'https://forms.gle/oS7S4LVh9AQSuXYB8'
---

🎉 CSC Circles is back and better than ever! 🎤✨ Join us for a night of board games, karaoke with Codey, snacks, drinks, and good vibes! Don’t miss out on the best way to start your term with new friends and fun memories! 🕹️🎲🎶

✅ Sign-up at https://forms.gle/kPzGVqraySYAVKQ48!

📅 Date: Sep 20th, 7-9pm

📍 Location: STC 0020

📝 Sign up by: Sep 19th, 11:59pm