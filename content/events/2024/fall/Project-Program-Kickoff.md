---
name: 'Project Program: Kickoff'
short: 'Ready to bring your ideas to life? Join the F24 CSC Projects Program Kickoff!'
startDate: 'October 4 2024 19:00'
endDate: 'October 4 2024 21:00'
online: false
location: 'MC 1085'
poster: 'images/events/2024/fall/Project-Program-Kickoff.jpg'
---

📣💡 Ready to bring your ideas to life? Join the F24 CSC Projects Program Kickoff! 🚀

Whether you’re passionate about coding, design, or problem-solving, this is your chance to collaborate with fellow UWaterloo students and work on exciting projects that make an impact! 💻✨

In this in-person program, you'll get the chance to explore the structure and roles for various projects, meet your fellow teammates, and submit your own ideas for future development. Plus, you'll have a social to connect with other members! 🎉

📅 Date: October 4

🕖 Time: 7:00 PM - 9:00 PM

📍 [UPDATED] Location: MC 1085

Don’t miss the chance to collaborate, learn, and create something amazing with the CSC community! 🌟