---
name: 'Codeylympics'
short: 'The Codeylympics are here!'
startDate: 'November 13 2024 19:00'
endDate: 'November 13 2024 21:30'
online: false
location: 'MC 4045'
poster: 'images/events/2024/fall/Codeylympics.png'
registerLink: 'https://forms.gle/FzqFdHQAiLRtZBMH8'
---

🎉 The Codeylympics are here!

🏃 Join us for a fun night of CS-themed challenges such as Sorting Shenanigans, Leaf It to Codey, Tech Pictionary, Decode the cipher, and LeetCode Relay! Compete in teams as you complete these challenges to earn points for a chance to win prizes!

📢 Sign up via the link below to register for the event either individually or with a team of friends! Those who sign up individually will be assigned teams on the day of the event.

🔗 Sign up here! https://forms.gle/FzqFdHQAiLRtZBMH8

📆 When? November 13th, 2024 at 7:00 - 9:30 pm EST
📍 Where?  MC 4045