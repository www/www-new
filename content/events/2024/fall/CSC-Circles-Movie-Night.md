---
name: 'CSC Circles Movie Night'
short: 'Join us at MC Comfy for an epic movie night!'
startDate: 'November 15 2024 19:00'
endDate: 'November 15 2024 21:00'
online: false
location: 'MC Comfy'
poster: 'images/events/2024/fall/CSC-Circles-Movie-Night.png'
registerLink: 'https://forms.gle/b6wxCVDKUWUZyQvLA'
---

🎬🍿 Join us at MC Comfy for an epic movie night! We’ve got the popcorn and drinks covered, so bring your blankets, get cozy, and settle in for good vibes all around. Don’t miss it! 

🎟️ Make sure to sign up and vote for which movie you would like to see at the link below!

Sign up here: https://forms.gle/b6wxCVDKUWUZyQvLA

📍Location: MC Comfy
🗓️ Date: November 15th, 2024 at 7:00 PM ET