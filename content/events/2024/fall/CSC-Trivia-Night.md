---
name: 'CSC Trivia Night'
short: 'Join us for a brain-busting event featuring jam-packed questions, delicious snacks, and Waterloo trivia'
startDate: 'October 7 2024 19:00'
endDate: 'October 7 2024 21:00'
online: false
location: 'PHY 145'
poster: 'images/events/2024/fall/CSC-Trivia-Night.jpg'
---

What sound does a goose make? Who is the architect behind MC? Will I ever find true love?! 🤔 Join us for a brain-busting event featuring jam-packed questions, delicious snacks, and Waterloo trivia. The top teams will each get some nice prizes, so go grab your friends and reach for the top!

When: Monday, October 7th

Time: 7 to 9 pm

Location: PHY145