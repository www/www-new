---
name: 'Specialization Night'
short: 'Interested in learning more about CS specializations and research positions?'
startDate: 'November 20 2024 19:00'
endDate: 'November 20 2024 21:00'
online: false
location: 'STC 0020'
poster: 'images/events/2024/fall/Specialization-Night.png'
registerLink: 'https://docs.google.com/forms/d/e/1FAIpQLSdHm6_yuzAUXZIXT5KKZiJ6q7EoBG1yhrkAmkARI_XRdK4rXQ/viewform?usp=pp_url'
---

Interested in learning more about CS specializations and research positions? Come join us along with CS advisors and URAs to get further insight into the available opportunities. 

🧋🧋 Bubble Tea will be served!!

Signup link: https://docs.google.com/forms/d/e/1FAIpQLSdHm6_yuzAUXZIXT5KKZiJ6q7EoBG1yhrkAmkARI_XRdK4rXQ/viewform?usp=pp_url