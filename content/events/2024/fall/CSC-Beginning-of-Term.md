---
name: 'CSC Beginning of Term - Fall 2024'
short: 'Kick off the term with CSC!'
startDate: 'September 30 2024 19:00'
endDate: 'September 30 2024 21:00'
online: false
location: 'STC 0060'
poster: 'images/events/2024/fall/CSC-Beginning-of-Term.jpg'
---

Kick off the term with CSC! 🎉 Join us for a fun-filled event with exciting activities, meet the team, and discover what the Computer Science Club is all about. Don’t miss out on making new friends and learning how you can get involved!