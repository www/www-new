---
name: 'Jason Goodison Talk'
short: 'Join us online to hear about YC founder, ex-Microsoft, Youtuber, and Waterloo alumnus, Jason Goodison, share his experiences!'
startDate: 'February 26 2024 18:30'
endDate: 'February 26 2024 19:30'
online: true
location: 'Google Meet'
poster: 'images/events/2024/winter/1711580385019--Jason-Goodison-Talk.png'
---

🚀 Ever wondered about the Y combinator experience? Want to learn about the life of a Microsoft Software Engineer? Want some advice for the rest of your time at Waterloo? Interested in the Youtuber life?

⚠️ Then look no further! Join us for a talk with Jason Goodison: a YC founder, Ex-Microsoft, Youtuber, and Waterloo alumnus is hosting a virtual talk and networking session.

📆 When? February 26th, 2024 at 6:30 - 7:30pm ET

📍 Where? Google Meet (https://meet.google.com/ihe-nnxf-szo)
