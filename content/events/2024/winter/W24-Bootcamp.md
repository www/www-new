---
name: 'W24 Bootcamp'
short: 'Bootcamp is back for Winter 2024 and we are streamlining to provide tailored advice on your resumes.'
startDate: 'January 26 2024 12:00'
endDate: 'February 09 2024 12:00'
online: true
location: 'Discord'
poster: 'images/events/2024/winter/1707084556826--W24 Bootcamp.png'
registerLink: 'https://forms.gle/hgSLdpvWDa5pSbi99'
---

📣 Bootcamp is back for Winter 2024 and we are streamlining to provide tailored advice on your resumes.

⭐️ As a mentee, you will be able to receive valuable feedback on your resume from multiple CSC and DSC mentors from a wide variety of tech backgrounds.

📅 The Resume Reviews will take place asynchronously throughout the two weeks of January 26-February 9, 2024. In this time, multiple members will add feedback to your resume, and it will be returned to you in a timely manner before Cycle 2.

📍 If you are confused about any comments you receive, you can submit the ‘Further Inquiries’ Google Form to get specific explanations synchronously with a mentor.

✏️ To sign up, please fill out the W24 Bootcamp Sign-Up form at the link in our bio!
(https://forms.gle/hgSLdpvWDa5pSbi99)

⚠️ The deadline to sign up for Bootcamp is January 26th, 2023, 11:59 p.m. ET.

🤩 Best of luck in your job search!!
