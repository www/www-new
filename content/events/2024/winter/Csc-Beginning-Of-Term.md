---
name: 'CSC Beginning of Term!'
short: 'Kick off the winter term with CSC’s BOT event!'
startDate: 'January 24 2024 19:00'
endDate: 'January 24 2024 21:00'
online: false
location: 'STC 0020'
poster: 'images/events/2024/winter/1705976921687--BOT.jpg'
---

📢 Kick off the winter term with CSC’s BOT event! Are you interested in attending upcoming CSC events? Want to meet others in the CS community? Want to have fun and eat food? You don’t want to miss our first event of this term!

🎉Come join us for a night of speed-friending, fun games, and pizza! 

📆 When? Wed Jan 24 at 7:00 - 9:00pm ET

📍 Where? STC 0020

