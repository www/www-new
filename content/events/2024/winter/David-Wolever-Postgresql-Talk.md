---
name: 'David Wolever PostgreSQL Talk'
short: 'Come out for a deep dive into PostgreSQL with David Wolever.'
startDate: 'January 25 2024 19:00'
endDate: 'January 25 2024 20:00'
online: false
location: 'RCH 307'
poster: 'images/events/2024/winter/1707084310348--PostgreSQL-Talk.jpg'
---

Dive into the depths of PostgreSQL with David Wolever as he unravels the intricate web of transaction implementation in this enlightening talk. Please join us on Jan 25th, 7pm - 8pm in RCH 307 ✨ 💻
