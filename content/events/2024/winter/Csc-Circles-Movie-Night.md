---
name: 'CSC Circles Movie Night'
short: 'Come join us for a movie, board games, and free bubble tea!'
startDate: 'March 15 2024 19:00'
endDate: 'March 15 2024 21:00'
online: false
location: 'STC 0010'
poster: 'images/events/2024/winter/1711581314872--CSC-Circles-Movie-Night.png'
registerLink: 'https://forms.gle/c9wTpwirzD6DChzK8'
---

📣📣 Have you been waiting for the next Circles event? Do you want to destress after midterms and meet new people? Well, CSC Circles is back with another fun movie night 🎬 along with board games and boba🧋(first come first serve)!

Come hang out with your circle and meet other CSC members!

📝 Sign up by: March 13th using the link (also in linktree) https://forms.gle/c9wTpwirzD6DChzK8

📍 Location: STC 0010

📅 Date: March 15th from 7 - 9pm

We can’t wait to see you there!
