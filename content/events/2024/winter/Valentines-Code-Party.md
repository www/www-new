---
name: 'Valentines Code Party'
short: 'Join us for the first code party of the term!'
startDate: 'February 14 2024 19:00'
endDate: 'February 14 2024 21:00'
online: false
location: 'EIT 1015'
poster: 'images/events/2024/winter/1707695673788--Valentines-Code-Party.png'
---

💕 Don’t spend your Valentine’s Day alone!
Join us for the first code party of the term! Work on assignments, side projects, and midterm preparation together, or socialize over board games and free food for CSC members.

⏰ Time: February 14th, 7-9pm ET

📍 Location: EIT 1015

Feel free to bring your Aphrodite match 😉
