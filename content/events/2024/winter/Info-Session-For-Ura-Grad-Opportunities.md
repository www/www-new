---
name: 'Info Session for URA & Grad Opportunities'
short: 'Join us for an exclusive Graduate Studies/Undergraduate Research Assistantships (URA) Info Session hosted by UW CS Club!'
startDate: 'March 06 2024 18:00'
endDate: 'March 06 2024 20:00'
online: false
location: 'DC 1302'
poster: 'images/events/2024/winter/1711580791369--Research-Info-Session.png'
---

🎓 Ever thought of working on a research project alongside professors? Join us for an exclusive Graduate Studies/Undergraduate Research Assistantships (URA) Info Session hosted by UW CS Club! 💻 Gain valuable insights from URA students and professors! 🌟 Don’t miss out on this invaluable opportunity to network with professors and have some yummy snacks!

📆 March 6th, 6-8pm

📍 Where? DC 1302
