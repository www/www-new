---
name: 'How to Build Side Projects'
short: 'Learn essential skills and insights to transform your ideas into impactful, real-world projects that employers will love'
startDate: 'June 20 2024 19:00'
endDate: 'June 20 2024 21:00'
online: false
location: 'STC 0010'
poster: 'images/events/2024/spring/Career-and-Specialization-Night.jpg'
---

Unlock your potential at CSC’s “How to Build Side Projects” session! Join us to learn essential skills and insights to transform your ideas into impactful, real-world projects that employers will love.

👉 Register at the link in our bio. See you there!

📅 Date: June 20th, 2024 at 7:00-9:00 PM EST

📍 Location: STC 0010