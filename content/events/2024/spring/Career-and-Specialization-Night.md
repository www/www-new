---
name: 'Career and Specialization Night'
short: 'Come out to learn about Computer Science Degree Specializations!'
startDate: 'July 18 2024 17:30'
endDate: 'July 18 2024 18:30'
online: false
location: 'DC 1350'
poster: 'images/events/2024/spring/Career-and-Specialization-Night.jpg'
---

🎉 Come out to learn about Computer Science Degree Specializations!

👥 Hear from CS advisor Mark Petric and several upper year students about requirements, favourite courses, and co-op experiences. Free food provided!

🔥 Don’t miss out on this amazing opportunity to plan ahead and gain valuable insights.

😋 P.S. There will be free food and drinks for all attendees

📆 Event Date: Thursday, July 18th, 5:30 - 6:30 PM at DC 1350