---
name: 'CSC Outdoor Activities Night'
short: "CSC is hosting a trivia night! Compete in teams of up to 4 for $250 worth of prizes!"
startDate: 'June 12 2024 19:00'
endDate: 'June 12 2024 21:00'
online: false
location: 'QNC 2502'
poster: 'images/events/2024/spring/trivia-night.png'
---

📣 CSC Trivia Night
 
⭐️ Join us for CSC's own Trivia Night! Come out with your friends to answer mind-boggling questions and win
from a wide range of prizes!

Date: June 12th 7-9pm