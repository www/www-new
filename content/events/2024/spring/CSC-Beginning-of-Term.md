---
name: 'CSC Beginning of Term - Spring 2024'
short: 'Kick off the term with CSC!'
startDate: 'May 22 2024 19:00'
endDate: 'May 22 2024 21:00'
online: false
location: 'STC 0050'
poster: 'images/events/2024/spring/CSC-Beginning-of-Term.jpg'
---

📢 Kick off the spring term with CSC’s Beginning of Term event! Are you interested in attending upcoming CSC events? Want to meet others in the CS community? Want to have fun and eat food? You don’t want to miss our first event of this term!

🎉 Come join us for a night of speed-friending, fun games, and pizza!

📆 When? Wednesday May 22 at 7:00 - 9:00pm ET

📍 Where? STC 0050
