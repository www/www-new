---
name: 'S24 Internship Insights'
short: 'Receive valuable feedback on your resume and sharpen your interviewing skills!'
startDate: 'June 5 2024 19:00'
endDate: 'July 31 2024 19:00'
online: false
location: 'MC 4020 & 4021'
poster: 'images/events/2024/spring/Internship-Insights.jpg'
---

📣 Applications to the Summer 2024 Internship Insights program are now open!
 
⭐️ As a mentee, you will receive valuable feedback on your resume and sharpen your interviewing skills through our mock interviews. You will be matched by field/career interest with one of our many experienced mentors, who will guide you using their own career experiences to assist with job applications, provide resume feedback, conduct mock interviews, and answer any questions you may have.

As a mentor, you will be giving information to other students, critiquing their resumes/performing mock interviews. Snacks will be provided!

All synchronous resume reviews and/or mock interviews will be held on campus, but we will also have an online portion for online mentors and mentees subject to the number of online mentors

 📃 The Resume Reviews and Mock Interviews will:
• Take place synchronously every week from June 5th from 7:00 - 8:30 pm until the end of July (weekly/bi-weekly on Wednesdays).
• Be personalized as you meet 1 on 1 with a mentor
 
 ⚠️ The deadline to submit this form is June 1st, 2024 at 11:59 p.m. ET. Don’t miss out on this opportunity!

🍔 Did we mention that snacks will be provided?? Snacks will be provided for all mentors and mentees that participate in our weekly sessions!
 
Mentor Signup Here:
https://docs.google.com/forms/d/1Ta74SQ7q471h502jyR2R5kzmdpMx-IFHhBJC7hX6MAM/edit

Mentee Signup Here: https://docs.google.com/forms/d/e/1FAIpQLSf3aOyU01Uhk6kTzM25H0RXcgzTbKloJbpaIzS6nFXn2ZjJdw/viewform
 
📨 You will receive an email with more information and instructions once signups are complete.

Location: MC 4020 & 4021

Date: Wednesdays, June 5th to July 31st