---
name: 'End of Term Bonfire'
short: 'Join us for our Bonfire End of Term event in collaboration with our friends at CSC, WiCS, and Tech+'
startDate: 'July 23 2024 18:00'
endDate: 'July 23 2024 21:00'
online: false
location: 'Columbia Lake, Fire Pit 1'
poster: 'images/events/2024/spring/Bonfire-EOT.jpg'
---

🔥✨ Join us for our Bonfire End of Term event in collaboration with our friends at CSC, WiCS, and Tech+ ✨🔥

📅 Tuesday, July 23rd
⏰ 6:00 - 9:00 PM
📍Columbia Lake, Fire Pit 1

Please note this event is drop in! No need to stay for the whole time, come and go as you please, open to UWaterloo students😄

Get ready for a fun evening with s'mores, hot dogs (vegan option available), drinks, freezies, and other snacks🍫😋

Don't miss out! See you there 🌐
