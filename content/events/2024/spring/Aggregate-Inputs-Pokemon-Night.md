---
name: 'Aggregate Inputs Pokemon Night'
short: 'Catch em all at CSC Aggregate Inputs Video Game Night, where we’ll work together to play a Pokemon game controlled by the sum of everyone’s actions!'
startDate: 'June 26 2024 19:00'
endDate: 'June 26 2024 21:00'
online: false
location: 'STC 0020'
poster: 'images/events/2024/spring/Aggregate-Inputs-Pokemon-Night.jpg'
---

Are two players twice as good at Pokemon? How about a whole room?

Catch em all at CSC Aggregate Inputs Video Game Night, where we’ll work together to play a Pokemon game controlled by the sum of everyone’s actions!

📅 Date: June 26th, 2024

🕖 Time: 7:00-9:00 PM EST

📍 Location: STC 0020 (or catch us at twitch.tv/uwcsclub)
