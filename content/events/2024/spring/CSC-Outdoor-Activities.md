---
name: 'CSC Outdoor Activities Night'
short: "CSC Outdoor Activities Night is a medley of everyone's favourite outdoor activities!"
startDate: 'July 18 2024 19:00'
endDate: 'July 18 2024 21:00'
online: false
location: 'Fields near CIF'
poster: 'images/events/2024/spring/outdoor-activities.png'
---

📣 CSC Outdoor Activities Night
 
⭐️ Join us in games of capture the flag, volleyball, frisbee, and other fun team competitions.

Location: Fields near CIF

Date: July 18th 7-9pm