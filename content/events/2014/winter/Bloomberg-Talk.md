---
name: 'Bloomberg Talk'
short: 'Bloomberg''s Alex Scotti will be presenting a talk this Tuesday on concurrency control implementations in relational databases. Free swag and dinner will be provided.'
startDate: 'February 04 2014 17:30'
online: false
location: 'MC 4058'
---

Join Alex Scotti of Bloomberg LP for a discussion of concurrency control implementation in relational database systems. Focus will be placed on the optimistic techniques as employed and developed inside Combdb2, Bloomberg's database system.

Food will be served by Kismet!

