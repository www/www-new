---
name: 'Battle Decks'
short: 'Create a 5-slide PowerPoint presentation about a specific topic. Bring it with you to the event (on a flash drive). Submit it into the lottery. Select a random PowerPoint presentation from the lottery and talk about it on the spot.'
startDate: 'March 18 2014 19:00'
online: false
location: 'MC 4041'
---

Create a 5-slide PowerPoint presentation about a specific topic. Bring it with you to the event (on a flash drive). Submit it into the lottery. Select a random PowerPoint presentation from the lottery and talk about it on the spot.

