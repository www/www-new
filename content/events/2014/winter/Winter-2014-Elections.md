---
name: 'Winter 2014 Elections'
short: 'Elections for Winter 2014 are being held! The Executive will be elected, and the Office Manager and Librarian will be appointed by the new executive.'
startDate: 'January 16 2014 17:30'
online: false
location: 'Comfy Lounge'
---

It's elections time again! On Thursday, January 16 at 5:30PM, come to the Comfy Lounge on the 3rd floor of the MC to vote in this term's President, Vice-President, Treasurer and Secretary. The Sysadmin, Librarian, and Office Manager will also be chosen at this time.

Nominations are open until 4:30PM on Wednesday, January 15, and can be written on the CSC office whiteboard (yes, you can nominate yourself). Full CSC members can vote and are invited to drop by. You may also send nominations to the [Chief Returning Officer](<mailto:cro@csclub.uwaterloo.ca>) by email.

Nominations are now closed. The candidates are:

- President:- Jonathan Bailey (<tt>jj2baile</tt>

    )
    - Nicholas Black (<tt>nablack</tt>

    )
    - Bryan Coutts (<tt>b2coutts</tt>

    )
    - Annamaria Dosseva (<tt>mdosseva</tt>

    )
    - Youn Jin Kim (<tt>yj7kim</tt>

    )
    - Visha Vijayanand (<tt>vvijayan</tt>

    )

    <!-- -->

- Vice-President:- Nicholas Black (<tt>nablack</tt>

    )
    - Bryan Coutts (<tt>b2coutts</tt>

    )
    - Visha Vijayanand (<tt>vvijayan</tt>

    )

    <!-- -->

- Treasurer:- Jonathan Bailey (<tt>jj2baile</tt>

    )
    - Nicholas Black (<tt>nablack</tt>

    )
    - Marc Burns (<tt>m4burns</tt>

    )
    - Bryan Coutts (<tt>b2coutts</tt>

    )

    <!-- -->

- Secretary:- Jonathan Bailey (<tt>jj2baile</tt>

    )
    - Bryan Coutts (<tt>b2coutts</tt>

    )
    - Mark Farrell (<tt>m4farrel</tt>

    )

    <!-- -->


<!-- -->

