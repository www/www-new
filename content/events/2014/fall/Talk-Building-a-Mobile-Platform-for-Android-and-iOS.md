---
name: 'Talk: Building a Mobile Platform for Android and iOS'
short: 'Come listen to a Google software engineer give a talk on building a mobile platform for Android and iOS! Wesley Tarle has been leading development at Google in Kitchener and Mountain View, and building stuff for third-party developers on Android and iOS. He''s contributed to Google Play services since its inception and continues to produce APIs and SDKs focused on mobile startups. RSVP at http://goo.gl/Pwc3m4.'
startDate: 'September 18 2014 18:00'
online: false
location: 'MC 4021'
---

Come listen to a Google software engineer give a talk on building a mobile platform for Android and iOS! Wesley Tarle has been leading development at Google in Kitchener and Mountain View, and building stuff for third-party developers on Android and iOS. He's contributed to Google Play services since its inception and continues to produce APIs and SDKs focused on mobile startups. RSVP at http://goo.gl/Pwc3m4.

