---
name: 'Talk: C++ ABI'
short: 'C++ is an interesting study because it supports a large number of powerful, abstract concepts, yet it operates very close to the hardware compared to many modern programming languages. There are also many implementations of C++ which must be made interoperable. I will discuss some aspects of the Itanium 64 Application Binary Interface (ABI) for C++, which is now the de facto standard across Unix-like platforms of all architectures. In particular, I will cover a number of aspects of the class system fundamental to C++: data layout, polymorphic types, construction and destruction, and dynamic casting.'
startDate: 'November 25 2014 17:30'
online: false
location: 'MC 4041'
---

C++ is an interesting study because it supports a large number of powerful, abstract concepts, yet it operates very close to the hardware compared to many modern programming languages. There are also many implementations of C++ which must be made interoperable. I will discuss some aspects of the Itanium 64 Application Binary Interface (ABI) for C++, which is now the de facto standard across Unix-like platforms of all architectures. In particular, I will cover a number of aspects of the class system fundamental to C++: data layout, polymorphic types, construction and destruction, and dynamic casting.

