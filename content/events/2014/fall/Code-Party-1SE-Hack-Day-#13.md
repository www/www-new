---
name: 'Code Party 1/SE Hack Day #13'
short: 'Why sleep when you could be hacking on $SIDE\_PROJECT, or working on $THE\_NEXT\_BIG\_THING with some cool CSC/SE people? Come when you want, hack on something cool, demo before you leave.If you don''t have a project, don''t worry - we have a list of ideas, and a lot of people will be looking for an extra helping hand on their projects.NOTE: Dinner and snacks will only be served to those working on projects during the event.'
startDate: 'November 21 2014 18:00'
online: false
location: 'M3 1006'
---

Why sleep when you could be hacking on $SIDE\_PROJECT, or working on $THE\_NEXT\_BIG\_THING with some cool CSC/SE people? Come when you want, hack on something cool, demo before you leave.

If you don't have a project, don't worry - we have a list of ideas, and a lot of people will be looking for an extra helping hand on their projects.

NOTE: Dinner and snacks will only be served to those working on projects during the event.

