---
name: '''Hackers'' Screening'
short: 'Women in Computer Science (WiCS) and the Computer Science Club (CSC) will meet up in the Comfy Lounge to watch a favourite cult classic: Hackers. Join us as we relive our 90s teenage hacking fantasies and stuff our faces with popcorn and junk food.Hackers of the world, unite!'
startDate: 'November 07 2014 19:00'
online: false
location: 'MC Comfy'
---

Women in Computer Science (WiCS) and the Computer Science Club (CSC) will meet up in the Comfy Lounge to watch a favourite cult classic: Hackers. Join us as we relive our 90s teenage hacking fantasies and stuff our faces with popcorn and junk food.

Hackers of the world, unite!

