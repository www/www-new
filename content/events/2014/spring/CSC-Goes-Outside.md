---
name: 'CSC Goes Outside'
short: 'Come throw a frisbee, hang around a bonfire, and roast marshmellows! This is a social event just for fun, so come relax and eat snacks in good company!'
startDate: 'June 13 2014 19:30'
online: false
location: 'Laurel Creek Fire Pit'
---

Meet at the Laurel Creek Fire Pit (the one across Ring Road from EV3) at 7:30 for a fun night of hanging out with friends. If you aren't sure where it is, meet at the office ten minutes before hand, and we will walk over together. We'll start the evening off with throwing around a frisbee or two, and as the night goes on we'll light up the fire and get some s'mores cooking!

