---
name: 'Spring 2014 Elections'
short: 'The Computer Science Club will soon be holding elections for this term''s executive. The president, vice president, treasurer, and secretary for the spring 2014 term will be elected. The system administrator, office manager, and librarian are also typically appointed here.'
startDate: 'May 15 2014 18:00'
online: false
location: 'Comfy Lounge'
---

Nominations are now closed. The candidates are:

- President:- Jinny Kim (<tt>yj7kim</tt>

    )
    - Matthew Thiffault (<tt>mthiffau</tt>

    )
    - Shane Creighton-Young (<tt>srcreigh</tt>

    )
    - Hayford Peprah (<tt>hkpeprah</tt>

    )

    <!-- -->

- Vice-President:- Luke Franceschini (<tt>l3france</tt>

    )
    - Jinny Kim (<tt>yj7kim</tt>

    )
    - Shane Creighton-Young (<tt>srcreigh</tt>

    )
    - Hayford Peprah (<tt>hkpeprah</tt>

    )

    <!-- -->

- Treasurer:- Luke Franceschini (<tt>l3france</tt>

    )
    - Matthew Thiffault (<tt>mthiffau</tt>

    )
    - Catherine Mercer (<tt>ccmercer</tt>

    )
    - Joseph Chouinard (<tt>jchouina</tt>

    )

    <!-- -->

- Secretary:- Luke Franceschini (<tt>l3france</tt>

    )
    - Catherine Mercer (<tt>ccmercer</tt>

    )
    - Joseph Chouinard (<tt>jchouina</tt>

    )
    - Ifaz Kabir (<tt>ikabir</tt>

    )

    <!-- -->


<!-- -->

