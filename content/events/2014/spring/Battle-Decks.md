---
name: 'Battle Decks'
short: 'Five slides. Five minutes. Pure fun.'
startDate: 'June 25 2014 18:00'
online: false
location: 'MC 2035'
---

Create an entertaining slideshow and present someone else's on the spot! Join us in MC 2035 on Wednesday June 25 at 18:00 for a fun evening of quick presentations of random slide decks. An example from last semester can be found at tinyurl.com/battle-decks-example. Please e-mail your battle deck to l3france@csclub.uwaterloo.ca. Snacks will be provided to fuel your battle hunger!

