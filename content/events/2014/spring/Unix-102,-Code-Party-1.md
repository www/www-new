---
name: 'Unix 102, Code Party 1'
short: 'Learn how to host a website and spend the night hacking!'
startDate: 'July 11 2014 17:00'
online: false
location: 'MC 3003, M3 1006'
---

Did you know that by becoming a CSC member, you get 4GB of free webspace? Join us in MC 3003 on Friday July 11 to learn how to use that space and host content for the world to see! Afterwards we will be moving over to M3 1006 for a night of hacking and snacking! Work on a personal project, open source software, or anything you wish. Food will be provided for your hacking pleasure. Come join us for an evening of fun, learning, and food!

