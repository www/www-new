---
name: 'The Most Important Parts of School (from a CS dropout)'
short: 'Learn about the real reasons you should be in school from David Wolever, CTO of akindi and a director of PyCon Canada.'
startDate: 'July 22 2014 18:00'
online: false
location: 'MC 4020'
---

Hindsight is 20/20, and since leaving university I’ve had five years and three startups to reflect on the most valuable things I have (and haven’t) taken away from my time in school. David studied computer science for three years at the University of Toronto before leaving to be employee zero at a Waterloo-based startup. Since then he has been a founder of two more startups, started PyCon Canada, and has written hundreds of thousands of lines of code. He is currently CTO of Akindi, a Toronto-based startup trying to make multiple choice testing a bit less terrible. He’s best found on Twitter at http://twitter.com/wolever

