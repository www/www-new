---
name: "GDB, Purify Tutorial"
short: ""
startDate: 'October 19 1999 17:30'
online: false
location: "DC1304"
---

Debugging can be the most difficult and time consuming part of any program's life-cycle. Far from an exact science, it's more of an art ... and close to some kind of dark magic. Cryptic error messages, lousy error checking, and icky things like implicit casts can make it nearly impossible to know what's going on inside your program.

Several tools are available to help automate your debugging. GDB and Purify are among the most powerful debugging tools available in a UNIX environment. GDB is an interactive debugger, allowing you to \`step' through a program, examine function calls, variable contents, stack traces and let you look at the state of a program after it crashes. Purify is a commercial program designed to help find and remove memory leaks from programs written in languages without automatic garbage collection.

This talk will cover how to compile your C and C++ programs for use with GDB and Purify, as well as how to use the available X interfaces. If a purify license is available on undergrad at the time of the talk, we will cover how to use it during runtime.
