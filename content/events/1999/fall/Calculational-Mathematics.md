---
name: 'Calculational Mathematics'
short: 'By Edgar Dijkstra'
startDate: 'December 02 1999 13:30'
online: false
location: 'DC1302'
---

By Edgar Dijkstra

This talk will use partial orders, lattice theory, and, if time permits, the Galois connection as carriers to illustrate the use of calculi in mathematics. We hope to show the brevity of many calculations (in order to fight the superstition that formal proofs are necessarily impractically long), and the strong heuristic guidance that is available for their design.

Dijkstra is known for early graph-theoretical algorithms, the first implementation of ALGOL 60, the first operating system composed of explicitly synchronized processes, the invention of guarded commands and of predicate transformers as a means for defining semantics, and programming methodology in the broadest sense of the word.

His current research interests focus on the formal derivation of proofs and programs, and the streamlining of the mathematical argument in general.

Dijkstra held the Schlumberger Centennial Chair in Computer Sciences at The University of Texas at Austin until retiring in October.

