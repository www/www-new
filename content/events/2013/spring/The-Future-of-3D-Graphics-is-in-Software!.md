---
name: 'The Future of 3D Graphics is in Software!'
short: 'Convergence between CPU and GPU approaches to processing sets the stage for an exciting transition to 3D rendering that takes place entirely in software. TransGaming''s Nicolas Capens and Gavriel State will speak about this convergence and how it will influence the future of graphics.'
startDate: 'July 22 2013 17:00'
online: false
location: 'MC 4020'
---

For some time now, it has been clear that there is strong momentum for convergence between CPU and GPU technologies. Initially, each technology used radically different approaches to processing, but over time GPUs have evolved to support more general purpose use while CPUs have evolved to include advanced vector processing and multiple execution cores. At TransGaming, we believe that this convergence will continue to the point where typical systems have only one type of processing unit, with large numbers of cores and very wide vector execution units available for high performance parallel execution. In this kind of environment, all graphics processing will ultimately take place in software.

In this talk, we will explore the converging nature of CPU and GPU approaches to processing, how dynamic specialization allows CPUs to efficiently perform tasks usually done by GPUs, and why we believe that the increased flexibility of more programmable architectures will ultimately win out over fixed function hardware, even in areas such as texture sampling.

**TransGaming Inc.** works at the cutting edge of 3D graphics, building technologies that bridge the gap between platform boundaries to allow games to be played on a variety of devices and operating systems. TransGaming works with other industry leaders to update established APIs such as OpenGL, while also breaking new ground in software rendering technology, which we believe will become increasingly important as CPU and GPU technologies converge.

**Nicolas Capens** is the architect of SwiftShader, TransGaming's high performance software renderer, and is also deeply involved in the ANGLE project, which provides efficient translation from OpenGL ES to Direct3D APIs for implementing WebGL on Windows. Nicolas received his MSci.Eng. degree in computer science from Ghent University in 2007.

**Gavriel State (Gav)** is TransGaming's Founder and CTO, and has worked in graphics and portability for over 20 years on dozens of platforms and APIs. Gav wrote his first software renderer when taking CS488 at UW, where he later graduated with a B.A.Sc. in Systems Design Engineering.

