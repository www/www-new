---
name: 'Spring 2013 Elections'
short: 'Elections for Spring 2013 are being held! The Executive will be elected, and the Office Manager and Librarian will be appointed by the new executive.'
startDate: 'May 15 2013 18:00'
online: false
location: 'Comfy Lounge'
---

It's elections time again! On Wednesday, May 15 at 6:00PM, come to the Comfy Lounge on the 3rd floor of the MC to vote in this term's President, Vice-President, Treasurer and Secretary. The Sysadmin, Librarian, and Office Manager will also be chosen at this time.

Nominations are open until 4:30PM on Tuesday, May 14, and can be written on the CSC office whiteboard (yes, you can nominate yourself). Full CSC members can vote and are invited to drop by. You may also send nominations to the [ Chief Returning Officer](<mailto:cro@csclub.uwaterloo.ca>). A full list of candidates will be posted when nominations close, along with instructions for voting remotely.

Good luck to our candidates!

