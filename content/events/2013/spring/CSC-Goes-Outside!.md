---
name: 'CSC Goes Outside!'
short: 'Do you love the combination of s''mores, burgers, and fire? Are you brave enough to face the newly-grown geese? Do you want to play some Frisbee while listening to some chill tunes? If so, come hang out with the CSC at the EV3 Fire Pit this Friday! All are welcome for some outdoor food, games, and music.'
startDate: 'July 19 2013 19:00'
online: false
location: 'EV3 Fire Pit'
---

Do you love the combination of s'mores, burgers, and fire? Are you brave enough to face the newly-grown geese? Do you want to play some Frisbee while listening to some chill tunes? If so, come hang out with the CSC at the EV3 Fire Pit this Friday! All are welcome for some outdoor food, games, and music.

