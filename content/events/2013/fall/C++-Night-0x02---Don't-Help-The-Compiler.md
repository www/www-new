---
name: 'C++ Night 0x02 - Don''t Help The Compiler'
short: 'The second in a series of recorded talks from GoingNative 2013. Featuring Stephan T. Lavavej.'
startDate: 'October 17 2013 18:30'
online: false
location: 'PHY 150'
---

The second in a series of recorded talks from GoingNative 2013. Featuring Stephan T. Lavavej.

C++ has powerful rules for dealing with low-level program structure. Before a program is ever executed, the compiler determines valuable information about every expression in the source code. The compiler understands exactly how long each object's resources will be needed (lifetime), whether each expression refers to an object that the program has no other way of accessing (rvalueness), and what operations can be performed on each object (type). Using examples from C++98 through C++14, this presentation will demonstrate how to write code that works with the compiler's knowledge to increase robustness, efficiency, and clarity. This presentation will also demonstrate the horrible things that happen when programmers think they can do tasks that are better left to compilers.

