---
name: 'Practical Tor Usage'
short: 'The third lecture of our security and privacy series. By undergraduate student Simon Gladstone.'
startDate: 'October 24 2013 18:30'
online: false
location: 'DC 1302'
---

An introduction to and overview of how to use the Tor Browser Bundle to browse the "Deep Web" and increase security while browsing the Internet. Tor is not the be all end all of Internet security, but it is definitely a step up from using the more popular browsers such as Chrome, Firefox, or Safari.

The third lecture of our security and privacy series. By undergraduate student Simon Gladstone.

