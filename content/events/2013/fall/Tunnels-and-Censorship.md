---
name: 'Tunnels and Censorship'
short: 'The second lecture of our security and privacy series. By undergraduate student Eric Dong.'
startDate: 'October 15 2013 17:00'
online: false
location: 'MC 4060'
---

In this talk, I will discuss censorship firewalls used in countries such as China and Iran, and how to counteract them. The focus is on advanced application-layer and Deep Packet Inspection firewalls, and unexpected hurdles in overcoming censorship by these firewalls due to the need for very unconventional adversary models. Approaches of the privacy tool Tor, popular proprietary freeware Ultrasurf and Freegate, payware VPNs, and my own experimental Kirisurf project are examined, where strengths and difficulties with each system are noted.

The second lecture of our security and privacy series. By undergraduate student Eric Dong.

