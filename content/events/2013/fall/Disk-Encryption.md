---
name: 'Disk Encryption'
short: 'The last lecture of our security and privacy series. By MMath alumnus Zak Blacher.'
startDate: 'November 26 2013 17:00'
online: false
location: 'MC 2038'
---

In Zak's talk, "Disk Encryption: Digital Forensic Analysis & Full Volume Encryption", he aims to cover filesystem forensic analysis and counter forensics by addressing the entire design stack; starting with filesystem construction, design, and theory, and drilling down to the inner workings of hard drives (modern platter hdds, as well as mlc-ssds). This talk leads in to a discussion on full volume encryption, and how this helps to protect one's data.

The sixth and final lecture of our security and privacy series.

