---
name: 'Using Computers to Find Evidence in Litigation'
short: 'Professor Gordon Cormack will be presenting a talk on using machine-learning based spam filters to accurately locate relevent electronic documents - a process which has typically been very manual, and very expensive.'
startDate: 'March 21 2013 16:30'
online: false
location: 'MC 4020'
---

In a lawsuit, each party is typically entitled to Discovery, in which the other party is compelled to produce any "documents" in its possession that may be pertinent to the case. Documents include not only traditional paper documents, but email messages, text messages, computer files, and other electronically stored information, or ESI. Suppose you were compelled to produce every document in your possession pertaining to software downloads or purchases? How would you do it? If you were a large corporation, you would probably hire an army of lawyers to read all your email, plus your assignments, and any other files on your UW account, your laptop, your phone, and your tablet, at a cost of one dollar or more per file. As a CSC member, you know there must be a better way. But what is that better way, and how do you convince the court to let you use it?

It turns out that spam filters that employ machine learning can do this job well -- better than that army of lawyers. But lawyers aren't happy about this. This talk will outline how the technology works and how to prove that it works, so as to convince scientists, lawyers, and judges.

