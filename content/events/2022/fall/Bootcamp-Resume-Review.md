---
name: 'Bootcamp Resume Review'
short: "Come to receive insightful feedback and advice on your resume to rock your job search!"
startDate: 'September 14 2022 18:00'
endDate: 'September 14 2022 22:00'
online: true
location: 'Discord'
poster: 'images/events/2022/fall/Bootcamp-Mentee-Applications.png'
registerLink: https://bit.ly/bootcamp-mentee-signups
---

📢 Applications for Bootcamp are now OPEN! 📢 CSC is bringing back Bootcamp to gear you up for your next recruiting season, partnered with @uwaterloodsc, @uwblueprint, @uwaterloowics, @watonomous and @techplusuw! 💻 The drop-in resume review event takes place September 14th 6:00 - 10:00 PM EST.

💁‍♀️ Sign up as a mentee, and join our experienced mentors in Resume Reviews and Mock Interviews (virtual 1:1 sessions) to receive feedback from various tech backgrounds 📃 You will be paired with a mentor who is knowledgeable in a similar career path to yours to ensure relevant feedback! 👌

We would love to help you feel ready and confident for the upcoming job hunt. Our collaborating clubs are excited to bring you this opportunity to sharpen your job hunting skills. 🧠

👉 Apply using this link https://bit.ly/bootcamp-mentee-signups

Alternatively, you can email us at exec@csclub.uwaterloo.ca with the year and program you’re in, along with interested job paths.

📅 Deadline to Apply for both: September 12th 2022, 11:59 PM EST
