---
name: 'CSC x Google: Life of a SWE + Q&A'
short: "Join us as Googlers share day-in-the-life stories about their work and provide an inside look on what makes engineering at Google unique."
startDate: 'September 13 2022 18:00'
endDate: 'September 13 2022 19:30'
online: false
location: 'AHS EXP 1689'
poster: 'images/events/2022/fall/Google-Life-of-a-SWE.jpeg'
registerLink: https://goo.gle/3AKSOR6
---
📣 Interested in learning what is it like to be a software engineer at Google? Join us as Googlers share day-in-the-life stories about their work and provide an inside look on what makes engineering at Google unique.

👀 RSVP with the following link to confirm your spot: https://goo.gle/3AKSOR6

🗓️ Event Date: Tuesday September 13th from 6:00 PM - 7:30 PM ET

We are looking forward to connecting Google with the UWaterloo community! 🎉
