---
name: "Alt-Tab"
short: "Join CSC for Alt-Tab, a lightning tech talk series presented by students!"
startDate: "November 29 2022 19:00"
endDate: "November 29 2022 21:00"
online: false
location: "MC 2035"
poster: "images/events/2022/fall/Alt-Tab.jpg"
---
🎙️ Join CSC for Alt-Tab, a lightning tech talk series presented by students! Alt-Tab consists of several ~15-minute talks about a variety of topics related to computer science and technology. Snacks will be provided.

⚡ Talk list:
- Tropical Semirings: General method to solve graph problems in a purely functional way - Simon Zeng
- A Deep Dive into Language Servers and Editors - Hamza Ali
- "Are Computers Conscious?" And Other Impractical Tech Philosophy - Justin Y.
- Dotfiles: Speeding up your shell experience - Raymond Li
- Networking 404: How to network as a person that doesn't know what networking means - Allen Q. Lu
- JITs in the Quest for Performance - Antonio Abbatangelo
- Turning the Calculator Factory Into a Calculator: C++ Template Metaprogramming - Evan Girardin

📅 Date: Tuesday, November 29, 2022 at 7 PM

📍 Location: MC 2035
