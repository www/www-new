---
name: 'First Year Orientation'
short: 'Come join us at CSC’s first-ever incoming student orientation to meet your peers and learn what CSC is all about!'
startDate: 'September 15 2022 19:45'
endDate: 'September 15 2022 21:00'
online: false
location: 'DC 1351'
poster: 'images/events/2022/fall/First-Year-Orientation.png'
registerLink: 'https://bit.ly/f22-orientation'
---

📣 Hey incoming CS and Math students! Are you interested in discovering the University of Waterloo's student computing community? If so, come check out CSC's First-Year Orientation!

✨ Looking to make friends in the CS community and attend upcoming CSC events? CSC’s First-Year Orientation will present to you an introduction about the club, followed by a casual social event, icebreakers, and more for students to meet each other! 🤩

🚀 CSC is an amazing community where you can attend various academic and social events, and build meaningful connections throughout the term and beyond. Make sure to come to CSC’s First-Year Orientation!

📆 Event Date: Sept 15, 7:45 PM - 9:00 PM 

📌 Location: DC 1351

👉 Sign-up through this link: https://bit.ly/f22-orientation