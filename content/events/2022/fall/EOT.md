---
name: 'CSC End of Term Event'
short: 'Come join us for a fun night of winter-themed activities with cool people!'
startDate: 'December 4 2022 19:00'
endDate: 'December 4 2022 22:00'
online: false
location: 'AHS EXP 1689'
poster: 'images/events/2022/fall/EOT.png'
registerLink: https://forms.gle/bCRpHrfyMQZJBhB99
---

🎊 Surprise, it's almost end of term!

🎉 Come join us for a fun night of winter-themed activities with cool people. 

🔥 We will be playing Jeopardy, decorating gingerbread cookies, making goose paintings, playing hot chocolate/marshmallow pong, and ending the night with Christmas karaoke.

🥳 If you want to have some fun before exams, make sure to attend!

⭐ P.S There will be pizza and other free snacks 😄

📆 Event Date: December 4th, 7-10 PM at EXP 1689

👉 Sign up for EOT through https://forms.gle/bCRpHrfyMQZJBhB99 (so we can guarantee food/materials) or feel free to drop in!
