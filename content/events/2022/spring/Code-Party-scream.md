---
name: 'Code Party and Midterm Scream'
short: 'Come hang out and work on homework or side-projects at our first code party of this term! Featuring snacks and a stress relieving synchronized scream.'
startDate: 'June 17 2022 19:00'
endDate: 'June 17 2022 21:00'
online: false
location: 'STC 0060'
poster: 'images/events/2022/spring/Code-Party-Scream.png'
registerLink: https://bit.ly/s22-midtermscream-codeparty-signup
---
📣 Do you ever feel like screaming on the top of your lungs? 🙀 Then CSC’s Code Party and Midterm Scream is where you need to be! 

🤩 Come to our first code party of the term! You can chill out, work on side-projects, or finish up on homework and assignments. 📚 There will also be snacks while you are working away or just hanging out with a fellow CSC friend. At 9 PM we will head out to the Rock Garden to let out a synchronized, one-minute long scream to relieve midterm stress! 🥳

Code Party:
📆 Event Date: June 17th from 7 - 9 PM

📌 Location: STC 0060

🙀 The Midterm Scream will take place at 9 PM in the Rock Garden!

👉 Sign-up through this link: https://bit.ly/s22-midtermscream-codeparty-signup

We hope to see you there! 
