---
name: 'Bond-fire'
short: "Join CSC and WICS for a bonfire with s'mores and snacks, on us!"
startDate: 'May 29 2022 19:00'
endDate: 'May 29 2022 22:00'
online: false
location: 'Laurel Creek Fire Pit'
poster: 'images/events/2022/spring/Bondfire.png'
registerLink: https://bit.ly/s22-bonfire-signup 
---

Would you like to meet new friends and talk with like-minded individuals in person? 🔥 Come join CSC and WICS for a bonfire outside at the Laurel Creek firepit (across Ring. Rd. from EV3) on May 29th 2022 from 7:00pm-10:00pm EST, and enjoy a wonderful time with lots of pizza and snacks! 🍕 The firepit location will be in the application form. Feel free to bring your own roasting skewer and instruments to jam along to 🤘

👉Sign up at https://bit.ly/s22-bonfire-signup or email us at exec@csclub.uwaterloo.ca!

📅 Deadline to sign up: May 27th, 2022 at 11:59 PM EST.