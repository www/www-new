---
name: 'Spring 2022 Elections'
short: 'CS Club will be holding elections for the Spring 2022 term on Thursday, May 5th at 5:30 pm in DC 1351.'
startDate: 'May 5 2022 17:30'
online: false
location: 'DC 1351'
poster: 'images/events/2022/spring/Spring-2022-Elections.png'
---

CS Club will be holding elections for the Spring 2022 term on Thursday, May 5th at 5:30 pm in DC 1351.
Come to learn more about CSC, sign up for membership, and vote on our new execs!
The president, vice-president, treasurer, and assistant vice-president will be elected, and the sysadmin will be appointed.

If you have any questions about elections, please email cro@csclub.uwaterloo.ca.
