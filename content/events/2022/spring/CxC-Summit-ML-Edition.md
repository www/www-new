---
name: 'CxC Summit - ML Edition'
short: 'Join CSC’s first Clubs x Companies Summit to learn about Machine Learning and network with some UW tech clubs and prestigious companies!'
startDate: 'May 19 2022 18:00'
endDate: 'May 29 2022 18:00'
online: true
location: 'Online'
poster: 'images/events/2022/spring/CxC-Summit.png'
registerLink: https://bit.ly/3KMDftE
---

📣 CxC Summit - ML Edition 📣

🤩 Interested in learning about Machine Learning? Want to gain more experience by deploying NLP or image processing projects? Join CSC's first Clubs x Companies Summit to learn and network with some UW tech clubs and prestigious companies!

💻 This weeklong online learnathon will include beginner-friendly workshops and networking events. Participants can tackle different ML challenges created by the clubs and companies, ranging from binary classification to sequential image prediction. There are prizes too!

📅 Event Date: Thursday, May 19th - Sunday, May 29th

👉 Register using this link: https://bit.ly/3KMDftE!

Hope to see you there!
