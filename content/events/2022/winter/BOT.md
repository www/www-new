---
name: 'CSC BOT & Game Night'
short: 'Learn about our plans for the term and play some games with us.'
startDate: 'January 15 2022 19:00'
online: true
location: 'Twitch'
poster: 'images/events/2022/winter/BOT.png'
registerLink: https://forms.gle/3kEhPLu8wt2KtdDi8
---

Kick off the winter term with CS Club’s BOT event! ☃️ Interested in attending upcoming CSC events? Want to meet others in the CS community?

💻 Come join us on Twitch to learn about how you can become a member of CSC and the fun events we have planned for next term. 🎲 Stay for games and socials on the CSC Discord!

Registration is not required to attend! We’ll just be sending you an email reminder, as well as inviting you to our calendar event.

🗓️ Event Date: Saturday, January 15th from 7:00-8:00 pm ET via Twitch (https://www.twitch.tv/uwcsclub) and Discord (https://discord.gg/pHfYBCg)

👉 Register at https://forms.gle/3kEhPLu8wt2KtdDi8! Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.

Looking forward to seeing you there!! 🤗
