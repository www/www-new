---
name: "Unix 101: SSH, Bash & CLI Commands"
short: "Join CSC's Systems Committee (syscom) for the first session of an introductory series on GNU/Linux!"
startDate: "March 19 2022 14:00"
online: true
location: "Twitch"
poster: 'images/events/2022/winter/Unix-101-1.png'
---

Want to learn about Bash, SSH and other terminal commands? 👨‍💻 Want to prepare for, or extend your skills from CS246? 📚 Join CSC’s Systems Committee (syscom) for our first introductory session on GNU/Linux 💻! 

🙆‍♂️ Unix 101 will be a 4-part, beginner-friendly series on the Unix operating system. Attendance in previous sessions is not required to understand the proceeding ones, but is strongly encouraged to make sure you get the most out of our workshops! ✨

👆 We additionally recommend joining CSC to access our machines for more hands-on activity. Learn more about how you can do so at https://csclub.uwaterloo.ca/get-involved/! 

🧠 Head over to our Twitch on March 19th from 2-3PM EST for our first session of Unix 101!

📅 Event Date: Saturday March 19th @ 2-3PM EST on Twitch
🔗 Twitch: https://www.twitch.tv/uwcsclub 
