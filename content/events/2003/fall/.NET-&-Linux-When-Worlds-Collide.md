---
name: '.NET & Linux: When Worlds Collide'
short: 'A talk by James Perry'
startDate: 'October 21 2003 16:30'
online: false
location: 'MC2065'
---

.NET is Microsoft's new development platform, including amongst other things a language called C# and a class library for various operating system services. .NET aims to be portable, although it is currently mostly only used on Windows systems.

With the full backing of Microsoft, it seems unlikely that .NET will disappear any time soon. There are several efforts underway to bring .NET to the GNU/Linux platform. Hosted by the Computer Science Club, this talk will discuss a number of the issues surrounding .NET and Linux.

