---
name: 'CS Pints With Profs'
short: 'Come have a pint with your favourite CS profs!'
startDate: 'November 05 2003 16:30'
online: false
location: 'Grad House Pub (Green Room)'
---

Come meet CS profs in a relaxed atmosphere this Wednesday at the Grad House (by South Campus Hall). This is your chance to meet those CS profs you enjoyed in lectures in person, have a chat with them and find out what they're doing outside the lecture halls.

We'll be providing free food, including hamburgers and nachos, and the Grad House offers a great selection of drinks.

If you'd like to invite a particular prof, stop by on the third floor of the MC (outside of the Comfy) to pick up an invitation.

Persons of all ages are welcome!

