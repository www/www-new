---
name: 'July
  Exec Meeting'
short: ' See Abstract for minutes '
startDate: 'July 24 2003 15:00'
online: false
location: 'CSC Office'
---

<pre>--paying Simon for Sugar
-Unanimous yea.
-ACTION ITEM: Mark
	Expense this to MathSoc in lieu of foreign speaker.

--We currently have (including CD-R and pop-income not
currently in safe) $972.85
-We have $359.02 on budget that we can expense to MathSoc.

--We got MEF money for books and video card. Funding for
wireless microphone is dependent on whether MFCF is
willing to host it.
-Funding for casters was denied.
-Shopping for the Video card.
-Expecting it after August (Stefanus shopping for it.)
-Will have to hear back regarding the microphone, best to
delay that now, discuss it with MEF.
	-Better to do it this term, so it doesn't get lost.
	-Let MFCF know about this concern.
-Regarding books, can be done anytime before September.

--Events feedback
	-Generally, Jim Eliot talk when really well.
		-Apparently he was generally offensive.
	-When was the LaTeX talk? End of the month.
	-Kegger at Jim's place on the 16th.

--Getting people in on the 6th, 7th, 8th for csc commercials
filmed by Jason
	-Hang out in here, and he'll make a CSC commercial.
	-Co-ordinate when everyone should be in here, so we can email Jason.

--CEO progress
	-CEO needs it's database changed to use ISBN as a primary key.
	-Needs functionality to take out/return books.

--Mark just entered financial stuff into GNUcash

--Choose CRO for next term.
-Stefanus has expressed desire not to be CRO.
-Gary Simmons was suggested (and he accepted)
-Unanimous yea

--Mike Biggs has to get here naked.
	-Four unanimous votes.
	-Nakedness only applies to getting here, not being here.


From last meeting:
ACTION ITEM: Biggs and Cass
-get labelmaker tape, masking tape
whiteboard makers, coloured paper, CD sleeves
-keep receipts for CSC office expenses.

How is the progress on allowing executives and voters to be non-math
members?
-The vote is coming up Monday.
-Proposal: Anyone who is a paying member can be a member
	-So you can either do two things:
	Pay MathSoc fees, or
	Get your faculty society to recognize CSC as a club.

Stefanus wanted to mention that we should talk to Yolanda,
Craig or Louie about a EYT event for frosh week.
-Organized by Meg.
-Sugar Mountain trying to hook all the Frosh
ACTION ITEM: Jim
-Email Meg

Reminder for Next Year's executive.
-September 16th @ 5:00pm, get a table for Clubs day, and 17th
and 18th, maintain the booth (full day events).
-Update pamphlets.
ACTION ITEM: Gary
-There should be executive before then

Note: There needs to be a private section in the CSC Procedures Manual.
(Only accessible by shell)
ACTION ITEM: Simon
-Do it.

ACTION ITEM: Mike
-Talk to Plantops about:
-Locks on doors
-Mounting corkboard.
-Talk about CSC Sign
  </pre>

