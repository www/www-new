---
name: 'May 22 Exec Meeting'
short: 'The execs discuss what needs discussion'
startDate: 'May 22 2003 16:30'
online: false
location: 'MC3036 CSC Office'
---

<pre>
Minutes for CSC Exec Meeting
May 22, 2003


* Add staff to burners group.
	-- Only office staff (people who do stuff) on burners list
  	-- No objections from executives

* We still need a webmaster, imapd
	-- Action Item: Mike
		--Check for pop delivery services (Like Grocery Gateway)
		so that we can replace imapd with an automated cronjob
		-- If this gets implemented, we must make sure that
		someone is around to receive the pop whenever it is
		delivered.

* Budgets
    	Action Item: Simon
		-- Make sure execs receive a copy of the proposed budget
	Action Item: Mark
		-- Look into claiming money from Mathsoc for the last
		term.
	--Will be looked over the week after next Monday at the Mathsoc
	Budget meeting.
	--June 27th is the WEF (Engineering Endowment Fund) deadline
	--EngSoc proposal for donations by the end of the month
	-- Around 15 events planned
		--Foreign Speaker
			--CS Departmant will pay for flight
			-- We can pay local expenses
		--Pints with Profs
		--Ro-Sham-Bo

*Changes in the MathSoc Clubs Policy
	Action Item: Jim and Stefanus
		--Bring thus up with MathSoc
		--Might be good to talk to Bioinformatics about this, as
		they have science faculty members to take care of as well.
	--Major issue: People who revoke their Mathsoc fees can still be
	voting 	members
	--We want it so that only people who have paid dues to Mathsoc
	can vote.
	--Execs should not take back fees, as that is bad form.
	--All execs unanimously agreed with this proposal

*Confirming that we have free printing and photocopying
	Action Item: Mark
		--Does Faculty of Math billing code apply to CSC
		(as Faculty of Math department?)
		-- Procedures manual has a billing code, but it should
		be confirmed.
		-- Ask MUO, then Shirley after that.
	Action Item: Simon
		--Apparently there is a special Watcard that provides
		free printing from MFCF
		--We do not know what account it is mapped to,
		or the password.

* Getting csc_disk, csc, csc_ceo accounts on undergrad to work again.
	Action Item: Phil
		-- Get csc-disk back up for student use.
		-- What group permissions do we need?
		-- CSC-Disk should be used as a repository for custom
		window managers, Mozilla, etc... (selling factor for
		CSC accounts)
		-- We should also have an announcement (MOTD, perhaps?)
		that we are providing and supporting this software.
			--Consider: Having university-wide accessible
			binaries might be a pain, as different machines
			might require different compilations.
		-- CSC-Disk is full of user data. Should that be blown away?

*Getting locker #7 from MathSoc (Don't we already have lockers 788 and
789?)
	--Why were the locks snipped? (Bring up at council meeting)
	--We would prefer one combo-lock and one key-lock.

* Review of the CSC office organization
	Action Item: Damien
		--Give Mike sudo access for shutdown
			--Will be rewiring stuff on Saturday
				--involves re-plugging machines
	Action Item: Simon
		--Get rubber wheels for chairs

	Action Item: Mike
		-- Ask PlantOps about:
			--Waxing floors
			--Installing Electronic Lock (asap)
				--According to Faculty of Math,
				we shouldn't need keys.
				--Currently, we still need keys
				--It is kosher to install Electronic lock
				--This provides access right control as
				compared to key-control.
				--Might be long term project.
				--Will green men do it?
			--Steam-clean chairs (at least once a term)
			--Cork-board
			--Making ugly wall prettier
				--PlantOps knows about office
				organization, making environment better.
	--Whiteboards need to be put up
	--Proposal: Cork-board on pillar (no objections)
	--Metal frames on Whiteboard will be in least annoying place

*Do we provide public stapler access?
	--People are often unappreciative and rude
	--Sign -  "Stapler if you say please" -- Unanimously voted
						 stapler policy

*MathSoc Sign
	--Action Item: Jim
		--Find out where to get CSC sign before Monday so we
		can claim it in old budget.

* Librarian's Report
	--Action Item: Jim
		--Find perl volunteer to finish CEO
		--Force Stefanus to export CVS tree and put onto Peri

	--Books were scanned into system with help of Mark
	--All books with valid barcodes entered into system on
	May 20th
	--Books without valid barcodes are not in system
		--Someone needs to do it
	--Plan is to implement Dewey decimal system
		--May be inefficient as all books are about CS
		--We will figure out a system later
	--No plans to purchase new books
	--Librarian's Request:	Office Staff should not lend out books
	that do not have barcodes (No objects to request)
	--We are still using /media/iso/request to track books
	--Should be charge late fees for books?
	--We should have money in budget for repairing,maintaining books
	--Before spending money on maintaining books, check if DC will
	do it
		--will it be cheaper/easier/better?

*Setting up extra quota for fun and profit.
	-- We don't implement quota properly right now
	-- Low demand for extra quota
	-- Counterpoint: Old CSC made tons of money
	-- Counter-counter-point: It's not that necessary for extra
	quota nowadays.
	-- Executives voted against proposal.

*Jim will spam with an update about the term
	--Consider making it opt-in
	--One email from a service you are using should be considered
	reasonable mass mailing

*Should Jim bring anything up at the MathSoc meeting?**
	-- Has a list

* Student branches for ACM and IEEE
	Action Item: Gaelan
		--Contact IEEE Computing Society in UW and ask if they want
		to merge or transfer society to us
	--Simon volunteers to be put down as exec for ACM
		--ACM rules state requirement that exec is a ACM member
	--Do we renew Calum's ACM membership?
		--Yes (3 Yes; 1 No; 1 Abstention)
	--ACM membership money in budget
	--ACM Student chapter form has not come in

* What to do with the donated Procedures Manual?
	--Term Task for webpage:
		--Put procedures manual on web-page.
		--Merge with current manual
	--We don't have a hard copy
	--Would be a good thing to read.
	--Many parts need updating

</pre>

