---
name: 'vi: the visual editor'
short: 'It''s not 6.'
startDate: 'June 19 2003 16:30'
online: false
location: 'MC2037'
---

In 1976, a University of California Berkeley student by the name of Bill Joy got sick of his text editor, ex. So he hacked it such that he could read his document as he wrote it. The result was "vi", which stands for VIsual editor. Today, it is shipped with every modern Unix system, due to its global influence.

In this talk, you will learn how to use vi to edit documents quickly and efficiently. At the end, you should be able to:

- Navigate and search through documents
- Cut, copy, and paste across documents
- Search and replace regular expressions

<!-- -->

If you do not have a Math computer account, don't panic; one will be lent to you for the duration of this class.

