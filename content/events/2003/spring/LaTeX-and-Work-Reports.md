---
name: 'LaTeX and Work Reports'
short: 'Writing beautiful work reports'
startDate: 'July 31 2003 16:30'
online: false
location: 'MC4064'
---

The work report is a familiar chore for any co-op student. Not only is there a report to write, but to add insult to injury, your report is returned if you do not follow your departmental guidelines.

Fear no more! In this talk, you will learn how to use LaTeX and a specially developed class to automatically format your work reports. This talk is especially useful to Mathematics, Computer Science, Electrical & Computer Engineering, and Software Engineeering co-op students about to go on work term.

[http://www.eng.uwaterloo.ca/\~sfllaw/programs/uw-wkrpt/](<http://www.eng.uwaterloo.ca/~sfllaw/programs/uw-wkrpt/>)

