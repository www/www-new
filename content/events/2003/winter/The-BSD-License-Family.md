---
name: 'The BSD License Family'
short: 'Free for all'
startDate: 'February 27 2003 18:00'
online: false
location: 'MC1085'
---

Before the GNU project ever existed, before the phrase "Free Software" was ever coined, students and researchers at the University of California, Berkeley were already practising it. They had acquired the source code to a little-known operating system developed at AT&T Bell Laboratories, and were creating improvements at a ferocious rate.

These improvements were sent back to Bell Labs, and shared to other Universities. Each of them were licensed under what is now known as the "Original BSD license". Find out what this license means, its implications, and what are its descendents by attending this short talk.

