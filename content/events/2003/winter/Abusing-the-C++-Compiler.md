---
name: 'Abusing the C++ Compiler'
short: 'Abusing template metaprogramming in C++'
startDate: 'March 26 2003 18:00'
online: false
location: 'MC2065'
---

Templates are a useful feature in C++ when it comes to writing type-independent data structures and algorithms. But that's not all they can be used for. Essentially, it is possible to write certain programs in C++ that execute completely at compile-time rather than run-time. Combined with some optimisations this is an interesting twist on regular C++ programming.

This talk will give a short overview of the features of templates and then go on to describe how to "abuse" templates to perform complex computations at compile time. The speaker will present three programs of increasing complexity which execute at compile time. First a factorial listing program, then a prime listing program will be presented. Finally the talk will conclude with the presentation of a *Mandelbrot generator running at compile time.*

Some basic knowledge of C++ will be assumed.

