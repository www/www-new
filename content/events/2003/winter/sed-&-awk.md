---
name: 'sed & awk'
short: 'Unix text editing'
startDate: 'January 30 2003 18:30'
online: false
location: 'MC1085'
---

*sed* is the Unix stream editor. A powerful way to automatically edit a large batch of text. *awk* is a programming language that allows you to manipulate structured data into formatted reports.

Both of these tools come from early Unix, and both are still useful today. Although modern programming languages such as Perl, Python, and Ruby have largely replaced the humble *sed* and *awk*, they still have their place in every Unix user's toolkit.

