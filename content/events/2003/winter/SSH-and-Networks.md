---
name: 'SSH and Networks'
short: 'Once more into the breach'
startDate: 'March 27 2003 18:30'
online: false
location: 'MC1085'
---

The Secure Shell (SSH) has now replaced traditional remote login tools such as *rsh*, *rlogin*, *rexec* and *telnet*. It is used to provide secure, authenticated, encrypted communications between remote systems. However, the SSH protocol provides for much more than this.

In this talk, we will discuss using SSH to its full extent. Topics to be covered include:

- Remote logins
- Remote execution
- Password-free authentication
- X11 forwarding
- TCP forwarding
- SOCKS tunnelling

<!-- -->

