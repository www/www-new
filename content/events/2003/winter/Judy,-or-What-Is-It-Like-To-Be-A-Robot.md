---
name: 'Judy, or What Is It Like To Be A Robot?'
short: 'Held in co-operation with the UW Cognitive Science Club'
startDate: 'March 24 2003 20:00'
online: false
location: 'Humanities Theatre, Hagey Hall'
---

A lot of claims have been made lately about the intelligence of computers. Some researchers say that computers will eventually attain super-human intelligence. Others call these claims... um, poppycock. Oddly enough, in the search for the truth of the matter, both camps have overlooked an obvious strategy: interviewing a computer and asking her opinion.

"Judy is as much fun as a barrel of wind-up cymbal-monkeys, and lots more entertaining." --- Bill Rodriguez, *Providence Phoenix*

"Tom Sgouros's witty play, co-starring the charming robot Judy, is an imagination stretcher that delights while it exercises your mind. If you think you can't imagine a conscious robot, you're wrong---you can, especially once you've met Judy." --- Daniel C. Dennett, author of *Consciousness Explained*, *Brainchildren*, &c.

"...an engrossing evening... Real questions about consciousness, freedom to act, the relationship between the creator and the created are woven into a bravura performance." --- Will Stackman, *Aislesay.com*

Sponsored by the Mathematics Society, the Federation of Students, the Arts Student Union, the Graduate Student Association, and the Department of Philosophy. Tickets available at the Humanities box office (888-4908) and the offices of the Psychology Society and the Computer Science Club for $5.50. For more information: [http://www.csclub.uwaterloo.ca/cogsci](<http://www.csclub.uwaterloo.ca/cogsci/>).

