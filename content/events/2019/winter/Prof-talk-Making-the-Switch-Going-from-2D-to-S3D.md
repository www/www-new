---
name: 'Prof talk: Making the Switch: Going from 2D to S3D'
short: 'We will be having our second prof talk of the term with Lesley Istead, on April 5, 3:30 pm in STC 0020. She will be talking about the problems and algorithms used in the film industry to go from 2D to S3D. Come listen to the talk and enjoy some free food!'
startDate: 'April 05 2019 15:30'
online: false
location: 'STC 0020'
---

We will be having our second prof talk of the term with Lesley Istead, on April 5, 3:30 pm in STC 0020. She will be talking about the problems faced, and algorithms used in the film industry to go from 2D to S3D. Come listen to the talk and enjoy some free food!

For the last 10 years, most blockbusters have been released in both 2D and stereoscopic 3D. The move from 2D to S3D is non-trivial and involves many new algorithms and technologies. But there are still many problems to be solved and many improvements that can still be made. In this talk, we'll explore artifacts of traditional 2D photography, methods to represent them comfortably in S3D, and their meaning in a 3D world.

