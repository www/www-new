---
name: 'End of Term Event'
short: 'The Computer Science Club will be holding a end of term event on Monday, July 29th	at MC Comfy at 6:00 PM.'
startDate: 'July 29 2019 18:00'
online: false
location: 'MC Comfy'
---

Come hang out and celebrate the end of the term with us. There will be pizza,	ice cream sandwiches, and juice boxes. :)

