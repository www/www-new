---
name: 'WiCS x CSC Bondfire Night'
short: 'The Computer Science Club will be holding a joint event with Women in Computer Science on Wednesday, June 5th at MC Comfy at 7:00 PM. NOTE: the event has been moved from the Laurel Creek Firepit due to rain.'
startDate: 'June 05 2019 19:00'
online: false
location: 'MOVED to MC Comfy'
---

Come and join us for a night of Campfire, friends, fun and yeah Free food 😁, we'll be having Pitas, drinks, water(Gotta Keep my Fam hydrated eh! 😉) and last but not the least we'll be having S'mores too, because any Campfire is incomplete without them.

