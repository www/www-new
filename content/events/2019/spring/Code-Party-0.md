---
name: 'Code Party 0'
short: 'The Computer Science Club will be holding Code Party 0 on Tuesday, June 18th	at PHY 150 at 6:00 PM.'
startDate: 'June 18 2019 18:00'
online: false
location: 'PHY 150'
---

Come hang out with us! Study for midterms, do assignments, work on side projects, or	prep for interviews. We'll have free food - we aren't ordering pizza, so don't worry. :)

