---
name: 'Technical Interview Prep'
short: 'Our first workshop of the term! Fatema and Arshia will be heading a workshop on how to prepare for technical interviews.Got technical interviews? Come out!'
startDate: 'February 07 2018 17:30'
online: false
location: 'QNC 1502'
---

Details and abstract TBA. So far, we're going to be meeting and going over how to get really good at technical interviews.

We will:

- Talk about the different kinds of programming jobs you can apply and interview for
- Present some general advice for tech interviews
- have sessions in parallel for software engineering, frontend engineering, security, and devops interviews

<!-- -->

If you're in 2B or below, you'll probably find this event helpful! Anyone is welcome to attend, however.

