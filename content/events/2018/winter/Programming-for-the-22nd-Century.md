---
name: 'Programming for the 22nd Century'
short: 'We are bringing an out-of-town speaker, John "maddog" Hall, to come speak! Come to this event, where he will be talking about changes in programming paradigms since the invention of C, and the discussion event tomorrow.'
startDate: 'March 12 2018 18:00'
online: false
location: 'MC Comfy'
---

Abstract: Many things have changed since the early days of programming, but many programs are written as if they were for the machines of the 20th century which had small memories, no cache, single core CPUs, small address spaces. Even the definition of "performance" has changed. This talk will investigate some of these issues and hopefully lead people to better programming.

