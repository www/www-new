---
name: 'Sound: From microphones to speakers and everything inbetween'
short: 'The CSC will be hosting a Prof Talk by Richard Mann.'
startDate: 'July 17 2018 18:00'
online: false
location: 'MC 4041'
---

The CSC will be hosting a Prof Talk by Richard Mann.

When you hit record on your phone how is the sound recorded? Air pressure changes come to a microphone, which converts electricity to voltage. The analog signal is digitized by an analog to digital converter (ADC), and finally stored as a digital file in memory. For playback, the process is reversed, and output to head phones or a (tiny) speaker.

In a loud room does your phone distort? Can you hear the sound clearly? Phones are getting better. But what is needed to achieve professional quality sound recording?

