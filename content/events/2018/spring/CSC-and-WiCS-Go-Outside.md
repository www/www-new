---
name: 'CSC and WiCS Go Outside'
short: 'Come join us for a bonfire outside at the Laurel Creek firepit (across Ring. Rd. from EV3) with fellow CSC and WiCS members. Smores and snacks will be provided.'
startDate: 'June 04 2018 18:00'
online: false
location: 'Laurel Creek Firepit'
---

Come join us for a bonfire outside at the Laurel Creek firepit (across Ring. Rd. from EV3) with fellow CSC and WiCS members. Smores and snacks will be provided.

The firepit we will be using is in the bottom right of [this map.](<https://uwaterloo.ca/economics/sites/ca.economics/files/uploads/files/firepit_map_oct_2012.pdf>)

