---
name: 'Understanding machine learning - a theory perspective'
short: 'Professor Shai Ben-David will discuss the basic principles behind machine learning and how they relate to some of	the headline-making practical tools, in addition to the major research challenges and directions that address	the fast expanding scope of potential machine learning applications.'
startDate: 'June 15 2017 18:00'
online: false
location: 'MC4060'
---

We are all aware that we live in the era of ("big") data. In contrast to classical scientists	that devoted much of their resources to collecting data, nowadays researchers are flooded with	data and the focus has switched to trying to make sense of and utilize the big and complex available data.	Machine learning is aimed to use computer power to do just that.

It is therefore no wonder that machine learning is currently a hot topic. Evidence is all over the map, from	NYTimes articles to being a top priority for research investments by Google, Amazon, Microsoft and Facebook.	Throughout its (short) history, machine learning has enjoyed fruitful interactions between theory and practice.	The growing awareness to its power keeps stimulating research towards new applications to the field, which in turn	spur the development of algorithms and inspire new frontiers for our theoretical pursuit.

In this talk Professor Shai Ben-David will explain the basic principles behind machine learning and how these principles relate to some	of headline-making practical tools. Ben-David will also describe some of the major research challenges and research	directions that address the fast expanding scope of potential machine learning applications.

