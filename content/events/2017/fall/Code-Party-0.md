---
name: 'Code Party 0'
short: 'The CS Club is hosting our first Code Party of the term (Friday September 29th) from 6:30pm until late in the evening in M3 1006!Come code with us, eat some food, do some things.'
startDate: 'September 29 2017 18:30'
online: false
location: 'M3 1006'
---

The CS Club is hosting our first Code Party of the term (Friday September 29th) from 6:30pm until late in the evening in M3 1006!

Come code with us, eat some food, do some things.

Personal projects you want to work on? Homework projects you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 0 and do it, with great company and great food.

Come any time after 6:30pm, there will be snacks and we'll be ordering pizza at around 7:00pm!

