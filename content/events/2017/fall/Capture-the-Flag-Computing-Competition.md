---
name: 'Capture the Flag Computing Competition'
short: 'Test your programming, web, networking, and trivial skillsLearn how to reverse engineer, crack codes, and use various tools.Win prizes!'
startDate: 'November 14 2017 18:00'
online: false
location: 'STC 0020'
---

Test your programming, web, networking, and trivial skills

Learn how to reverse engineer, crack codes, and use various tools.

Win prizes!

