---
name: 'Alumni Tech Talk'
short: 'Alex Tsay from AeroFS will talk about the high availability distributed file systems they develop.The CAP Theorem outlined the fundamental limitations of a distributed system. When designing a distributed system, one has to constantly be aware of the trade-off between consistency and availability. Most distributed systems are designed with consistency in mind. However, AeroFS has decided to build a high-availability file system instead. In this tech talk, I''ll be presenting an overview of AeroFS file system, advantages and challenges of a high-availability file system, and examine the inner workings of AeroFS''s core syncing algorithm.'
startDate: 'January 21 2015 18:00'
online: false
location: 'MC 2017'
---

Alex Tsay from AeroFS will talk about the high availability distributed file systems they develop.

The CAP Theorem outlined the fundamental limitations of a distributed system. When designing a distributed system, one has to constantly be aware of the trade-off between consistency and availability. Most distributed systems are designed with consistency in mind. However, AeroFS has decided to build a high-availability file system instead. In this tech talk, I'll be presenting an overview of AeroFS file system, advantages and challenges of a high-availability file system, and examine the inner workings of AeroFS's core syncing algorithm.

