---
name: 'Winter 2015 Elections'
short: 'Elections for Winter 2015 are being held! Submit a nomination and join your fellow members in choosing this term''s CSC executive. (Please note the time change to 7PM.)'
startDate: 'January 15 2015 19:00'
online: false
location: 'Comfy Lounge'
---

The Computer Science Club will be holding its termly elections this upcoming Thursday, Jan. 15 at 6PM in the Comfy Lounge (MC 3001). During the election, the president, vice-president, treasurer and secretary will be elected, the sysadmin will be ratified, and the librarian and office manager will be appointed.

Nominations are now closed. The candidates are:

- President:- Luke Franceschini (<tt>l3france</tt>

    )
    - Gianni Gambetti (<tt>glgambet</tt>

    )
    - Ford Peprah (<tt>hkpeprah</tt>

    )
    - Khashayar Pourdeilami (<tt>kpourdei</tt>

    )

    <!-- -->

- Vice-President:- Luke Franceschini (<tt>l3france</tt>

    )
    - Gianni Gambetti (<tt>glgambet</tt>

    )
    - Patrick Melanson (<tt>pj2melan</tt>

    )
    - Ford Peprah (<tt>hkpeprah</tt>

    )
    - Khashayar Pourdeilami (<tt>kpourdei</tt>

    )

    <!-- -->

- Treasurer:- Weitian Ding (<tt>wt2ding</tt>

    )
    - Aishwarya Gupta (<tt>a72gupta</tt>

    )
    - Edward Lee (<tt>e45lee</tt>

    )

    <!-- -->

- Secretary:- Ilia "itchy" Chtcherbakov (<tt>ischtche</tt>

    )
    - Luke Franceschini (<tt>l3france</tt>

    )
    - Patrick Melanson (<tt>pj2melan</tt>

    )
    - Ford Peprah (<tt>hkpeprah</tt>

    )
    - Khashayar Pourdeilami (<tt>kpourdei</tt>

    )

    <!-- -->


<!-- -->

Voting will be heads-down, hands-up, restricted to MathSoc social members. If you'd like to review the elections procedure, you can visit our [Constitution](<http://csclub.uwaterloo.ca/about/constitution#officers>) page.

