---
name: 'SAT and SMT solvers'
short: 'Murphy Berzish explains how to programmatically determine if a program is satisfiable, and how to find a concrete counterexample if it is unsatisfiable. At the core are SAT/SMT solvers. SAT theory deals with Boolean Satisfiability solvers, while SMT theory--Satisfiability Modulo a Theory--allows SMT to be extended to common data structures. Free food!'
startDate: 'March 03 2015 18:00'
online: false
location: 'MC 2038'
---

Does your program have an overflow error? Will it work with all inputs? How do you know for sure? Test cases are the bread and butter of resilient design, but bugs still sneak into software. What if we could prove our programs are error-free? 


 Boolean Satisfiability (SAT) solvers determine the ‘satisfiability’ of boolean set of equations for a set of inputs. An SMT solver (Satisfiability Modulo a Theory) applies SMT to bit-vectors, strings, arrays, and more. Together, we can reduce a program and prove it is satisfiable, or provide a concrete counter-example. The implications of this are computer-aided reasoning tools for error-checking in addition to much more robust programs. 


 In this talk Murphy Berzish will give an overview of SAT/SMT theory and some real-world solution methods. He will also demonstrate applications of SAT/SMT solvers in theorem proving, model checking, and program verification. 


 What else? Oh yes, refreshments and drinks will be served. Come out!

