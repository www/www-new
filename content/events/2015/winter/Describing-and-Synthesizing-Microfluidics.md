---
name: 'Describing and Synthesizing Microfluidics'
short: 'Derek Rayside presents current research on the field of microfluidics. Microfluidics are currently developed mainly by trial and error. How can this be improved?'
startDate: 'April 02 2015 17:30'
online: false
location: 'MC 4020'
---

Microfluidics is an exciting new area concerned with designing devices that perform some medical diagnoses and chemical synthesis tasks orders of magnitude faster and less expensively than traditional techniques. However, microfluidic device design is currently a black art, akin to how digital circuits were designed before 1980. 


 We have developed a hardware description language that is appropriate for the description and synthesis of both single-phase and multi-phase microfluidic devices. These are new results that have not yet been published. This is collaborative work with other research groups in Mechanical Engineering, Chemical Engineering, and Electrical Engineering.

