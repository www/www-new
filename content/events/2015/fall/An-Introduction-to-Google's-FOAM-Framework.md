---
name: 'An Introduction to Google''s FOAM Framework'
short: 'An introduction to Google''s FOAM framework, an open-source modeling framework written in Javascript, by Google''s Kevin Greer.'
startDate: 'November 26 2015 05:00'
online: false
location: 'MC 4063'
---

FOAM is an open-source modeling framework written in Javascript. With FOAM, you can create Domain Specific Languages (DSLs), which are high-level models that can be interpreted or compiled to different languages or environments (Java/Android, Swift/iOS, and JS/Web). Currently, it supports DSLs for entities/classes, parsers, animations, database queries, interactive documents, and, most importantly, new DSLs.

FOAM supports building text, HTML, and graphical views for DSLs using a small Model View Controller (MVC) library, which is itself modeled with FOAM. This library can also be used by modeled Javascript applications.

FOAM increases developer productivity by allowing them to express solutions at a higher, more succinct level. The MVC library also increases application performance through its efficient data-binding, caching, and query-optimization mechanisms.

Learn more at http://foamdev.com

You can get in contact with Kevin Greer on twitter, [@kgrgreer](<https://twitter.com/kgrgreer>).

