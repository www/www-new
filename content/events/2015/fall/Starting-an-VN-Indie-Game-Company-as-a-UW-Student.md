---
name: 'Starting an VN Indie Game Company as a UW Student'
short: 'Come out to a talk by Alfe Clemencio!Many people want to make games as signified by all the game development schools that are appearing everywhere. But how would you do it as a UW student? This talk shares the experiences of how making Sakura River Interactive was founded without any Angel/VC investment.'
startDate: 'October 07 2015 17:30'
online: false
location: 'MC 4061'
---

Come out to a talk by Alfe Clemencio!

Many people want to make games as signified by all the game development schools that are appearing everywhere. But how would you do it as a UW student? This talk shares the experiences of how making Sakura River Interactive was founded without any Angel/VC investment.

The talk will start off with inspiration drawn of Co-op Japan, to it's beginnings at Velocity. Then a reflection of how various game development and business skills was obtained in the unexpected ways at UW will follow. How the application of probabilities, theory of computation, physical/psychological attraction theories was used in the development of the company's first game. Finally how various Computer Science theories helped evaluate feasibility of several potential incoming business deals.

[From Sakura River interactive](<http://www.sakurariver.ca/>)