---
name: 'Fall 2015 Elections'
short: 'The Computer Science Club will be holding elections for the Fall 2015 term on Tuesday, September 22nd in MC Comfy (MC 3001) at 19:00. During the meeting, the president, vice-president, treasurer and secretary will be elected, the sysadmin will be ratified, and the librarian and office manager will be appointed.See inside for nominations.'
startDate: 'September 22 2015 19:00'
online: false
location: 'MC 3001'
---

The Computer Science Club will be holding elections for the Fall 2015 term on Tuesday, September 22nd in MC Comfy (MC 3001) at 19:00. During the meeting, the president, vice-president, treasurer and secretary will be elected, the sysadmin will be ratified, and the librarian and office manager will be appointed.

If you'd like to run for any of these positions or nominate someone, you can write your name on the board in the CSC office (MC 3036/3037) or send me (Charlie) an email at cro@csclub.uwaterloo.ca. You can also deposit nominations in the CSC mailbox in MathSoc or present them to me in person. Nominations will close at 18:00 on Monday, September 21st. All members are welcome to run! First-years are especially encouraged to run for secretary, office manager, and librarian, but they are not limited to those positions.

