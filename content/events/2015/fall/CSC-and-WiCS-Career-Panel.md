---
name: 'CSC and WiCS Career Panel'
short: 'The CSC is joining WiCS to host a career panel! Come hear from Waterloo alumni as they speak about their time at Waterloo, experience with coop, and life beyond the university. Please register at http://bit.ly/1OyJP6D'
startDate: 'September 24 2015 16:30'
online: false
location: 'EIT 3142'
---

The CSC is joining WiCS to host a career panel! Come hear from Waterloo alumni as they speak about their time at Waterloo, experience with coop, and life beyond the university. A great chance to network and seek advice!

The panelists are:

- Joanne Mckinley - Software Engineer, Google
- Carol Kilner - COO, BanaLogic Corporation
- Harshal Jethwa - Consultant, Infusion
- Dan Collens - CTO, Big Roads

<!-- -->

Food and drinks will be provided! Please register [here](<https://docs.google.com/forms/d/1G-8LFLgxQUkahXvODpS2cVSvceNibTt18Uc8TnhlKI8/viewform?usp=send_form>)

