---
name: 'Cory Doctorow - The War on General Purpose Computing'
short: ' Between walled gardens, surveillance agencies, and political opponents, no matter who''s winning the war on general purpose computing you''re losing. The Computer Science Club will be hosting Cory Doctorow''s talk in the Theatre of the Arts on October 16. '
startDate: 'October 16 2015 19:00'
online: false
location: 'ML Theatre of the Arts'
---

No Matter Who's Winning the War on General Purpose Computing, You're Losing

If cyberwar were a hockey game, it'd be the end of the first period and the score would be tied 500-500. All offense, no defense.

Meanwhile, a horrible convergence has occurred as everyone from car manufacturers to insulin pump makers have adopted the inkjet printer business model, insisting that only their authorized partners can make consumables, software and replacement parts -- with the side-effect of making it a felony to report showstopper, potentially fatal bugs in technology that we live and die by.

And then there's the FBI and the UK's David Cameron, who've joined in with the NSA and GCHQ in insisting that everyone must be vulnerable to Chinese spies and identity thieves and pervert voyeurs so that the spy agencies will always be able to spy on everyone and everything, everywhere.

It's been fifteen years since the copyright wars kicked off, and we're still treating the Internet as a glorified video-on-demand service -- when we're not treating it as a more perfect pornography distribution system, or a jihadi recruitment tool.

It's all of those -- and more. Because it's the nervous system of the 21st century. We've got to stop treating it like a political football.

Cory Doctorow will be talking on Friday October 16, 7pm in the Theatre of the Arts. Admission is free, and the talk will be open to the public. Doors open at 6:30pm. Headsets will be provided for the hard of hearing, email Patrick at pj2melan@uwaterloo.ca . The theatre is wheelchair accessible.

The following books written by Cory will be sold at the event:

- Little Brother
- Homeland
- For the Win
- Makers
- Pirate Cinema
- Information Doesn't want to be free
- In Real Life

<!-- -->

