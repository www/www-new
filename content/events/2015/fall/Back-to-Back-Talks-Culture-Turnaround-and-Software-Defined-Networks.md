---
name: 'Back to Back Talks: Culture Turnaround and Software Defined Networks'
short: 'Back to back talks from John Stix and Francisco Dominguez on turning a company''s culture around and on Software Defined Networks!'
startDate: 'September 30 2015 17:00'
online: false
location: 'DC 1304'
---

Back to back talks from John Stix and Francisco Dominguez on turning a company's culture around and on Software Defined Networks!

John Stix will be talking about how he turned around the corporate culture at Fibernetics Corporation.

Francisco Dominguez will be talking about Software Defined Networks, which for example can turn multiple flakey internet connections into one reliable one.

The speakers are:

- John Stix - President, Fibernetics
- Francisco Dominguez - CTO, Fibernetics

<!-- -->

Food and drinks will be provided!

