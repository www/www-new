---
name: 'Off-the-Record Messaging: Useful Security and Privacy for IM'
short: 'Ian Goldberg'
startDate: 'October 15 2007 16:30'
online: false
location: 'MC4041'
---

Instant messaging (IM) is an increasingly popular mode of communication on the Internet. Although it is used for personal and private conversations, it is not at all a private medium. Not only are all of the messages unencrypted and unauthenticated, but they are all routedthrough a central server, forming a convenient interception point for an attacker. Users would benefit from being able to have truly private conversations over IM, combining the features of encryption, authentication, deniability, and forward secrecy, while working within their existing IM infrastructure.

In this talk, I will discuss "Off-the-Record Messaging" (OTR), a widely used software tool for secure and private instant messaging. I will outline the properties of Useful Security and Privacy Technologies that motivated OTR's design, compare it to other IM security mechanisms, and talk about its ongoing development directions.

Ian Goldberg is an Assistant Professor of Computer Science at the University of Waterloo, where he is a founding member of the Cryptography, Security, and Privacy (CrySP) research group. He holds a Ph.D. from the University of California, Berkeley, where he discovered serious weaknesses in a number of widely deployed security systems, including those used by cellular phones and wireless networks. He also studied systems for protecting the personal privacy of Internet users, which led to his role as Chief Scientist at Zero-Knowledge Systems (now known as Radialpoint), where he commercialized his research as the Freedom Network.

