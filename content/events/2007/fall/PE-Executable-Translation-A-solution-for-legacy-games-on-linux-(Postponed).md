---
name: 'PE Executable Translation: A solution for legacy games on linux (Postponed)'
short: 'David Tenty'
startDate: 'December 04 2007 16:30'
online: false
location: 'TBA'
---

With today's fast growing linux user base, a large porportion of legacy applications have established open-source equivalents or ports.	However, legacy games provided an intresting problem to gamers who might be inclinded to migrate to linux or other open platforms.	PE executable translation software will be presented that provides a solution to this dilema and will be contrasted with the windows compatiblity framwork Wine.	Postponed to a later date.

