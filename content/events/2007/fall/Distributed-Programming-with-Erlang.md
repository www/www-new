---
name: 'Distributed Programming with Erlang'
short: 'Brennan Taylor'
startDate: 'October 04 2007 16:30'
online: false
location: 'TBA'
---

A quick introduction on the current state of distributed programming and various grid computing projects. Followed by some	history and features of the Erlang language and finishing with distributed examples including operating on a cluster.

