---
name: 'Google Summer of Code, a look back on 2007'
short: 'Holden Karau'
startDate: 'December 05 2007 16:30'
online: false
location: 'MC 4061'
---

An overview on Google Summer of Code 2007. This talk will look at some of the Summer of Code projects, the project organization, etc.

Holden Karau participated in Google Summer of Code 2007 as a student on the subversion team. He created a set of scheme bindings for the	subversion project.

