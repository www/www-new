---
name: 'Writing World Class Software'
short: 'A talk by James Simpson'
startDate: 'February 09 2007 20:30'
online: false
location: 'DC 1351'
---

A common misconception amongst software developers is that top quality software encompasses certain platforms, is driven by a particular new piece of technology, or relies solely on a particular programming language. However as developers we tend to miss the less hyped issues and techniques involved in writing world class software. These techniques are universal to all programming languages, platforms and deployed technologies but are often times viewed as being so obvious that they are ignored by the typical developer. The topics covered in this lecture will include:

\- Writing bug-free to extremely low bug count software in real-time


 \- The concept of single-source, universal platform software


 \- Programming language interoperability





 ... and other less hyped yet vitally important concepts to writing World Class Software

