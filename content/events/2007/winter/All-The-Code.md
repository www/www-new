---
name: 'All The Code'
short: 'A demo/introduction to a new source code search engine. A talk by Holden Karau'
startDate: 'March 29 2007 16:30'
online: false
location: 'MC 1056'
---

Source code search engines are a relatively new phenomenon . The general idea of most source code search engines is helping programmers find pre-existing code. So if you were writing some code and you wanted to find a csv library, for example, you could search for csv. [All The Code](<http://www.allthecode.com/>) is a next generation source code search engine. Unlike earlier generations of source code search engines, it considers how code is used to help determine relevance of code.

The talk will primarily be a demo of [All The Code](<http://www.allthecode.com>), along with a brief discussion of some of the technology behind it.

