---
name: 'Surprise
    Bill Gates Visit'
short: 'Bill Gates is coming to visit the CSClub'
startDate: 'April 01 2007 16:30'
online: false
location: 'MC 3036'
---

While reading Slashdot, Bill came across the recently digitized audio recording of his 1989 talk at the Computer Science Club. As Bill has always had a soft-spot for the Computer Science Club, he has decided to pay us a surprise visit.

Bill promises to give away free copies of Windows Vista Ultimate, because frankly, nobody here (except j2simpso) wants to pay for a frisbee. Be sure to bring your resumes kids, because Bill will be recruiting for some exciting new positions at Microsoft, including Mindless Drone, Junior Code Monkey, and Assistant Human Cannonball.

