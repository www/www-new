---
name: 'Usability in the wild'
short: 'A talk by Michael Terry'
startDate: 'June 27 2007 16:30'
online: false
location: 'MC 4042'
---

What is the typical monitor resolution of a GIMP user? How many monitors do they have? What size images do they work on? How many layers are in their images? The answers to these questions are generally unknown: No means currently exist for open source applications to collect usage data. In this talk, I will present ingimp, a version of GIMP that has been instrumented to automatically collect usage data from real-world users. I will discuss ingimp's design, the type of data we collect, how we make the data available on the web, and initial results that begin to answer the motivating questions.

ingimp can be found at http://www.ingimp.org.

