---
name: 'Email encryption for the masses'
short: 'Ken Ho'
startDate: 'June 22 2007 16:30'
online: false
location: 'MC 4042'
---

E-mail transactions and confirmations have become commonplace and the information therein can often be sensitive. We use email for purposes as mundane as inbound marketing, to as sensitive as account passwords and financial transactions. And nearly all our email is sent in clear text; we trust only that others will not eavesdrop or modify our messages. But why rely on the goodness or apathy of your fellow man when you can ensure your message's confidentiality with encryption so strong not even the NSA can break? Speaker (Kenneth Ho) will discuss email encryption, and GNU Privacy Guard to ensure that your messages are sent, knowing that only your intended recipient can receive it.

An optional code-signing party will be held immediately afterwards; if you already have a PGP or GPG key and wish to participate, please submit the public key to [ gpg-keys@csclub.uwaterloo.ca](<mailto:gpg-keys@csclub.uwaterloo.ca>).

Laptop users are invited also to participate in key-pair sharing on-site, though it is preferable to send keys ahead of time.

