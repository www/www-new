---
name: 'Fedspulse.ca, Web 3.0, Portals and the Metaverse'
short: 'Peter Macdonald'
startDate: 'June 18 2007 16:30'
online: false
location: 'DC 4040'
---

The purpose of the talk is to address how students interact with the internet, and possibilities for how they could do so more efficiently. Information on events and happenings on UW campus is currently hosted on a desperate, series of internet applications. Interactions with WatSFIC is done over a Yahoo! mailing list, GLOW is organized through a Facebook group, campus information at large comes from [imprint.uwaterloo.ca](<http://imprint.uwaterloo.ca>). There has been historical pressures from various bodies, including some thinkers in feds and the administration, to centralize these issues. To create a one stop shop for students on campus.

It is not through confining data in cages that we will finally link all student activities together, instead it is by truly freeing it. When data can be anywhere, then it will be everywhere students need it. This is the underlying concept behind metadata, data that is freed from the confines of it's technical imprisonment. Metadata is the extension of people, organizations, and activities onto the internet in a way that is above the traditional understanding of how people interact with their networks. The talk will explore how Metadata can exist freely on the internet, how this affects concepts like Web 3.0, and how the university and the federation are poised to take advantage of this burgeoning new technology through adoptions of portals which will allow students to interact with a metaverse of data.

