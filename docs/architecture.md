# Architecture and Folder Structure

The diagram below shows a general overview of how the website is architected. A thin black arrow on the graph depicts a dependency.

Legend:

- <span style="background: #dae8fc;">Blue</span>: React components
- <span style="background: #f8cecc;">Red</span>: Code or assets that only exist during build time
- <span style="background: #d5e8d4;">Green</span>: Static assets that exist during runtime on the server

![diagram](static/architecture.svg)

## <code style="background: #dae8fc;">/pages</code>

This folder acts as the entry point for the our website. Pages are not built as reusable components, but rather as an outline of our website. All dynamically generated pages use the functionality exposed by the `/lib` folder. There are some folders that directly use the items in the `/content` folder directly.

Look at the [docs about pages](pages.md) to learn more about them.

## <code style="background: #dae8fc;">/components</code>

Components are the building blocks of our website. Most of our components are simple functional components that do not do much except making the UI look nice. Almost all of our components have no dependencies except `react`, `date-fns`, and other components from the `/components` folder. This structure allows us keep components and design separate from the business logic of the website making it easier to split them off into their own mini design framework if necessary.

## <code style="background: #f8cecc;">/scripts</code>

These contain scripts that run during the CI/CD phase to insert dynamically generated assets into the public folder - this is what the thick red arrow is referring to.

The two main scripts that we have today are:

- `generate-calendar` (`npm run build:calendar`): to generate an ical file that consumes events using the `/lib` folder - writing the resulting file to `public/events.ics`
- `optimize-images` (`npm run build:images`): to optimize images present in the `images` folder and write the optimized images in the `public/images` folder for the website to consume. You **must run it at least once after cloning** the repository so that you can see all the images during development.

## <code style="background: #f8cecc;">/lib</code>

This folder acts as an API layer for the website. It mainly adds helper functions to access the data in the `/content` folder easily, with the exception of `/lib/members.ts` - which uses LDAP to query CSC members.

## <code style="background: #f8cecc;">/content</code>

All the `.md` files in this folder are used by the `/lib` folder to provide an API over them. These markdown files may or may not have some metadata at the top written in yaml, which we parse using the `gray-matter` library.

Example of the metadata:

```
---
foo: i am some metadata
bar: some more
baz: ok last one
---

# This is not metadata
```

**Note**: The `.mdx` files under this folder are directly used in `/pages`. They are automatically converted to react components on the fly.

## <code style="background: #f8cecc;">/images</code>

These are the unoptimized images that you see on the website. They are consumed by the `optimize-images` script to ... optimize them ... before putting them in the `/public/images` folder. We need to optimize images in order to decrease the latency of the website.

## <span style="background: #d5e8d4;">`/public`</span>

This is the folder that contains all the static assets of the website. Everything in here is accessible at `https://csclub.uwaterloo.ca/[asset file name]`

For example `/public/fonts/future-bold.woff` is available at `https://csclub.uwaterloo.ca/fonts/future-bold.woff`.
