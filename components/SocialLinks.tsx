import React from "react";

import styles from "./SocialLinks.module.css";

interface Props {
  color: "white" | "gradient" | "blue";
  size: "small" | "big";
}

export const SocialLinks: React.FC<Props> = (props) => {
  return (
    <div className={styles.links}>
      {links.map((Link, index) => (
        <Link {...props} key={index} />
      ))}
    </div>
  );
};

const iconList = [
  {
    name: "Facebook",
    image: FacebookSvg,
    link: "https://www.facebook.com/uw.computerscienceclub",
  },
  {
    name: "Instagram",
    image: InstagramSvg,
    link: "https://www.instagram.com/uwcsclub/",
  },
  {
    name: "Twitch",
    image: TwitchSvg,
    link: "https://www.twitch.tv/uwcsclub",
  },
  {
    name: "Discord",
    image: DiscordSvg,
    link: "https://discord.gg/pHfYBCg",
  },
  {
    name: "Libera",
    image: LiberaSvg,
    link: "ircs://irc.libera.chat:6697/csc",
  },
];

const links = iconList.map((icon) => {
  function SocialLink({ size, color }: Props) {
    return (
      <a
        key={icon.link}
        href={icon.link}
        rel="noreferrer"
        target="_blank"
        className={`${styles.link} ${size === "big" ? styles.big : ""}`}
      >
        {icon.image(color)}
      </a>
    );
  }

  SocialLink.displayName = icon.name;

  return SocialLink;
});

export const [Facebook, Instagram, Twitch, Discord, Libera] = links;

function InstagramSvg(color: string) {
  return (
    <svg
      width="33"
      height="32"
      viewBox="0 0 33 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <linearGradient id="bluegreen-gradient">
        <stop offset="0%" stopColor="var(--blue-gradient)" />
        <stop offset="100%" stopColor="#4ED4B2" />
      </linearGradient>
      <path
        className={color !== "gradient" ? styles[color] : ""}
        fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.60352 8.00008C3.60352 6.58559 4.16542 5.22904 5.16561 4.22885C6.16581 3.22865 7.52236 2.66675 8.93685 2.66675H24.9368C26.3513 2.66675 27.7079 3.22865 28.7081 4.22885C29.7083 5.22904 30.2702 6.58559 30.2702 8.00008V24.0001C30.2702 25.4146 29.7083 26.7711 28.7081 27.7713C27.7079 28.7715 26.3513 29.3334 24.9368 29.3334H8.93685C7.52236 29.3334 6.16581 28.7715 5.16561 27.7713C4.16542 26.7711 3.60352 25.4146 3.60352 24.0001V8.00008ZM8.93685 5.33341C8.22961 5.33341 7.55133 5.61437 7.05123 6.11446C6.55113 6.61456 6.27018 7.29284 6.27018 8.00008V24.0001C6.27018 24.7073 6.55113 25.3856 7.05123 25.8857C7.55133 26.3858 8.22961 26.6667 8.93685 26.6667H24.9368C25.6441 26.6667 26.3224 26.3858 26.8225 25.8857C27.3226 25.3856 27.6035 24.7073 27.6035 24.0001V8.00008C27.6035 7.29284 27.3226 6.61456 26.8225 6.11446C26.3224 5.61437 25.6441 5.33341 24.9368 5.33341H8.93685ZM16.9368 12.0001C15.876 12.0001 14.8586 12.4215 14.1084 13.1717C13.3583 13.9218 12.9368 14.9392 12.9368 16.0001C12.9368 17.0609 13.3583 18.0784 14.1084 18.8285C14.8586 19.5787 15.876 20.0001 16.9368 20.0001C17.9977 20.0001 19.0151 19.5787 19.7653 18.8285C20.5154 18.0784 20.9368 17.0609 20.9368 16.0001C20.9368 14.9392 20.5154 13.9218 19.7653 13.1717C19.0151 12.4215 17.9977 12.0001 16.9368 12.0001ZM10.2702 16.0001C10.2702 14.232 10.9726 12.5363 12.2228 11.286C13.473 10.0358 15.1687 9.33342 16.9368 9.33342C18.705 9.33342 20.4007 10.0358 21.6509 11.286C22.9011 12.5363 23.6035 14.232 23.6035 16.0001C23.6035 17.7682 22.9011 19.4639 21.6509 20.7141C20.4007 21.9644 18.705 22.6667 16.9368 22.6667C15.1687 22.6667 13.473 21.9644 12.2228 20.7141C10.9726 19.4639 10.2702 17.7682 10.2702 16.0001ZM24.2702 10.6667C24.8006 10.6667 25.3093 10.456 25.6844 10.081C26.0595 9.70589 26.2702 9.19718 26.2702 8.66675C26.2702 8.13632 26.0595 7.62761 25.6844 7.25253C25.3093 6.87746 24.8006 6.66675 24.2702 6.66675C23.7397 6.66675 23.231 6.87746 22.856 7.25253C22.4809 7.62761 22.2702 8.13632 22.2702 8.66675C22.2702 9.19718 22.4809 9.70589 22.856 10.081C23.231 10.456 23.7397 10.6667 24.2702 10.6667Z"
      />
    </svg>
  );
}

function DiscordSvg(color: string) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="29"
      height="23"
      viewBox="0 0 29 23"
      version="1.1"
    >
      <linearGradient id="bluegreen-gradient">
        <stop offset="0%" stopColor="var(--blue-gradient)" />
        <stop offset="100%" stopColor="#4ED4B2" />
      </linearGradient>
      <g id="surface1">
        <path
          className={color !== "gradient" ? styles[color] : ""}
          fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
          d="M 24.550781 2.046875 C 22.703125 1.179688 20.71875 0.539062 18.648438 0.171875 C 18.609375 0.167969 18.570312 0.183594 18.550781 0.21875 C 18.296875 0.683594 18.015625 1.289062 17.816406 1.765625 C 15.589844 1.421875 13.371094 1.421875 11.1875 1.765625 C 10.988281 1.277344 10.695312 0.683594 10.441406 0.21875 C 10.421875 0.183594 10.382812 0.167969 10.347656 0.171875 C 8.273438 0.539062 6.292969 1.179688 4.441406 2.046875 C 4.425781 2.054688 4.414062 2.066406 4.402344 2.082031 C 0.644531 7.832031 -0.386719 13.441406 0.121094 18.980469 C 0.121094 19.007812 0.136719 19.035156 0.15625 19.050781 C 2.636719 20.917969 5.039062 22.046875 7.398438 22.800781 C 7.4375 22.8125 7.476562 22.796875 7.5 22.765625 C 8.058594 21.984375 8.554688 21.164062 8.980469 20.296875 C 9.007812 20.246094 8.984375 20.1875 8.933594 20.167969 C 8.144531 19.859375 7.390625 19.488281 6.667969 19.0625 C 6.613281 19.027344 6.609375 18.945312 6.660156 18.90625 C 6.8125 18.789062 6.964844 18.667969 7.109375 18.542969 C 7.136719 18.523438 7.171875 18.519531 7.203125 18.53125 C 11.949219 20.75 17.085938 20.75 21.777344 18.53125 C 21.808594 18.515625 21.84375 18.519531 21.871094 18.542969 C 22.019531 18.667969 22.167969 18.789062 22.324219 18.90625 C 22.375 18.945312 22.371094 19.027344 22.316406 19.0625 C 21.59375 19.496094 20.839844 19.859375 20.050781 20.164062 C 20 20.1875 19.976562 20.246094 20.003906 20.296875 C 20.4375 21.160156 20.933594 21.984375 21.484375 22.765625 C 21.503906 22.796875 21.546875 22.8125 21.585938 22.800781 C 23.953125 22.046875 26.355469 20.917969 28.835938 19.050781 C 28.859375 19.035156 28.871094 19.011719 28.875 18.984375 C 29.480469 12.578125 27.863281 7.015625 24.585938 2.082031 C 24.578125 2.066406 24.566406 2.054688 24.550781 2.046875 Z M 9.691406 15.609375 C 8.261719 15.609375 7.085938 14.265625 7.085938 12.617188 C 7.085938 10.96875 8.238281 9.625 9.691406 9.625 C 11.152344 9.625 12.320312 10.980469 12.296875 12.617188 C 12.296875 14.265625 11.140625 15.609375 9.691406 15.609375 Z M 19.328125 15.609375 C 17.898438 15.609375 16.722656 14.265625 16.722656 12.617188 C 16.722656 10.96875 17.875 9.625 19.328125 9.625 C 20.789062 9.625 21.957031 10.980469 21.933594 12.617188 C 21.933594 14.265625 20.789062 15.609375 19.328125 15.609375 Z M 19.328125 15.609375 "
        />
      </g>
    </svg>
  );
}

function TwitchSvg(color: string) {
  return (
    <svg
      width="26"
      height="31"
      viewBox="0 0 26 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <linearGradient id="bluegreen-gradient">
        <stop offset="0%" stopColor="var(--blue-gradient)" />
        <stop offset="100%" stopColor="#4ED4B2" />
      </linearGradient>
      <g clipPath="url(#clip0)">
        <path
          d="M5.83051 0.872314L0.535156 6.15123V25.1553H6.88958V30.4342L12.1849 25.1553H16.4212L25.9529 15.6533V0.872314H5.83051ZM23.8347 14.5975L19.5984 18.8206H15.3622L11.6554 22.5159V18.8206H6.88958V2.98388H23.8347V14.5975Z"
          className={color !== "gradient" ? styles[color] : ""}
          fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
        />
        <path
          d="M20.6572 6.6792H18.5391V13.0139H20.6572V6.6792Z"
          className={color !== "gradient" ? styles[color] : ""}
          fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
        />
        <path
          d="M14.832 6.6792H12.7139V13.0139H14.832V6.6792Z"
          className={color !== "gradient" ? styles[color] : ""}
          fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect
            width="25.4177"
            height="29.5619"
            className={color !== "gradient" ? styles[color] : ""}
            fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
            transform="translate(0.535156 0.872314)"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

function FacebookSvg(color: string) {
  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <linearGradient id="bluegreen-gradient">
        <stop offset="0%" stopColor="var(--blue-gradient)" />
        <stop offset="100%" stopColor="#4ED4B2" />
      </linearGradient>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.33268 2.66675H26.666C28.1388 2.66675 29.3327 3.86066 29.3327 5.33342V26.6668C29.3327 28.1395 28.1388 29.3334 26.666 29.3334H5.33268C3.85992 29.3334 2.66602 28.1395 2.66602 26.6668V5.33342C2.66602 3.86066 3.85992 2.66675 5.33268 2.66675ZM5.3311 5.33333V26.6667H26.6644V5.33333H5.3311ZM14.8428 16.0059H17.332V24H19.9987V16.0059H22.6419V13.3392H19.9987V12C19.9987 11.2636 20.5957 10.6667 21.332 10.6667H22.6654V8H21.332C19.1229 8 17.332 9.79086 17.332 12V13.3392H14.8428V16.0059Z"
        className={color !== "gradient" ? styles[color] : ""}
        fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
      />
    </svg>
  );
}

function LiberaSvg(color: string) {
  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 96.89 78.87"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <linearGradient id="bluegreen-gradient">
        <stop offset="0%" stopColor="var(--blue-gradient)" />
        <stop offset="100%" stopColor="#4ED4B2" />
      </linearGradient>
      <path
        d="M40.86 9.3h-.01a32.2 32.2 0 00-.65.14l-.22.04a39.48 39.48 0 00-.43.1l-.21.05a39.35 39.35 0 00-5.68 1.8l-.12.06L44.8 48.82l.47-.55 1.26-1.48zm14.98-.04l-4.1 31.45 3.32-3.88.66 1.05 7.36-26.51-.18-.07a37.97 37.97 0 00-6.55-1.94 39.84 39.84 0 00-.43-.09zm-35.33 10.9a34.93 34.93 0 00-3.03 3.42 41.1 41.1 0 00-1.8 2.48v.02L37.2 49.59l1.62-2.12.08.04zm55.45-.44l-15.91 25.1 1.81 2.9 19.26-21.8a35.29 35.29 0 00-2.9-3.82 38.85 38.85 0 00-2.26-2.38zM9.98 39.94a38.23 38.23 0 00-.72 7.54L32.2 56.1l1.79-2.33zm77.07.67L65.2 53.1l1.72 2.75 20.7-7.55v-.1a22.66 22.66 0 00.01-.66v-.44-.22-.14-.22l-.01-.21v-.22l-.01-.22-.01-.22-.01-.22-.01-.22-.02-.21-.01-.22-.02-.22-.01-.22-.02-.21-.02-.22-.02-.22-.02-.22-.02-.21a31.66 31.66 0 00-.37-2.6v-.04zM12.42 62.57a39.78 39.78 0 003.96 7.03h.01l6.73-1.48.14-.18h-.16l4.18-5.44zm58.83.21l3.24 5.39 6.05 1.36.05-.06a36.02 36.02 0 002.53-4.1A37.2 37.2 0 0084.27 63z"
        className={color !== "gradient" ? styles[color] : ""}
        fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}
        paintOrder="markers fill stroke"
      />
      <g fill={color === "gradient" ? "url(#bluegreen-gradient)" : ""}>
        <path
          style={{
            lineHeight: "normal",
            fontVariantLigatures: "normal",
            fontVariantPosition: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantAlternates: "normal",
            fontVariantEastAsian: "normal",
            fontFeatureSettings: "normal",
            fontVariationSettings: "normal",
            textIndent: "0",
            textAlign: "start",
            textDecorationLine: "none",
            textDecorationStyle: "solid",
            textDecorationColor: "#000",
            textTransform: "none",
            textOrientation: "mixed",
            whiteSpace: "normal",
            shapeMargin: "0",
            inlineSize: "0",
            isolation: "auto",
            mixBlendMode: "normal",
          }}
          d="M55.53 35.83L44.12 48.86l-5.22-2.3-16.98 21.82h1.48l15.88-20.32 5.17 2.29 10.9-12.45c6.04 10.27 12.55 20.15 18.47 30.49h1.48z"
          color="#000"
          fontWeight="400"
          fontFamily="sans-serif"
          overflow="visible"
        />
        <path
          style={{
            lineHeight: "normal",
            fontVariantLigatures: "normal",
            fontVariantPosition: "normal",
            fontVariantCaps: "normal",
            fontVariantNumeric: "normal",
            fontVariantAlternates: "normal",
            fontVariantEastAsian: "normal",
            fontFeatureSettings: "normal",
            fontVariationSettings: "normal",
            textIndent: "0",
            textAlign: "start",
            textDecorationLine: "none",
            textDecorationStyle: "solid",
            textDecorationColor: "#000",
            textTransform: "none",
            textOrientation: "mixed",
            whiteSpace: "normal",
            shapeMargin: "0",
            inlineSize: "0",
            isolation: "auto",
            mixBlendMode: "normal",
          }}
          d="M55.32 39.73l-10.6 12.15-5.17-2.15-14.64 18.64h1.62l13.4-17.15 5.14 2.13L55.14 41.8l15.84 26.62 1.56-.03z"
          color="#000"
          fontWeight="400"
          fontFamily="sans-serif"
          overflow="visible"
        />
        <path d="M28.1 68.36l12.23-15.59 5.24 2.13 9.51-10.92 14.28 24.4z" />
      </g>
    </svg>
  );
}
