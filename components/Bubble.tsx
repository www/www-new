import React from "react";

import { Image } from "@/components/Image";

import { DefaultLayout } from "./DefaultLayout";

import styles from "./Bubble.module.css";

export function Bubble(props: { children: React.ReactNode }) {
  return (
    <div className={styles.container}>
      <div className={styles.bubble} aria-hidden>
        <div className={styles.bar} />
        <Image
          className={styles.decoration}
          src="/images/bubble-decoration.svg"
        />
      </div>
      <div className={styles.content}>
        <DefaultLayout>{props.children}</DefaultLayout>
      </div>
    </div>
  );
}
