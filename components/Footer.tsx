import Link from "next/link";
import React from "react";

import { Button } from "./Button";
import { SocialLinks } from "./SocialLinks";
import { useThemeContext } from "./Theme";

import styles from "./Footer.module.css";

export function Footer() {
  const themeContext = useThemeContext();

  return (
    <footer className={styles.footer}>
      <div className={styles.container}>
        <div className={styles.text}>
          Have questions? Email us at{" "}
          <Link href="mailto:exec@csclub.uwaterloo.ca">
            <a className={styles.email}>exec@csclub.uwaterloo.ca</a>
          </Link>
        </div>
        <Button
          size="small"
          onClick={() =>
            themeContext?.theme.name === "dark"
              ? themeContext?.setTheme("light")
              : themeContext?.setTheme("dark")
          }
        >
          Toggle Theme
        </Button>
        <SocialLinks color="white" size="small" />
      </div>
    </footer>
  );
}
