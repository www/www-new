import React, { HTMLAttributes } from "react";

import styles from "./Code.module.css";

export function Code(props: HTMLAttributes<HTMLElement>) {
  const classes = [styles.code, props.className ?? ""];

  return <code {...props} className={classes.join(" ")} />;
}
