import React from "react";

import { Button } from "./Button";

import styles from "./EmailSignup.module.css";

export function EmailSignup() {
  return (
    <section className={styles.container}>
      <h1 className={styles.header}>Join our mailing list!</h1>
      <p>
        Join our mailing list to receive email notifications about important
        news and upcoming events!
      </p>
      <Button
        isLink={true}
        href="https://mailman.csclub.uwaterloo.ca/postorius/lists/csc-general.csclub.uwaterloo.ca/"
        className={styles.button}
      >
        Subscribe
      </Button>
    </section>
  );
}
