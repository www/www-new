import React, { HTMLAttributes } from "react";

import styles from "./Pre.module.css";

export function Pre(props: HTMLAttributes<HTMLPreElement>) {
  const classes = [styles.pre, props.className ?? ""];

  return <pre {...props} className={classes.join(" ")} />;
}
